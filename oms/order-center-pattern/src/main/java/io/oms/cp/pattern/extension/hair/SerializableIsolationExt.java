package io.oms.cp.pattern.extension.hair;

import io.oms.cp.pattern.HairPattern;
import io.dddplus.annotation.Extension;
import io.oms.cp.spec.ext.ISerializableIsolationExt;
import io.oms.cp.spec.model.IOrderMain;
import io.oms.cp.spec.model.vo.LockEntry;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

@Extension(code = HairPattern.CODE, value = "hairSerializableIsolationExt")
public class SerializableIsolationExt implements ISerializableIsolationExt {

    @Override
    public LockEntry createLockEntry(@NotNull IOrderMain model) {
        return new LockEntry(model.customerProvidedOrderNo(), 1, TimeUnit.HOURS);
    }
}
