package io.oms.cp.pattern;

import io.dddplus.annotation.Pattern;
import io.oms.cp.spec.Patterns;
import io.oms.cp.spec.model.IOrderMain;
import io.dddplus.ext.IIdentityResolver;

import javax.validation.constraints.NotNull;

@Pattern(code = HairPattern.CODE, name = "海尔业务模式")
public class HairPattern implements IIdentityResolver<IOrderMain> {
    public static final String CODE = Patterns.Hair;

    @Override
    public boolean match(@NotNull IOrderMain model) {
        if (model.getCustomerNo() == null) {
            return false;
        }

        return model.getCustomerNo().equals(CODE);
    }
}
