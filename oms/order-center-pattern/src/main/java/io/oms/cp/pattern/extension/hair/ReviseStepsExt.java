package io.oms.cp.pattern.extension.hair;

import io.oms.cp.pattern.HairPattern;
import io.dddplus.annotation.Extension;
import io.oms.cp.spec.Steps;
import io.oms.cp.spec.ext.IReviseStepsExt;
import io.oms.cp.spec.model.IOrderMain;

import java.util.ArrayList;
import java.util.List;

@Extension(code = HairPattern.CODE, value = "hairReviseStepsExt")
public class ReviseStepsExt implements IReviseStepsExt {

    @Override
    public List<String> reviseSteps(IOrderMain model) {
        if (Steps.SubmitOrder.Activity.equals(model.currentActivity())) {
            if (model.currentStep().equals(Steps.SubmitOrder.BasicStep)) {
                List<String> subsequentSteps = new ArrayList<>();
                return subsequentSteps; // 没有后续步骤了：跳过PersistStep
            }
        }

        List<String> result = new ArrayList<>();
        return result;
    }
}
