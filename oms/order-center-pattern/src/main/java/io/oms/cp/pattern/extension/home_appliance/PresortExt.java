package io.oms.cp.pattern.extension.home_appliance;

import io.oms.cp.pattern.HomeAppliancePattern;
import lombok.extern.slf4j.Slf4j;
import io.dddplus.annotation.Extension;
import io.oms.cp.spec.ext.IPresortExt;
import io.oms.cp.spec.model.IOrderMain;

import javax.validation.constraints.NotNull;

@Slf4j
@Extension(code = HomeAppliancePattern.CODE, value = "homePresort")
public class PresortExt implements IPresortExt {

    @Override
    public void presort(@NotNull IOrderMain model) {
        log.info("家电的预分拣执行了");
    }
}
