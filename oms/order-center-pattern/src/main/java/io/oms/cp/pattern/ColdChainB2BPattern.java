package io.oms.cp.pattern;

import io.dddplus.annotation.Pattern;
import io.dddplus.ext.IIdentityResolver;
import io.oms.cp.spec.Patterns;
import io.oms.cp.spec.model.IOrderMain;

import javax.validation.constraints.NotNull;

@Pattern(code = ColdChainB2BPattern.CODE, name = "冷链B2B模式", tags = Patterns.Tags.B2B)
public class ColdChainB2BPattern implements IIdentityResolver<IOrderMain> {
    public static final String CODE = Patterns.ColdChainB2B;

    @Override
    public boolean match(@NotNull IOrderMain model) {
        return model.isB2B() && model.isColdChain();
    }
}
