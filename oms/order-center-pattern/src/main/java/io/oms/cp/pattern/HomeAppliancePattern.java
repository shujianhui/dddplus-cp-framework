package io.oms.cp.pattern;

import io.dddplus.annotation.Pattern;
import io.oms.cp.spec.Patterns;
import io.oms.cp.spec.model.IOrderMain;
import io.dddplus.ext.IIdentityResolver;

import javax.validation.constraints.NotNull;

@Pattern(code = HomeAppliancePattern.CODE, name = "家用电器行业模式")
public class HomeAppliancePattern implements IIdentityResolver<IOrderMain> {
    public static final String CODE = Patterns.HomeAppliance;

    @Override
    public boolean match(@NotNull IOrderMain model) {
        if (model.getCustomerNo() == null) {
            return false;
        }

        return model.getCustomerNo().equals(CODE);
    }
}
