package io.oms.cp.pattern.extension.home_appliance;

import io.oms.cp.pattern.HomeAppliancePattern;
import io.dddplus.annotation.Extension;
import io.oms.cp.spec.ext.IAssignOrderNoExt;
import io.oms.cp.spec.model.IOrderMain;

import javax.validation.constraints.NotNull;

@Extension(code = HomeAppliancePattern.CODE, value = "homeAssignOrderNoExt")
public class AssignOrderNoExt implements IAssignOrderNoExt {
    public static final String HOME_ORDER_NO = "SO9987012";

    @Override
    public void assignOrderNo(@NotNull IOrderMain model) {
        model.assignOrderNo(this, HOME_ORDER_NO);
    }
}
