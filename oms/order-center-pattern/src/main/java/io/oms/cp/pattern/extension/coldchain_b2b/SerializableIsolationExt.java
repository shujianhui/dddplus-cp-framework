package io.oms.cp.pattern.extension.coldchain_b2b;

import io.oms.cp.pattern.ColdChainB2BPattern;
import io.dddplus.annotation.Extension;
import io.oms.cp.spec.ext.ISerializableIsolationExt;
import io.oms.cp.spec.model.IOrderMain;
import io.oms.cp.spec.model.vo.LockEntry;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

@Extension(code = ColdChainB2BPattern.CODE, value = "ccb2bSerializableIsolationExt", name = "冷链B2B模式下的防并发机制")
public class SerializableIsolationExt implements ISerializableIsolationExt {

    @Override
    public LockEntry createLockEntry(@NotNull IOrderMain model) {
        return new LockEntry(model.customerProvidedOrderNo(), 19, TimeUnit.MINUTES);
    }
}
