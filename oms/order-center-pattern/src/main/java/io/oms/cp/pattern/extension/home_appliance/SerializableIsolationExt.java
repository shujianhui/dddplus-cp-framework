package io.oms.cp.pattern.extension.home_appliance;

import io.oms.cp.pattern.HomeAppliancePattern;
import io.dddplus.annotation.Extension;
import io.oms.cp.spec.ext.ISerializableIsolationExt;
import io.oms.cp.spec.model.IOrderMain;
import io.oms.cp.spec.model.vo.LockEntry;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

@Extension(code = HomeAppliancePattern.CODE, value = "homeSerializableIsolationExt")
public class SerializableIsolationExt implements ISerializableIsolationExt {

    @Override
    public LockEntry createLockEntry(@NotNull IOrderMain model) {
        return new LockEntry(model.customerProvidedOrderNo(), 5, TimeUnit.MINUTES);
    }
}
