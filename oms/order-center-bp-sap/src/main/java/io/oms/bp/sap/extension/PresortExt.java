package io.oms.bp.sap.extension;

import io.dddplus.annotation.Extension;
import lombok.extern.slf4j.Slf4j;
import io.oms.bp.sap.SapPartner;
import io.oms.cp.spec.ext.IPresortExt;
import io.oms.cp.spec.model.IOrderMain;

import javax.validation.constraints.NotNull;

@Extension(code = SapPartner.CODE, value = "sapPresort")
@Slf4j
public class PresortExt implements IPresortExt {

    @Override
    public void presort(@NotNull IOrderMain model) {
        log.info("SAP里预分拣的结果：{}", new MockInnerClass().getResult());
    }

    // 演示内部类的使用
    private static class MockInnerClass {
        int getResult() {
            return 2;
        }
    }
}
