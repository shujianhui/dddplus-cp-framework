package io.oms.bp.sap;

import io.dddplus.annotation.Partner;
import io.dddplus.ext.IIdentityResolver;
import io.oms.cp.spec.model.IOrderMain;

@Partner(code = SapPartner.CODE, name = "结算管理业务前台BP")
public class SapPartner implements IIdentityResolver<IOrderMain> {
    public static final String CODE = "sap";

    @Override
    public boolean match(IOrderMain model) {
        return model.getSource().toLowerCase().equals(CODE);
    }
}
