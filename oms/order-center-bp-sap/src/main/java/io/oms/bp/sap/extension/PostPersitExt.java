package io.oms.bp.sap.extension;

import io.dddplus.annotation.Extension;
import lombok.extern.slf4j.Slf4j;
import io.oms.bp.sap.SapPartner;
import io.oms.cp.spec.ext.IPostPersistExt;
import io.oms.cp.spec.model.IOrderMain;

@Slf4j
@Extension(code = SapPartner.CODE, value = "sapPostPersistExt")
public class PostPersitExt implements IPostPersistExt {

    @Override
    public void afterPersist(IOrderMain model) {
        log.info("{} 落库了，SAP，要发个MQ通知我的下游！", model);
    }
}
