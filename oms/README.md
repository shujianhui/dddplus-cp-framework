# oms
订单管理系统

![Requirement](https://img.shields.io/badge/JDK-8+-green.svg)

[OMS业务入门](https://github.com/funkygao/oms/blob/master/README.md)。

## 目录

- [Terms explained](#terms-explained)
- [如何运行该演示](#如何运行该演示)
- [演示代码入口](#演示代码入口)
- [项目基本介绍](https://github.com/funkygao/cp-ddd-framework/wiki/The-Demo)
- [代码快速入门](#代码快速入门)
- [代码结构](#代码结构)
   - [依赖关系](#依赖关系)
   - 一套[订单履约中台代码库](#order-center-cp)
   - [中台的个性化业务包](#order-center-pattern)
   - [三个业务前台代码库](#订单履约中台的多个业务前台)
      - [KA业务前台](#order-center-bp-ka)
      - [ISV业务前台](#order-center-bp-isv)
      - [结算管理业务前台](#order-center-bp-sap)
   - [支撑域](#支撑域)
- [如何快速搭建中台工程骨架](#如何快速搭建中台工程骨架)

## Terms explained

- bp
   - Business Partner
   - 业务前台
- cp
   - Central Platform
   - 中台
- isv
   - Independent Software Vendors
   - 独立软件开发商
- sap
   - 取Settlement(结算)、Accounting(会计)、Payment(支付)三个单词的首字母
   - 结算管理
- ka
   - Key Account
   - 关键客户
- oc
   - Order Center
   - 订单中心


## 如何运行该演示

``` bash
mvn package
java -jar order-center-cp/cp-oc-main/target/jeecg-boot-module-oms.jar
#java -jar order-center-cp/cp-oc-main/target/jeecg-boot-module-oms.jar 9090 plugin

# in another terminal
curl -XPOST http://localhost:9090/order             # submit an order
curl -XPOST http://localhost:9090/reload?plugin=isv # plugin hot reloading
```

## 演示代码入口

- 启动入口 [OrderServer.java](order-center-cp/cp-oc-main/src/main/java/org/example/cp/oms/OrderServer.java)
- Web Controller [OrderController.java](order-center-cp/cp-oc-controller/src/main/java/org/example/cp/oms/controller/OrderController.java)

## 代码快速入门

- [中台架构特色的DDD分层架构](order-center-cp)
   - 也可以通过[dddplus-archetype](https://github.com/dddplus/dddplus-archetype)，快速从零开始搭建中台的分层架构，并融入最佳实践
   - [domain层是如何通过依赖倒置模式与infrastructure层交互的](order-center-cp/cp-oc-domain/src/main/java/org/example/cp/oms/domain/facade/mq/IMessageProducer.java)
   - [Repository同理](order-center-cp/cp-oc-domain/src/main/java/org/example/cp/oms/domain/facade/repository/IOrderRepository.java)
   - [为什么依赖倒置统一存放在facade包](order-center-cp/cp-oc-domain/src/main/java/org/example/cp/oms/domain/facade/package-info.java)
- 如何理解 [Step](https://github.com/funkygao/cp-ddd-framework/wiki/Steps)
- 扩展点：订单的防并发
   - [如何识别该业务属于KA业务前台](order-center-bp-ka/src/main/java/org/example/bp/oms/ka/KaPartner.java)
   - [一个扩展点声明](order-center-cp/cp-oc-spec/src/main/java/org/example/cp/oms/spec/ext/ISerializableIsolationExt.java)
   - [该扩展点，KA业务前台的实现](order-center-bp-ka/src/main/java/org/example/bp/oms/ka/extension/SerializableIsolationExt.java)
   - [该扩展点，ISV业务前台的实现](order-center-bp-isv/src/main/java/org/example/bp/oms/isv/extension/SerializableIsolationExt.java)
   - [该扩展点，中台的一个业务场景实现](order-center-pattern/src/main/java/org/example/cp/oms/pattern/extension/coldchain_b2b/SerializableIsolationExt.java)
   - [扩展点被封装到DomainAbility](order-center-cp/cp-oc-domain/src/main/java/org/example/cp/oms/domain/ability/SerializableIsolationAbility.java)
   - [扩展点被调用](order-center-cp/cp-oc-domain/src/main/java/org/example/cp/oms/domain/service/SubmitOrder.java)
- [前台对中台的步骤编排](order-center-bp-ka/src/main/java/org/example/bp/oms/ka/extension/DecideStepsExt.java)
   - [动态的步骤编排](order-center-cp/cp-oc-domain/src/main/java/org/example/cp/oms/domain/step/submitorder/BasicStep.java)
- [扩展属性通过扩展点的实现](order-center-bp-isv/src/main/java/org/example/bp/oms/isv/extension/CustomModelExt.java)
- [业务约束规则的显式化](order-center-cp/cp-oc-domain/src/main/java/org/example/cp/oms/domain/specification/ProductNotEmptySpec.java)
   - [如何使用](order-center-cp/cp-oc-domain/src/main/java/org/example/cp/oms/domain/ability/extension/DefaultAssignOrderNoExt.java)
- [中台统一定义，兼顾前台个性化的错误码机制](order-center-cp/cp-oc-spec/src/main/java/org/example/cp/oms/spec/exception/OrderException.java)
- [中台特色的领域模型](order-center-cp/cp-oc-spec/src/main/java/org/example/cp/oms/spec/model/IOrderMain.java)
   - spec jar里定义受限的领域模型输出给业务前台：通过接口，而不是类
   - 一种中台控制力更强的shared kernel机制
   - domain层是如何实现的领域模型接口的：[订单主档](order-center-cp/cp-oc-domain/src/main/java/org/example/cp/oms/domain/model/OrderMain.java)
   - [Creator的作用](order-center-cp/cp-oc-domain/src/main/java/org/example/cp/oms/domain/model/OrderMain.java#L50)
- 领域步骤，业务模式等，中台如何统一定义，并输出给前台使用？
   - 例如，业务前台是可以编排中台的步骤的，它必须要知道中台有哪些步骤
   - [领域步骤的统一输出](order-center-cp/cp-oc-spec/src/main/java/org/example/cp/oms/spec/Steps.java)
   - [业务模式的统一输出](order-center-cp/cp-oc-spec/src/main/java/org/example/cp/oms/spec/Patterns.java)
- [中台如何输出资源给业务前台使用](order-center-cp/cp-oc-spec/src/main/java/org/example/cp/oms/spec/resource/IStockRpc.java)
   - 如果业务前台要使用中台的MQ怎么办？能否中台封装一下，不直接暴露给前台
- [库存支撑域给订单核心域的能力输出](order-center-domain-stock/order-center-stock-spec/src/main/java/org/example/oms/d/stock/spec/)
   - [能力的实现](order-center-domain-stock/order-center-stock-domain/src/main/java/org/example/oms/d/stock/domain/service/StockService.java)
   - [订单核心域对库存支撑域的调用](order-center-cp/cp-oc-domain/src/main/java/org/example/cp/oms/domain/step/submitorder/StockStep.java)
- [按需打包](order-center-cp/cp-oc-main/pom.xml)
   - 这样才能做到一套代码，支撑国内、国际业务
   - 灵活的部署形式

## 代码结构

### 依赖关系

![](/doc/assets/img/ddd-depgraph.png)

### [order-center-cp](order-center-cp)

订单履约中台，通过[spec jar](order-center-cp/cp-oc-spec)为业务前台赋能，输出中台标准，并提供扩展机制。

#### [order-center-pattern](order-center-pattern)

订单履约中台本身的个性化业务，即个性化的业务模式包。

### 订单履约中台的多个业务前台

#### [order-center-bp-ka](order-center-bp-ka)

KA，关键客户的个性化业务通过扩展点的实现完成。

#### [order-center-bp-isv](order-center-bp-isv)

ISV，独立软件开发商的个性化业务通过扩展点的实现完成。

#### [order-center-bp-sap](order-center-bp-sap)

SAP，订单结算业务前台的个性化业务通过扩展点的实现完成。

这个业务BP，被中台要求不能使用Spring框架开发，不能在业务扩展包里使用AOP等Spring机制，只能严格实现中台定义的扩展点。

为了演示，ISV和KA这2个业务前台BP在开发业务扩展包时，可以使用Spring框架。

### 支撑域

![](http://www.plantuml.com/plantuml/svg/RPBFRh905CNtFWMhRy4pV6byWQwwwDf4cfYeCTNkmJzeDKHGI9lM5ajCQfk8DgcK0jMNcJiCRz7HcKObBCyzltivSCZN6uNhHgLKBLOAjPme4DS1pSBc4eyCiErSJXG52CQmkDyfaQh__setvRfqbZXjqARCInoLsdYYGV-5GfH2FnQ-ynY3Rz_WmugRtynYAyXVmBR50B8UW1mbSaWsHWecNeVUWLaxrjMK5KT1qXrcFY9fpQ6dPbY7zAO2xaDs-WFKxMDpam5HIhWylq3130MZz8T1_W261Wh7TF74OAFOj57m6lSzB2lm-8oYwVvSqj78LZ--A82bWkinXe-G7s9hXJNt21ahNB3k86g2x--xAqjN3Q5UAaginiuShvLuFY1VDl7V-HBDShYmIxWCSQ1pJKETQFBfqDVd0YOhU9Ave2LXg_Ut9gkW6pkHbwf5_dFz0W00)

- [库存支撑域](order-center-domain-stock)
- 更多的支撑域...

## 如何快速搭建中台工程骨架

使用 [dddplus-archetype](https://github.com/dddplus/dddplus-archetype)，可以快速搭建中台的工程骨架。

# OMS

## TOC

- [What is OMS](#what-is-oms)
  - [Functionalities](#functionalities)
  - [Features](#features)
  - [Characteriscs](#characteriscs)
    - [variety](#variety多样性)
    - [正向、逆向、调拨、盘点和搬仓混在一起，有race condition](#正向逆向调拨盘点和搬仓混在一起有race-condition)
    - [模型复杂](#模型复杂)
    - [依赖多](#依赖多)
- [Core tech building blocks](#core-tech-building-blocks)
- [Design](#design)
  - [带着问题思考](#带着问题思考)
  - [订单形态](#订单形态)
  - [Landscape](#Landscape)
  - [核心流程](#核心流程)

## What is OMS

订单作为电商系统的纽带，贯穿了电商系统的关键流程，其他模块都是围绕订单构建的。

订单可以认为是一次交易的生命周期，订单是交易的载体，反应的是履约的内容，即：一份订单就是一份合同。

通过OMS，使得订单执行过程中的相关系统，例如TMS，OMS，BMS等成为一个有机整体，并为持续优化提供有力的数据支持。

一个订单从提交到最后真正生产成功，需要经历几十个系统，涉及的接口交互、MQ交互可能达到上百个，任何一个环节出问题，都会导致这一单的异常。

```
       Partener --+         
                  |         
          Admin --+         
                  |         
         Seller --+         +-- BMS
                  |         |
           User --+         +-- TMS
                  |-- OMS --|
           API ---+    |    +-- WMS
                       |
    +------------------------------------------------------+
    |		     |       |                                 |
orderMark       FSM   storage                         dependencies
                         |                                 |
                    +----+--------+                        |- 台账
                    |             |                        |- 计费
              lookup & search  modeling                    |- BigData
                                  |                        |- 商品
                                facts                      |- 库存
                                  |                        |- 产品模型
                           BI & data mining                |- 时效
                                                           |- 地址和GIS
                                                           |- 支付
                                                           |- 预约
                                                           |- 主数据
                                                           |- 预分拣
                                                           |- 风控
                                                           +- ...
```

### Functionalities

#### 用户视角

- 全渠道订单
  - 多业态
  - 中间枢纽
  - 统一视图
- 集单
- 团单，拼单
- 合单
- 预约单

#### 执行视角

- 憋单
- 拆单
- 转移
- 下发
- 回传
- 拦截
- 正向和逆向

### Features

- FSM
- Information Collector and Passthru(model)
  - Products
    - 商品信息主要影响库存、金额和WMS生产
  - Stakeholders
  - Addresses
  - Promotions
    - 涉及风控、黑白名单等
    - 可能涉及到人工审核
  - Payment
    - 记录交易金额，也要记录过程金额
    - 例如，商品分摊的优惠金额、积分、支付金额、应付等
      - 积分互换
    - 退换货、风控、财务等会用到
  - Time
    - 全生命周期的状态节点触发时间
    - 与状态一起表示它发生的时间语义
- Interact with stock
  - 避免超卖和少卖
  - 超卖对用户体验差
  - 少卖对商家体验差
- Execution(translation or orchestrator)
  - transport to WMS
  - transport to TMS
  - 履约内容
    - 时效
    - 发票
    - 优惠
    - COD收款
    - 预约
    - 运费
- Push
  - to whom
    - 商家
    - 用户
    - 仓储人员
    - 配送人员
  - how
    - 手机push
    - 站内信
    - callback API
    - SMS
    - wechat

### Characteriscs

#### Variety(多样性)

例如
- 地址信息，O2O订单，会多出自提点地址
- 支付信息，如果COD
- 缺货处理
  - 部分缺货
- 预售，团购，拼单，拆单，合单，集单，憋单
- 来源
  - 导入
  - API
  - 页面手工建单
  - 内部测试UAT
  - 自动生成订单
  - 无界(新)零售
    - 意味着到处都可能是入口

#### 正向、逆向、调拨、盘点和搬仓混在一起，有race condition

取消可能包括如下场景，同时包括整单取消和部分取消：
- 买家取消
- 卖家取消
- WMS取消
- TMS取消
- 风控取消
- 客服取消
- 超时未支付取消
- 换货报缺
- 仲裁取消

#### 模型复杂

- 父子单(拆单)
- 主赠关系
- 团单与子单

#### 依赖多

- 内部依赖
  - 强依赖
    - 商品
    - 主数据
    - 客户中心
    - 库存
    - 产品中心
    - 时效
      - 控制下发WMS的时机
      - 控制下发TMS的时机
    - 预分拣
    - 台账
    - 风控
    - VMI
    - GIS
    - 优惠券
    - 账户中心
  - 弱依赖
    - 清关
    - 大件预约
    - 发票系统
    - 计费系统
    - 售后系统
    - 订单异常中心
    - 保价系统
    - TMS
    - WMS
      - 仓间调拨
- 外部依赖
  - 第三方承运商
  - 第三方WMS
  - 第三方支付

## Core tech building blocks

- [ ] 各种语义的分布式锁
  - 幂等性
  - 唯一性
  - 防并发锁
  - 防重锁
  - 时间区间锁
  - 分布式Latch
- [X] 定时任务平台
- [ ] 可靠的MQ发送机制
- [X] FSM
  - 订单状态是交易进展的反馈，是订单流程的一个个连接点
  - 不同类型订单的状态机不同
- [X] 统一异常中心
  - 异常的定义平台化
  - 异常处理平台化、流程化
  - 异常的自我解释，自我定位
  - 主动和被动的异常监控
- [X] 订单全流程跟踪
- [X] 异步接单框架 task
- [X] 接单与落库解耦
  - 提高接单能力
- [X] 流程引擎，流程编排
- [X] 业务扩展点机制
  - 微内核
  - 不能让接单系统成为瓶颈
  - 打破Conway law
- [X] 查询
  - 读写分离
  - 按订单号查询
    - 动静分离
  - 搜索引擎
- [X] FlexDB
  - 解决个性化扩展字段问题
- [ ] TCC
  - 解决复杂场景下数据一致性问题
- [X] Graceful shutdown
- [X] Custom metrics reporter
  - central reporter
- [ ] UAT环境
  - 全链路压测
  - profiler
- [ ] 动态分组(请求路由)
  - 降级方案
  - 请求分发机制

## Design

### 带着问题思考

- 多层接单架构
  - 统一接单
  - 订单中间件接单
- 多渠道隔离
- 2B和2C的订单是否统一存放和处理
- 订单类型是否分层
  - 前端下单与后端生产分离

### 订单形态

- 交易单(用户下达的指令)
- 计划单(生产计划单)
  - 订单拆分
  - 订单转移
- 生产单(生产运营单)

### Landscape

### 核心流程

- 接单
- 订单履约
- 数据浏览

## TODO

- 订单的分配

