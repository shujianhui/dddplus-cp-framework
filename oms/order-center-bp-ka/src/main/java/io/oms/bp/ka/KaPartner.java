package io.oms.bp.ka;

import io.oms.cp.spec.model.IOrderMain;
import io.dddplus.annotation.Partner;
import io.dddplus.ext.IIdentityResolver;

@Partner(code = KaPartner.CODE, name = "KA业务前台")
public class KaPartner implements IIdentityResolver<IOrderMain> {
    public static final String CODE = "KA";

    @Override
    public boolean match(IOrderMain model) {
        if (model.getSource() == null) {
            return false;
        }

        return model.getSource().equalsIgnoreCase(CODE);
    }
}
