package io.oms.bp.ka.extension;

import io.oms.bp.ka.KaPartner;
import io.dddplus.annotation.Extension;
import lombok.extern.slf4j.Slf4j;
import io.oms.cp.spec.ext.IReviseStepsExt;
import io.oms.cp.spec.model.IOrderMain;

import java.util.List;

@Slf4j
@Extension(code = KaPartner.CODE, value = "kaReviseStepsExt")
public class ReviseStepsExt implements IReviseStepsExt {

    @Override
    public List<String> reviseSteps(IOrderMain model) {
        log.info("KA will not revise subsequent steps");
        return null;
    }
}
