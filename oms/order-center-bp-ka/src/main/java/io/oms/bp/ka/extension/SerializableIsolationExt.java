package io.oms.bp.ka.extension;

import io.oms.bp.ka.KaPartner;
import lombok.extern.slf4j.Slf4j;
import io.dddplus.annotation.Extension;
import io.oms.cp.spec.ext.ISerializableIsolationExt;
import io.oms.cp.spec.model.IOrderMain;
import io.oms.cp.spec.model.vo.LockEntry;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

@Extension(code = KaPartner.CODE, value = "kaSerializableIsolationExt")
@Slf4j
public class SerializableIsolationExt implements ISerializableIsolationExt {

    @Override
    public LockEntry createLockEntry(@NotNull IOrderMain model) {
        log.info("KA的锁TTL大一些");
        return new LockEntry(model.customerProvidedOrderNo(), 11, TimeUnit.MINUTES);
    }
}
