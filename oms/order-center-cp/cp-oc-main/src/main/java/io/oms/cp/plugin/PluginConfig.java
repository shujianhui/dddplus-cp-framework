package io.oms.cp.plugin;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"io.oms.cp", "io.oms.d"})
public class PluginConfig {
}
