package io.oms.cp.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"io.oms.cp", "io.oms.bp", "io.oms.d"})
public class AppConfig {
}
