package io.oms.cp.domain.step.submitorder;

import io.oms.cp.domain.model.OrderMain;
import io.oms.cp.domain.step.SubmitOrderStep;
import io.dddplus.annotation.Step;
import io.oms.cp.spec.Steps;
import io.oms.cp.spec.exception.OrderException;
import io.oms.d.stock.spec.service.IStockService;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

@Step(value = "stockStep")
public class StockStep extends SubmitOrderStep {

    // 演示：通过库存支撑域来为订单核心域提供方便的服务
    @Resource
    private IStockService stockService;
    
    @Override
    public void execute(@NotNull OrderMain model) throws OrderException {
        stockService.occupyStock(model);
    }

    @Override
    public String stepCode() {
        return Steps.SubmitOrder.StockStep;
    }
}
