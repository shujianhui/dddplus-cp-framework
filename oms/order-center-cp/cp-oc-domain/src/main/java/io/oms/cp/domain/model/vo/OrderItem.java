package io.oms.cp.domain.model.vo;

import io.oms.cp.domain.model.OrderMain;
import lombok.Data;
import io.oms.cp.spec.model.vo.IOrderItem;

import java.math.BigDecimal;

/**
 * 订单项.
 *
 * <p>每个{@link OrderMain}包含多个订单项.</p>
 */
@Data
public class OrderItem implements IOrderItem {
    private String sku;
    private Integer quantity;
    private String orderLine;
    private BigDecimal price;
}
