package io.oms.cp.domain.facade.repository;

import io.oms.cp.domain.model.OrderMain;

import javax.validation.constraints.NotNull;

public interface IOrderRepository {

    void persist(@NotNull OrderMain orderModel);

    OrderMain getOrder(@NotNull Long orderId);
}
