package io.oms.cp.domain.step;

import io.oms.cp.domain.model.OrderMain;
import io.oms.cp.spec.exception.OrderException;
import io.dddplus.step.IDomainStep;
import io.oms.cp.spec.Steps;

public abstract class CancelOrderStep implements IDomainStep<OrderMain, OrderException> {

    @Override
    public String activityCode() {
        return Steps.CancelOrder.Activity;
    }

}
