package io.oms.cp.domain.facade.lock;

import io.oms.cp.domain.facade.cache.IRedisClient;
import io.oms.cp.spec.model.vo.LockEntry;

import java.util.concurrent.locks.Lock;

public interface IRedisLockFactory {

    /**
     * Create a mutex lock.
     */
    Lock create(IRedisClient redisClient, LockEntry lockEntry);
}
