package io.oms.cp.domain.step.cancelorder;

import io.oms.cp.domain.model.OrderMain;
import io.oms.cp.domain.step.CancelOrderStep;
import lombok.extern.slf4j.Slf4j;
import io.oms.cp.spec.exception.OrderException;
import io.oms.cp.spec.Steps;
import io.dddplus.annotation.Step;

import javax.validation.constraints.NotNull;

@Step(value = "cancelBasicStep")
@Slf4j
public class BasicStep extends CancelOrderStep {

    @Override
    public void execute(@NotNull OrderMain model) throws OrderException {

    }

    @Override
    public String stepCode() {
        return Steps.CancelOrder.BasicStep;
    }
}
