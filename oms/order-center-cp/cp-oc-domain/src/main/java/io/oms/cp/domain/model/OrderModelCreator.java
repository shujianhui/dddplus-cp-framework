package io.oms.cp.domain.model;

import io.dddplus.api.RequestProfile;
import io.dddplus.model.IDomainModelCreator;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import io.oms.cp.domain.model.vo.OrderItem;
import io.oms.cp.domain.model.vo.Product;

import java.util.List;

@Getter
@Setter
@ToString
public class OrderModelCreator implements IDomainModelCreator {
    private Long id;

    private RequestProfile requestProfile;

    /**
     * 订单来源
     */
    private String source;

    /**
     * 客户编号.
     */
    private String customerNo;

    /**
     * 客户携带的外部单号.
     */
    private String externalNo;

    private List<OrderItem> orderItems;

    private List<Product> products;
}
