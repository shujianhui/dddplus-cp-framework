package io.oms.cp.domain.step.submitorder;

import io.oms.cp.domain.facade.mq.IMessageProducer;
import io.oms.cp.domain.model.OrderMain;
import io.oms.cp.domain.step.SubmitOrderStep;
import io.oms.cp.spec.exception.OrderException;
import io.oms.cp.spec.Steps;
import io.dddplus.annotation.Step;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

@Step(value = "submitMqStep")
public class BroadcastStep extends SubmitOrderStep {

    @Resource
    private IMessageProducer messageProducer;
    
    @Override
    public void execute(@NotNull OrderMain model) throws OrderException {
        messageProducer.produce(model);
    }

    @Override
    public String stepCode() {
        return Steps.SubmitOrder.BroadcastStep;
    }
}
