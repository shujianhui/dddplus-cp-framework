package io.oms.cp.domain.ability;

import io.dddplus.annotation.DomainAbility;
import io.dddplus.runtime.BaseDomainAbility;
import io.oms.cp.domain.CoreDomain;
import io.oms.cp.spec.ext.IPresortExt;
import io.oms.cp.spec.model.IOrderMain;

import javax.validation.constraints.NotNull;

@DomainAbility(domain = CoreDomain.CODE, name = "预分拣的能力")
public class PresortAbility extends BaseDomainAbility<IOrderMain, IPresortExt> {

    public void presort(@NotNull IOrderMain model) {
        firstExtension(model).presort(model);
    }

    @Override
    public IPresortExt defaultExtension(@NotNull IOrderMain model) {
        return null;
    }
}
