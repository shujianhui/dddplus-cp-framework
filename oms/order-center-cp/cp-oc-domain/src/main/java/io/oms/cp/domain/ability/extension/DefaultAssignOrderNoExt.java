package io.oms.cp.domain.ability.extension;

import io.dddplus.annotation.Extension;
import lombok.extern.slf4j.Slf4j;
import io.oms.cp.domain.specification.ProductNotEmptySpec;
import io.oms.cp.spec.ext.IAssignOrderNoExt;
import io.oms.cp.spec.model.IOrderMain;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

@Extension(code = IAssignOrderNoExt.DefaultCode, value = "defaultAssignOrderNoExt")
@Slf4j
public class DefaultAssignOrderNoExt implements IAssignOrderNoExt {

    @Resource
    private ProductNotEmptySpec productNotEmptySpec;

    @Override
    public void assignOrderNo(@NotNull IOrderMain model) {
        // 演示调用业务约束的使用：把implicit business rules变成explicit
        if (!productNotEmptySpec.satisfiedBy(model)) {
            log.warn("Spec:{} not satisfied", productNotEmptySpec);
            //throw new OrderException(OrderErrorReason.SubmitOrder.ProductEmpty);
        }

    }
}
