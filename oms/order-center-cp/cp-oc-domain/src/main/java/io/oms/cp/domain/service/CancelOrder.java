package io.oms.cp.domain.service;

import io.oms.cp.domain.model.OrderMain;
import io.oms.cp.domain.step.CancelOrderStepsExec;
import io.oms.cp.domain.CoreDomain;
import io.oms.cp.domain.ability.DecideStepsAbility;
import io.oms.cp.domain.ability.SerializableIsolationAbility;
import io.oms.cp.spec.exception.OrderErrorReason;
import io.oms.cp.spec.exception.OrderException;
import io.dddplus.annotation.DomainService;
import io.dddplus.model.IDomainService;
import io.dddplus.runtime.DDD;
import lombok.extern.slf4j.Slf4j;
import io.oms.cp.spec.Steps;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.concurrent.locks.Lock;

@DomainService(domain = CoreDomain.CODE)
@Slf4j
public class CancelOrder implements IDomainService {

    @Resource
    private CancelOrderStepsExec cancelOrderStepsExec;

    public void submit(@NotNull OrderMain orderModel) throws OrderException {
        Lock lock = DDD.findAbility(SerializableIsolationAbility.class).acquireLock(orderModel);
        if (SerializableIsolationAbility.useLock(lock) && !lock.tryLock()) {
            // 存在并发
            throw new OrderException(OrderErrorReason.SubmitOrder.OrderConcurrentNotAllowed);
        }

        List<String> steps = DDD.findAbility(DecideStepsAbility.class).decideSteps(orderModel, Steps.CancelOrder.Activity);
        cancelOrderStepsExec.execute(Steps.CancelOrder.Activity, steps, orderModel);
    }
}
