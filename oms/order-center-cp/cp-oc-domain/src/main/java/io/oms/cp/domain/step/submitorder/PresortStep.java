package io.oms.cp.domain.step.submitorder;

import io.oms.cp.domain.model.OrderMain;
import io.oms.cp.domain.step.SubmitOrderStep;
import io.dddplus.annotation.Step;
import io.oms.cp.spec.Steps;
import io.oms.cp.spec.exception.OrderException;

import javax.validation.constraints.NotNull;

@Step(value = "submitPresortStep", name = "预分拣步骤")
public class PresortStep extends SubmitOrderStep {

    @Override
    public void execute(@NotNull OrderMain model) throws OrderException {
        // TODO 把预分拣扩展点从BasicStep移动到这里
    }

    @Override
    public String stepCode() {
        return Steps.SubmitOrder.PresortStep;
    }
}
