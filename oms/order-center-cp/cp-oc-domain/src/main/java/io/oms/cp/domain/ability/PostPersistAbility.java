package io.oms.cp.domain.ability;

import io.oms.cp.domain.CoreDomain;
import io.dddplus.annotation.DomainAbility;
import io.dddplus.runtime.BaseDomainAbility;
import io.oms.cp.spec.ext.IPostPersistExt;
import io.oms.cp.spec.model.IOrderMain;

import javax.validation.constraints.NotNull;

@DomainAbility(domain = CoreDomain.CODE, name = "落库后的扩展能力")
public class PostPersistAbility extends BaseDomainAbility<IOrderMain, IPostPersistExt> {

    public void afterPersist(@NotNull IOrderMain model) {
        firstExtension(model).afterPersist(model);
    }

    @Override
    public IPostPersistExt defaultExtension(@NotNull IOrderMain model) {
        return null;
    }
}
