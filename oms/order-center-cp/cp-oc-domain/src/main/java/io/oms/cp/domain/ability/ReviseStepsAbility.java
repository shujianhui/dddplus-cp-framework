package io.oms.cp.domain.ability;

import io.oms.cp.domain.model.OrderMain;
import io.dddplus.annotation.DomainAbility;
import io.dddplus.runtime.BaseDomainAbility;
import io.oms.cp.domain.CoreDomain;
import io.oms.cp.spec.ext.IReviseStepsExt;

import javax.validation.constraints.NotNull;
import java.util.List;

@DomainAbility(domain = CoreDomain.CODE)
public class ReviseStepsAbility extends BaseDomainAbility<OrderMain, IReviseStepsExt> {

    public List<String> revisedSteps(@NotNull OrderMain model) {
        // execute ext with timeout 300ms
        return firstExtension(model, 300).reviseSteps(model);
    }

    @Override
    public IReviseStepsExt defaultExtension(@NotNull OrderMain model) {
        return null;
    }
}
