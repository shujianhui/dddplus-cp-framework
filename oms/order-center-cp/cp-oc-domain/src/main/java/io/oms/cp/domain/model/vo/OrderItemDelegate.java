package io.oms.cp.domain.model.vo;

import io.oms.cp.domain.model.OrderModelCreator;
import io.oms.cp.spec.model.vo.IOrderItem;
import io.oms.cp.spec.model.vo.IOrderItemDelegate;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class OrderItemDelegate implements IOrderItemDelegate {

    private List<OrderItem> items;

    private OrderItemDelegate() {}

    public static OrderItemDelegate createWith(@NotNull OrderModelCreator creator) {
        OrderItemDelegate delegate = new OrderItemDelegate();
        delegate.items = new ArrayList<>();
        return delegate;
    }

    @Override
    public List<? extends IOrderItem> getItems() {
        return items;
    }
}
