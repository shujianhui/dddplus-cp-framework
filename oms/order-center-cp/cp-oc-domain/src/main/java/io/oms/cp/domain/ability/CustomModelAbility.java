package io.oms.cp.domain.ability;

import io.oms.cp.domain.CoreDomain;
import io.dddplus.annotation.DomainAbility;
import io.dddplus.ext.IModelAttachmentExt;
import io.dddplus.runtime.BaseDomainAbility;
import io.oms.cp.spec.model.IOrderMain;

import javax.validation.constraints.NotNull;

@DomainAbility(domain = CoreDomain.CODE)
public class CustomModelAbility extends BaseDomainAbility<IOrderMain, IModelAttachmentExt> {

    public void explain(@NotNull IOrderMain model) {
        firstExtension(model).explain(model.requestProfile(), model);
    }

    @Override
    public IModelAttachmentExt defaultExtension(IOrderMain model) {
        return null;
    }
}
