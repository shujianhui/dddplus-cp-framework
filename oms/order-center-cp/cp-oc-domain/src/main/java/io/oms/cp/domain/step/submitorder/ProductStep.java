package io.oms.cp.domain.step.submitorder;

import io.oms.cp.domain.model.OrderMain;
import io.oms.cp.domain.step.SubmitOrderStep;
import lombok.extern.slf4j.Slf4j;
import io.oms.cp.spec.exception.OrderException;
import io.oms.cp.spec.Steps;
import io.dddplus.annotation.Step;

import javax.validation.constraints.NotNull;

@Step(value = "submitProductStep", name = "订单里产品校验", tags = Steps.Tags.Product)
@Slf4j
public class ProductStep extends SubmitOrderStep {

    @Override
    public void execute(@NotNull OrderMain model) throws OrderException {
    }

    @Override
    public String stepCode() {
        return Steps.SubmitOrder.ProductStep;
    }
}
