package io.oms.cp.domain.service;

import io.oms.cp.domain.model.OrderMain;
import io.oms.cp.domain.step.SubmitOrderStepsExec;
import io.oms.cp.domain.ability.PostPersistAbility;
import io.oms.cp.domain.ability.SerializableIsolationAbility;
import io.dddplus.annotation.DomainService;
import io.dddplus.model.IDomainService;
import io.dddplus.runtime.DDD;
import lombok.extern.slf4j.Slf4j;
import io.oms.cp.domain.CoreDomain;
import io.oms.cp.domain.ability.DecideStepsAbility;
import io.oms.cp.spec.exception.OrderErrorReason;
import io.oms.cp.spec.exception.OrderException;
import io.oms.cp.spec.Steps;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.concurrent.locks.Lock;

@DomainService(domain = CoreDomain.CODE)
@Slf4j
public class SubmitOrder implements IDomainService {

    @Resource
    private SubmitOrderStepsExec submitOrderStepsExec;

    public void submit(@NotNull OrderMain orderModel) throws OrderException {
        // 先通过防并发扩展点防止一个订单多次处理：但防并发逻辑在不同场景下不同
        // 同时，也希望研发清楚：扩展点不是绑定到领域步骤的，它可以在任何地方使用！
        Lock lock = DDD.findAbility(SerializableIsolationAbility.class).acquireLock(orderModel);
        if (!SerializableIsolationAbility.useLock(lock)) {
            log.info("will not use lock");
        } else if (!lock.tryLock()) {
            // 存在并发
            throw new OrderException(OrderErrorReason.SubmitOrder.OrderConcurrentNotAllowed);
        }

        // 不同场景下，接单的执行步骤不同：通过扩展点实现业务的多态
        List<String> steps = DDD.findAbility(DecideStepsAbility.class).decideSteps(orderModel, Steps.SubmitOrder.Activity);
        log.info("steps {}", steps);

        if (steps != null && !steps.isEmpty()) {
            // 通过步骤编排的模板方法执行每一个步骤，其中涉及到：步骤回滚，步骤重新编排
            submitOrderStepsExec.execute(Steps.SubmitOrder.Activity, steps, orderModel);
        }

        DDD.findAbility(PostPersistAbility.class).afterPersist(orderModel);

        log.info("接单完毕！");
    }
}
