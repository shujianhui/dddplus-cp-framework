package io.oms.cp.domain.ability;

import io.oms.cp.domain.CoreDomain;
import io.oms.cp.domain.ability.extension.DefaultAssignOrderNoExt;
import io.oms.cp.spec.ext.IAssignOrderNoExt;
import io.oms.cp.spec.model.IOrderMain;
import io.dddplus.annotation.DomainAbility;
import io.dddplus.runtime.BaseDomainAbility;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

@DomainAbility(domain = CoreDomain.CODE, name = "分配订单号的能力")
public class AssignOrderNoAbility extends BaseDomainAbility<IOrderMain, IAssignOrderNoExt> {

    @Resource
    private DefaultAssignOrderNoExt defaultAssignOrderNoExt;

    public void assignOrderNo(@NotNull IOrderMain model) {
        firstExtension(model).assignOrderNo(model);
    }

    @Override
    public IAssignOrderNoExt defaultExtension(@NotNull IOrderMain model) {
        return defaultAssignOrderNoExt;
    }
}
