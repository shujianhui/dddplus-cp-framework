package io.oms.cp.domain.ability;

import io.oms.cp.domain.CoreDomain;
import io.oms.cp.domain.facade.cache.IRedisClient;
import io.oms.cp.domain.facade.lock.IRedisLockFactory;
import lombok.extern.slf4j.Slf4j;
import io.oms.cp.spec.ext.ISerializableIsolationExt;
import io.oms.cp.spec.model.IOrderMain;
import io.oms.cp.spec.model.vo.LockEntry;
import io.dddplus.annotation.DomainAbility;
import io.dddplus.runtime.BaseDomainAbility;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.concurrent.locks.Lock;

@DomainAbility(domain = CoreDomain.CODE, name = "订单串行化隔离的能力")
@Slf4j
public class SerializableIsolationAbility extends BaseDomainAbility<IOrderMain, ISerializableIsolationExt> {
    private static final Lock withoutLock = null;

    @Resource
    private IRedisLockFactory redisLockFactory;

    @Resource
    private IRedisClient redisClient;

    public Lock acquireLock(@NotNull IOrderMain model) {
        LockEntry lockEntry = firstExtension(model).createLockEntry(model);
        if (lockEntry == null) {
            return withoutLock;
        }

        // 为了避免前台锁key冲突，中台统一加锁前缀，隔离不同的业务前台
        lockEntry.withPrefix(model.getCustomerNo());
        log.info("key:{} expires:{} {}", lockEntry.lockKey(), lockEntry.getLeaseTime(), lockEntry.getTimeUnit());
        return redisLockFactory.create(redisClient, lockEntry);
    }

    public static boolean useLock(Lock lock) {
        return lock != withoutLock;
    }

    @Override
    public ISerializableIsolationExt defaultExtension(@NotNull IOrderMain model) {
        // 默认不防并发
        return null;
    }
}
