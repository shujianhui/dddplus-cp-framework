package io.oms.cp.domain.specification;

import io.dddplus.annotation.Specification;
import io.dddplus.specification.ISpecification;
import io.dddplus.specification.Notification;
import io.oms.cp.spec.model.IOrderMain;

// 之前的implicit business rules，现在变成了explicit rules
@Specification("产品项不能空")
public class ProductNotEmptySpec implements ISpecification<IOrderMain> {

    @Override
    public boolean satisfiedBy(IOrderMain candidate, Notification notification) {
        if (candidate.productDelegate() == null || candidate.productDelegate().getProducts() == null || candidate.productDelegate().getProducts().isEmpty()) {
            return false;
        }

        return true;
    }
}
