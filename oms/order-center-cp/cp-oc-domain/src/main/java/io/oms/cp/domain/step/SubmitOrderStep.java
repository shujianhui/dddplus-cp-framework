package io.oms.cp.domain.step;

import io.oms.cp.domain.model.OrderMain;
import io.dddplus.step.IRevokableDomainStep;
import io.oms.cp.spec.exception.OrderException;
import io.oms.cp.spec.Steps;

import javax.validation.constraints.NotNull;

public abstract class SubmitOrderStep implements IRevokableDomainStep<OrderMain, OrderException> {

    @Override
    public String activityCode() {
        return Steps.SubmitOrder.Activity;
    }

    @Override
    public void rollback(@NotNull OrderMain model, @NotNull OrderException cause) {
        // 默认不回滚，子类可以通过覆盖实现对应步骤的回滚逻辑
    }
}
