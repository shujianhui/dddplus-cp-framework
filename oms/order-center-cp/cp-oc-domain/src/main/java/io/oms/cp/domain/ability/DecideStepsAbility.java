package io.oms.cp.domain.ability;

import io.dddplus.annotation.DomainAbility;
import io.dddplus.runtime.BaseDecideStepsAbility;
import io.oms.cp.domain.CoreDomain;
import io.oms.cp.spec.DomainAbilities;
import io.oms.cp.spec.model.IOrderMain;

@DomainAbility(domain = CoreDomain.CODE, name = "动态决定领域步骤的能力", tags = DomainAbilities.decideSteps)
public class DecideStepsAbility extends BaseDecideStepsAbility<IOrderMain> {
}
