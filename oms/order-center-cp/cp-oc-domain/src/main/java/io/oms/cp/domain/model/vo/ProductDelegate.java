package io.oms.cp.domain.model.vo;

import io.oms.cp.domain.model.OrderModelCreator;
import io.oms.cp.spec.model.vo.IProduct;
import io.oms.cp.spec.model.vo.IProductDelegate;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class ProductDelegate implements IProductDelegate {

    private List<Product> products;

    private ProductDelegate() {}

    public static ProductDelegate createWith(@NotNull OrderModelCreator creator) {
        ProductDelegate delegate = new ProductDelegate();
        delegate.products = new ArrayList<>();
        return delegate;
    }

    @Override
    public List<? extends IProduct> getProducts() {
        return products;
    }
}
