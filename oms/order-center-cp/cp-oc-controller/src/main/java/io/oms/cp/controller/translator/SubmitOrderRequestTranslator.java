package io.oms.cp.controller.translator;

import io.dddplus.IBaseTranslator;
import io.oms.cp.controller.dto.SubmitOrderRequest;
import io.oms.cp.domain.model.OrderModelCreator;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SubmitOrderRequestTranslator extends IBaseTranslator<SubmitOrderRequest, OrderModelCreator> {
    SubmitOrderRequestTranslator instance = Mappers.getMapper(SubmitOrderRequestTranslator.class);
}
