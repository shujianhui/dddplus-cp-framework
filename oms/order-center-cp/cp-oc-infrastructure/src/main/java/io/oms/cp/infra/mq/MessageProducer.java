package io.oms.cp.infra.mq;

import lombok.extern.slf4j.Slf4j;
import io.oms.cp.domain.facade.mq.IMessageProducer;
import io.oms.cp.domain.model.OrderMain;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
@Slf4j
public class MessageProducer implements IMessageProducer {

    @Override
    public void produce(@NotNull OrderMain orderModel) {
        log.info("已经发送给MQ：{}", orderModel);
    }
}
