package io.oms.cp.infra.dao.mock;

import io.oms.cp.infra.dao.OrderItemDao;
import io.oms.cp.infra.po.OrderItemData;
import org.springframework.stereotype.Component;

import java.util.List;

// 实际项目，可以使用MyBatis/Hibernate/JPA等
@Component
public class MockOrderItemDao implements OrderItemDao {

    @Override
    public List<OrderItemData> itemsOfOrder(Long orderId) {
        return null;
    }
}
