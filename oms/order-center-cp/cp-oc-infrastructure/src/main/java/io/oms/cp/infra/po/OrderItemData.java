package io.oms.cp.infra.po;

import lombok.Data;

@Data
public class OrderItemData {
    private Long orderId;
}
