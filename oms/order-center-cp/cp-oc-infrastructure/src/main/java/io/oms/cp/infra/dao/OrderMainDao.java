package io.oms.cp.infra.dao;

import io.oms.cp.infra.po.OrderMainData;

public interface OrderMainDao {

    void insert(OrderMainData orderMainData);

    OrderMainData getById(Long id);
}
