package io.oms.cp.infra.translator;

import io.oms.cp.infra.po.OrderMainData;
import io.dddplus.IBaseTranslator;
import io.oms.cp.domain.model.OrderMain;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(
        unmappedSourcePolicy = ReportingPolicy.WARN,
        unmappedTargetPolicy = ReportingPolicy.WARN,
        typeConversionPolicy = ReportingPolicy.ERROR
)
public interface Model2Data extends IBaseTranslator<OrderMain, OrderMainData> {

    Model2Data instance = Mappers.getMapper(Model2Data.class);

    @Override
    OrderMainData translate(OrderMain orderModel);
}
