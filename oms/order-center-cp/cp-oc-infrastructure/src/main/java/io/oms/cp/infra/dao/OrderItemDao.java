package io.oms.cp.infra.dao;

import io.oms.cp.infra.po.OrderItemData;

import java.util.List;

public interface OrderItemDao {

    List<OrderItemData> itemsOfOrder(Long orderId);
}
