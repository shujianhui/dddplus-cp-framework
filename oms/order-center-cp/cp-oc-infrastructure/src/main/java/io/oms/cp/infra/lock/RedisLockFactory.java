package io.oms.cp.infra.lock;

import io.oms.cp.domain.facade.cache.IRedisClient;
import io.oms.cp.domain.facade.lock.IRedisLockFactory;
import io.oms.cp.spec.model.vo.LockEntry;
import org.springframework.stereotype.Component;

import java.util.concurrent.locks.Lock;

@Component
public class RedisLockFactory implements IRedisLockFactory {

    @Override
    public Lock create(IRedisClient redisClient, LockEntry lockEntry) {
        return new RedisLock(redisClient, lockEntry);
    }
}
