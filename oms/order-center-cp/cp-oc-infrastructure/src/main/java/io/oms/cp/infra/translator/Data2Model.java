package io.oms.cp.infra.translator;

import io.oms.cp.domain.model.OrderModelCreator;
import io.oms.cp.infra.po.OrderItemData;
import io.oms.cp.infra.po.OrderMainData;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface Data2Model {

    Data2Model instance = Mappers.getMapper(Data2Model.class);

    OrderModelCreator translate(OrderMainData orderMainData, @Context List<OrderItemData> orderItemData);
}
