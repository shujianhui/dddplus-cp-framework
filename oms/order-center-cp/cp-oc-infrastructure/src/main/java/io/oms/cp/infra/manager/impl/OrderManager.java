package io.oms.cp.infra.manager.impl;

import io.oms.cp.infra.dao.OrderMainDao;
import io.oms.cp.infra.manager.IOrderManager;
import io.oms.cp.infra.po.OrderMainData;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Component
public class OrderManager implements IOrderManager {

    @Resource
    private OrderMainDao orderMainDao;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insert(OrderMainData orderMainData) {
        orderMainDao.insert(orderMainData);
    }
}
