package io.oms.cp.infra.manager;

import io.oms.cp.infra.po.OrderMainData;

public interface IOrderManager {

    void insert(OrderMainData orderMainData);
}
