package io.oms.cp.infra.resource;

import lombok.extern.slf4j.Slf4j;
import io.oms.cp.spec.resource.IStockRpc;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class StockRpc implements IStockRpc {
    @Override
    public boolean preOccupyStock(String sku) {
        // 真实场景，会通过RPC/RESTful接口调用“库存中心”的服务接口
        // 这里仅仅是mock
        log.info("预占库存：{}", sku);
        return true;
    }
}
