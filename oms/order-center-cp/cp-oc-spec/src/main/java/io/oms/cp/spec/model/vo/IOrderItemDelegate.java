package io.oms.cp.spec.model.vo;

import java.util.List;

public interface IOrderItemDelegate {
    List<? extends IOrderItem> getItems();
}
