package io.oms.cp.spec.ext;

import io.oms.cp.spec.model.IOrderMain;
import io.dddplus.ext.IDomainExtension;

import javax.validation.constraints.NotNull;

/**
 * 落库后的处理扩展点.
 */
public interface IPostPersistExt extends IDomainExtension {

    void afterPersist(@NotNull IOrderMain model);
}
