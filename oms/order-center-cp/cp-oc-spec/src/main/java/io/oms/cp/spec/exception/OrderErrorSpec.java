package io.oms.cp.spec.exception;

public interface OrderErrorSpec {
    String code();
}
