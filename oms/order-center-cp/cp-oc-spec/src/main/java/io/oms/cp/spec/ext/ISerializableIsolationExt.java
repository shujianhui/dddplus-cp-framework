package io.oms.cp.spec.ext;

import io.oms.cp.spec.model.IOrderMain;
import io.oms.cp.spec.model.vo.LockEntry;
import io.dddplus.ext.IDomainExtension;

import javax.validation.constraints.NotNull;

/**
 * 订单串行化隔离的扩展点声明，即防并发锁.
 */
public interface ISerializableIsolationExt extends IDomainExtension {

    /**
     * 获取防并发锁的信息.
     *
     * @param model
     * @return lock entry object. if null, 不需要防并发
     */
    LockEntry createLockEntry(@NotNull IOrderMain model);

}
