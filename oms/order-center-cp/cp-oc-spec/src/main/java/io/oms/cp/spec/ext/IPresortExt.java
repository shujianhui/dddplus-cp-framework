package io.oms.cp.spec.ext;

import io.oms.cp.spec.model.IOrderMain;
import io.dddplus.ext.IDomainExtension;

import javax.validation.constraints.NotNull;

/**
 * 预分拣扩展点.
 * <p>
 * <p>根据配送地址获取配送站点信息，进而获取波次配置</p>
 */
public interface IPresortExt extends IDomainExtension {

    void presort(@NotNull IOrderMain model);
}
