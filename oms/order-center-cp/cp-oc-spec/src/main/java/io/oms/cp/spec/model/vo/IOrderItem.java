package io.oms.cp.spec.model.vo;

public interface IOrderItem {

    String getSku();
    Integer getQuantity();

}
