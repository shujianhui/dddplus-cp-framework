package io.oms.cp.spec.ext;

import io.oms.cp.spec.model.IOrderMain;
import io.dddplus.ext.IDomainExtension;

import javax.validation.constraints.NotNull;

/**
 * 敏感词信息获取.
 */
public interface ISensitiveWordsExt extends IDomainExtension {

    String[] extract(@NotNull IOrderMain model);
}
