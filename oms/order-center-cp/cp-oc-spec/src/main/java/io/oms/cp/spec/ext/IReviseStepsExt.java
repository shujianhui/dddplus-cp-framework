package io.oms.cp.spec.ext;

import io.oms.cp.spec.model.IOrderMain;
import io.dddplus.ext.IDomainExtension;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface IReviseStepsExt extends IDomainExtension {

    List<String> reviseSteps(@NotNull IOrderMain model);

}
