package io.oms.cp.spec.ext;

import io.oms.cp.spec.model.IOrderMain;
import io.dddplus.ext.IDomainExtension;

import javax.validation.constraints.NotNull;

/**
 * 生成、分配订单号扩展点.
 */
public interface IAssignOrderNoExt extends IDomainExtension {

    void assignOrderNo(@NotNull IOrderMain model);

}
