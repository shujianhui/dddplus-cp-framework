package io.oms.bp.isv.extension;

import io.dddplus.annotation.Extension;
import io.oms.bp.isv.IsvPartner;
import io.oms.cp.spec.ext.ISerializableIsolationExt;
import io.oms.cp.spec.model.IOrderMain;
import io.oms.cp.spec.model.vo.LockEntry;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

@Extension(code = IsvPartner.CODE, value = "isvSerializableIsolationExt", name = "ISV场景的订单锁机制")
public class SerializableIsolationExt implements ISerializableIsolationExt {

    @Override
    public LockEntry createLockEntry(@NotNull IOrderMain model) {
        return new LockEntry(model.customerProvidedOrderNo(), 50, TimeUnit.MINUTES);
    }
}
