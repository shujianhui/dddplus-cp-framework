package io.oms.bp.isv;

import lombok.extern.slf4j.Slf4j;
import io.oms.cp.spec.model.IOrderMain;
import io.dddplus.annotation.Partner;
import io.dddplus.ext.IIdentityResolver;
import org.springframework.beans.factory.DisposableBean;

@Partner(code = IsvPartner.CODE, name = "ISV业务前台")
@Slf4j
public class IsvPartner implements IIdentityResolver<IOrderMain>, DisposableBean {
    public static final String CODE = "ISV";

    public IsvPartner() {
        // hook how Spring create bean instance
        log.info("ISV new instanced, cl:{}", this.getClass().getClassLoader());
    }

    @Override
    public boolean match(IOrderMain model) {
        if (model.getSource() == null) {
            return false;
        }

        return model.getSource().equalsIgnoreCase(CODE);
    }

    @Override
    public void destroy() throws Exception {
        log.warn("IsvPartner destroyed");
    }
}
