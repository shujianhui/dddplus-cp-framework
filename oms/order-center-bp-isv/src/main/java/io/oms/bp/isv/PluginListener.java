package io.oms.bp.isv;

import lombok.extern.slf4j.Slf4j;
import io.dddplus.plugin.IContainerContext;
import io.dddplus.plugin.IPluginListener;

@Slf4j
public class PluginListener implements IPluginListener {

    @Override
    public void onCommitted(IContainerContext ctx) throws Exception {
        log.info("ISV Jar loaded, ctx:{}", ctx);
    }

    @Override
    public void onPrepared(IContainerContext ctx) throws Exception {

    }
}
