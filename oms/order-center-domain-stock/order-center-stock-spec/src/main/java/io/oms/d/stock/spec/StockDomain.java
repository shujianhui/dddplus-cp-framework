package io.oms.d.stock.spec;

import io.dddplus.annotation.Domain;

@Domain(code = StockDomain.CODE, name = "库存支撑域")
public class StockDomain {
    public static final String CODE = "stock";
}
