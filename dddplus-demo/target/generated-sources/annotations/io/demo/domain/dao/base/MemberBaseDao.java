package io.demo.domain.dao.base;

import static io.demo.domain.helper.MemberDefaults.INSTANCE;

import cn.org.atool.fluent.mybatis.base.dao.BaseDao;
import io.demo.domain.entity.MemberEntity;
import io.demo.domain.mapper.MemberMapper;
import io.demo.domain.wrapper.MemberQuery;
import io.demo.domain.wrapper.MemberUpdate;
import java.lang.Override;
import javax.annotation.Resource;

/**
 *
 * MemberBaseDao
 *
 * @author powered by FluentMybatis
 */
public abstract class MemberBaseDao extends BaseDao<MemberEntity> {
  @Resource(
      name = "memberMapper"
  )
  protected MemberMapper mapper;

  @Override
  public MemberMapper mapper() {
    return mapper;
  }

  @Override
  protected MemberQuery query() {
    return new MemberQuery();
  }

  @Override
  protected MemberQuery defaultQuery() {
    return INSTANCE.defaultQuery();
  }

  @Override
  protected MemberUpdate updater() {
    return new MemberUpdate();
  }

  @Override
  protected MemberUpdate defaultUpdater() {
    return INSTANCE.defaultUpdater();
  }
}
