package io.demo.domain.dao.base;

import static io.demo.domain.helper.MemberLoveDefaults.INSTANCE;

import cn.org.atool.fluent.mybatis.base.dao.BaseDao;
import io.demo.domain.entity.MemberLoveEntity;
import io.demo.domain.mapper.MemberLoveMapper;
import io.demo.domain.wrapper.MemberLoveQuery;
import io.demo.domain.wrapper.MemberLoveUpdate;
import java.lang.Override;
import javax.annotation.Resource;

/**
 *
 * MemberLoveBaseDao
 *
 * @author powered by FluentMybatis
 */
public abstract class MemberLoveBaseDao extends BaseDao<MemberLoveEntity> {
  @Resource(
      name = "memberLoveMapper"
  )
  protected MemberLoveMapper mapper;

  @Override
  public MemberLoveMapper mapper() {
    return mapper;
  }

  @Override
  protected MemberLoveQuery query() {
    return new MemberLoveQuery();
  }

  @Override
  protected MemberLoveQuery defaultQuery() {
    return INSTANCE.defaultQuery();
  }

  @Override
  protected MemberLoveUpdate updater() {
    return new MemberLoveUpdate();
  }

  @Override
  protected MemberLoveUpdate defaultUpdater() {
    return INSTANCE.defaultUpdater();
  }
}
