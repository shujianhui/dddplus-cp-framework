package io.demo.domain.dao.base;

import static io.demo.domain.helper.MemberFavoriteDefaults.INSTANCE;

import cn.org.atool.fluent.mybatis.base.dao.BaseDao;
import io.demo.domain.entity.MemberFavoriteEntity;
import io.demo.domain.mapper.MemberFavoriteMapper;
import io.demo.domain.wrapper.MemberFavoriteQuery;
import io.demo.domain.wrapper.MemberFavoriteUpdate;
import java.lang.Override;
import javax.annotation.Resource;

/**
 *
 * MemberFavoriteBaseDao
 *
 * @author powered by FluentMybatis
 */
public abstract class MemberFavoriteBaseDao extends BaseDao<MemberFavoriteEntity> {
  @Resource(
      name = "memberFavoriteMapper"
  )
  protected MemberFavoriteMapper mapper;

  @Override
  public MemberFavoriteMapper mapper() {
    return mapper;
  }

  @Override
  protected MemberFavoriteQuery query() {
    return new MemberFavoriteQuery();
  }

  @Override
  protected MemberFavoriteQuery defaultQuery() {
    return INSTANCE.defaultQuery();
  }

  @Override
  protected MemberFavoriteUpdate updater() {
    return new MemberFavoriteUpdate();
  }

  @Override
  protected MemberFavoriteUpdate defaultUpdater() {
    return INSTANCE.defaultUpdater();
  }
}
