package io.demo.domain.dao.base;

import static io.demo.domain.helper.CountyDivisionDefaults.INSTANCE;

import cn.org.atool.fluent.mybatis.base.dao.BaseDao;
import io.demo.domain.entity.CountyDivisionEntity;
import io.demo.domain.mapper.CountyDivisionMapper;
import io.demo.domain.wrapper.CountyDivisionQuery;
import io.demo.domain.wrapper.CountyDivisionUpdate;
import java.lang.Override;
import javax.annotation.Resource;

/**
 *
 * CountyDivisionBaseDao
 *
 * @author powered by FluentMybatis
 */
public abstract class CountyDivisionBaseDao extends BaseDao<CountyDivisionEntity> {
  @Resource(
      name = "countyDivisionMapper"
  )
  protected CountyDivisionMapper mapper;

  @Override
  public CountyDivisionMapper mapper() {
    return mapper;
  }

  @Override
  protected CountyDivisionQuery query() {
    return new CountyDivisionQuery();
  }

  @Override
  protected CountyDivisionQuery defaultQuery() {
    return INSTANCE.defaultQuery();
  }

  @Override
  protected CountyDivisionUpdate updater() {
    return new CountyDivisionUpdate();
  }

  @Override
  protected CountyDivisionUpdate defaultUpdater() {
    return INSTANCE.defaultUpdater();
  }
}
