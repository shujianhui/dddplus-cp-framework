package io.demo.domain.dao.base;

import static io.demo.domain.helper.StudentDefaults.INSTANCE;

import cn.org.atool.fluent.mybatis.base.dao.BaseDao;
import io.demo.domain.entity.StudentEntity;
import io.demo.domain.mapper.StudentMapper;
import io.demo.domain.wrapper.StudentQuery;
import io.demo.domain.wrapper.StudentUpdate;
import java.lang.Override;
import javax.annotation.Resource;

/**
 *
 * StudentBaseDao
 *
 * @author powered by FluentMybatis
 */
public abstract class StudentBaseDao extends BaseDao<StudentEntity> {
  @Resource(
      name = "studentMapper"
  )
  protected StudentMapper mapper;

  @Override
  public StudentMapper mapper() {
    return mapper;
  }

  @Override
  protected StudentQuery query() {
    return new StudentQuery();
  }

  @Override
  protected StudentQuery defaultQuery() {
    return INSTANCE.defaultQuery();
  }

  @Override
  protected StudentUpdate updater() {
    return new StudentUpdate();
  }

  @Override
  protected StudentUpdate defaultUpdater() {
    return INSTANCE.defaultUpdater();
  }
}
