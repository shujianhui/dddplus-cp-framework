package io.demo.domain.dao.base;

import static io.demo.domain.helper.UserDefaults.INSTANCE;

import cn.org.atool.fluent.mybatis.base.dao.BaseDao;
import io.demo.domain.entity.UserEntity;
import io.demo.domain.mapper.UserMapper;
import io.demo.domain.wrapper.UserQuery;
import io.demo.domain.wrapper.UserUpdate;
import java.lang.Override;
import javax.annotation.Resource;

/**
 *
 * UserBaseDao
 *
 * @author powered by FluentMybatis
 */
public abstract class UserBaseDao extends BaseDao<UserEntity> {
  @Resource(
      name = "userMapper"
  )
  protected UserMapper mapper;

  @Override
  public UserMapper mapper() {
    return mapper;
  }

  @Override
  protected UserQuery query() {
    return new UserQuery();
  }

  @Override
  protected UserQuery defaultQuery() {
    return INSTANCE.defaultQuery();
  }

  @Override
  protected UserUpdate updater() {
    return new UserUpdate();
  }

  @Override
  protected UserUpdate defaultUpdater() {
    return INSTANCE.defaultUpdater();
  }
}
