package io.demo.domain.dao.base;

import static io.demo.domain.helper.ReceivingAddressDefaults.INSTANCE;

import cn.org.atool.fluent.mybatis.base.dao.BaseDao;
import io.demo.domain.entity.ReceivingAddressEntity;
import io.demo.domain.mapper.ReceivingAddressMapper;
import io.demo.domain.wrapper.ReceivingAddressQuery;
import io.demo.domain.wrapper.ReceivingAddressUpdate;
import java.lang.Override;
import javax.annotation.Resource;

/**
 *
 * ReceivingAddressBaseDao
 *
 * @author powered by FluentMybatis
 */
public abstract class ReceivingAddressBaseDao extends BaseDao<ReceivingAddressEntity> {
  @Resource(
      name = "receivingAddressMapper"
  )
  protected ReceivingAddressMapper mapper;

  @Override
  public ReceivingAddressMapper mapper() {
    return mapper;
  }

  @Override
  protected ReceivingAddressQuery query() {
    return new ReceivingAddressQuery();
  }

  @Override
  protected ReceivingAddressQuery defaultQuery() {
    return INSTANCE.defaultQuery();
  }

  @Override
  protected ReceivingAddressUpdate updater() {
    return new ReceivingAddressUpdate();
  }

  @Override
  protected ReceivingAddressUpdate defaultUpdater() {
    return INSTANCE.defaultUpdater();
  }
}
