package io.demo.domain.dao.base;

import static io.demo.domain.helper.StudentScoreDefaults.INSTANCE;

import cn.org.atool.fluent.mybatis.base.dao.BaseDao;
import io.demo.domain.entity.StudentScoreEntity;
import io.demo.domain.mapper.StudentScoreMapper;
import io.demo.domain.wrapper.StudentScoreQuery;
import io.demo.domain.wrapper.StudentScoreUpdate;
import java.lang.Override;
import javax.annotation.Resource;

/**
 *
 * StudentScoreBaseDao
 *
 * @author powered by FluentMybatis
 */
public abstract class StudentScoreBaseDao extends BaseDao<StudentScoreEntity> {
  @Resource(
      name = "studentScoreMapper"
  )
  protected StudentScoreMapper mapper;

  @Override
  public StudentScoreMapper mapper() {
    return mapper;
  }

  @Override
  protected StudentScoreQuery query() {
    return new StudentScoreQuery();
  }

  @Override
  protected StudentScoreQuery defaultQuery() {
    return INSTANCE.defaultQuery();
  }

  @Override
  protected StudentScoreUpdate updater() {
    return new StudentScoreUpdate();
  }

  @Override
  protected StudentScoreUpdate defaultUpdater() {
    return INSTANCE.defaultUpdater();
  }
}
