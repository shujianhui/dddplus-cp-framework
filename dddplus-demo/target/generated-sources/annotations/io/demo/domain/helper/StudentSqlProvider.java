package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.base.model.InsertList.el;
import static cn.org.atool.fluent.mybatis.mapper.FluentConst.*;
import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.*;
import static cn.org.atool.fluent.mybatis.utility.SqlProviderUtils.*;
import static io.demo.domain.helper.StudentMapping.*;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.InsertList;
import cn.org.atool.fluent.mybatis.base.model.UpdateDefault;
import cn.org.atool.fluent.mybatis.base.model.UpdateSet;
import cn.org.atool.fluent.mybatis.base.provider.BaseSqlProvider;
import cn.org.atool.fluent.mybatis.mapper.MapperSql;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import io.demo.domain.entity.StudentEntity;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * StudentSqlProvider: 动态语句封装
 *
 * @author powered by FluentMybatis
 */
public class StudentSqlProvider extends BaseSqlProvider<StudentEntity> {
  /**
   * 默认设置器
   */
  private static final StudentDefaults defaults = StudentDefaults.INSTANCE;

  @Override
  public boolean primaryIsNull(StudentEntity entity) {
    return entity.getId() == null;
  }

  @Override
  public boolean primaryNotNull(StudentEntity entity) {
    return entity.getId() != null;
  }

  @Override
  protected void insertEntity(InsertList inserts, String prefix, StudentEntity entity,
      boolean withPk) {
    if (withPk) {
    	inserts.add(prefix, id, entity.getId(), null);
    }
    inserts.add(prefix, gmtCreated, entity.getGmtCreated(), "now()");
    inserts.add(prefix, gmtModified, entity.getGmtModified(), "now()");
    inserts.add(prefix, isDeleted, entity.getIsDeleted(), "0");
    inserts.add(prefix, address, entity.getAddress(), "");
    inserts.add(prefix, addressId, entity.getAddressId(), "");
    inserts.add(prefix, age, entity.getAge(), "");
    inserts.add(prefix, birthday, entity.getBirthday(), "");
    inserts.add(prefix, bonusPoints, entity.getBonusPoints(), "");
    inserts.add(prefix, genderMan, entity.getGenderMan(), "");
    inserts.add(prefix, grade, entity.getGrade(), "");
    inserts.add(prefix, homeCountyId, entity.getHomeCountyId(), "");
    inserts.add(prefix, phone, entity.getPhone(), "");
    inserts.add(prefix, status, entity.getStatus(), "");
    inserts.add(prefix, userName, entity.getUserName(), "");
  }

  @Override
  protected List<String> insertBatchEntity(int index, StudentEntity entity, boolean withPk) {
    List<String> values = new ArrayList<>();
    if (withPk) {
    	values.add(el("list[" + index + "].", id, entity.getId(), null));
    }
    values.add(el("list[" + index + "].", gmtCreated, entity.getGmtCreated(), "now()"));
    values.add(el("list[" + index + "].", gmtModified, entity.getGmtModified(), "now()"));
    values.add(el("list[" + index + "].", isDeleted, entity.getIsDeleted(), "0"));
    values.add(el("list[" + index + "].", address, entity.getAddress(), ""));
    values.add(el("list[" + index + "].", addressId, entity.getAddressId(), ""));
    values.add(el("list[" + index + "].", age, entity.getAge(), ""));
    values.add(el("list[" + index + "].", birthday, entity.getBirthday(), ""));
    values.add(el("list[" + index + "].", bonusPoints, entity.getBonusPoints(), ""));
    values.add(el("list[" + index + "].", genderMan, entity.getGenderMan(), ""));
    values.add(el("list[" + index + "].", grade, entity.getGrade(), ""));
    values.add(el("list[" + index + "].", homeCountyId, entity.getHomeCountyId(), ""));
    values.add(el("list[" + index + "].", phone, entity.getPhone(), ""));
    values.add(el("list[" + index + "].", status, entity.getStatus(), ""));
    values.add(el("list[" + index + "].", userName, entity.getUserName(), ""));
    return values;
  }

  public String updateById(Map<String, Object> map) {
    StudentEntity entity = getParas(map, Param_ET);
    assertNotNull(Param_Entity, entity);
    MapperSql sql = new MapperSql();
    sql.UPDATE(this.tableName());
    UpdateSet updates = new UpdateSet()
    	.add(this.dbType(), gmtCreated, entity.getGmtCreated(), "")
    	.add(this.dbType(), gmtModified, entity.getGmtModified(), "now()")
    	.add(this.dbType(), isDeleted, entity.getIsDeleted(), "")
    	.add(this.dbType(), address, entity.getAddress(), "")
    	.add(this.dbType(), addressId, entity.getAddressId(), "")
    	.add(this.dbType(), age, entity.getAge(), "")
    	.add(this.dbType(), birthday, entity.getBirthday(), "")
    	.add(this.dbType(), bonusPoints, entity.getBonusPoints(), "")
    	.add(this.dbType(), genderMan, entity.getGenderMan(), "")
    	.add(this.dbType(), grade, entity.getGrade(), "")
    	.add(this.dbType(), homeCountyId, entity.getHomeCountyId(), "")
    	.add(this.dbType(), phone, entity.getPhone(), "")
    	.add(this.dbType(), status, entity.getStatus(), "")
    	.add(this.dbType(), userName, entity.getUserName(), "");
    sql.SET(updates.getUpdates());
    sql.WHERE(id.el(this.dbType(), Param_ET));
    return sql.toString();
  }

  @Override
  public List<String> updateDefaults(Map<String, String> updates, boolean ignoreLockVersion) {
    UpdateDefault defaults = new UpdateDefault(updates);
    defaults.add(dbType(), gmtModified, "now()");
    return defaults.getUpdateDefaults();
  }

  @Override
  public String tableName() {
    return defaults.table().get();
  }

  @Override
  protected IMapping mapping() {
    return MAPPING;
  }

  @Override
  public List<String> allFields(boolean withPk) {
    if (withPk) {
    	return Arrays.asList("id", "gmt_created", "gmt_modified", "is_deleted", "address", "address_id", "age", "birthday", "bonus_points", "gender_man", "grade", "home_county_id", "phone", "status", "user_name");
    } else {
    	return Arrays.asList("gmt_created", "gmt_modified", "is_deleted", "address", "address_id", "age", "birthday", "bonus_points", "gender_man", "grade", "home_county_id", "phone", "status", "user_name");
    }
  }

  @Override
  protected void setEntityByDefault(IEntity entity) {
    defaults.setInsertDefault(entity);
  }

  @Override
  public DbType dbType() {
    return DbType.MYSQL;
  }

  @Override
  protected boolean longTypeOfLogicDelete() {
    return false;
  }
}
