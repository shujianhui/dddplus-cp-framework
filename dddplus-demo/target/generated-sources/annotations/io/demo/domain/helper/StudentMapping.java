package io.demo.domain.helper;

import static java.util.Optional.ofNullable;

import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldType;
import java.lang.Override;
import java.lang.String;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 * StudentMapping: Entity类字段和表结构映射
 *
 * @author powered by FluentMybatis
 */
public interface StudentMapping extends IMapping {
  StudentMapping MAPPING = new StudentMapping(){};

  /**
   * 表名称
   */
  String Table_Name = "t_student";

  /**
   * Entity名称
   */
  String Entity_Name = "StudentEntity";

  /**
   * 实体属性 : 数据库字段 映射
   *  id : id
   */
  FieldMapping id = new FieldMapping("id", "id");

  /**
   * 实体属性 : 数据库字段 映射
   *  gmtCreated : gmt_created
   */
  FieldMapping gmtCreated = new FieldMapping("gmtCreated", "gmt_created");

  /**
   * 实体属性 : 数据库字段 映射
   *  gmtModified : gmt_modified
   */
  FieldMapping gmtModified = new FieldMapping("gmtModified", "gmt_modified");

  /**
   * 实体属性 : 数据库字段 映射
   *  isDeleted : is_deleted
   */
  FieldMapping isDeleted = new FieldMapping("isDeleted", "is_deleted");

  /**
   * 实体属性 : 数据库字段 映射
   *  address : address
   */
  FieldMapping address = new FieldMapping("address", "address");

  /**
   * 实体属性 : 数据库字段 映射
   *  addressId : address_id
   */
  FieldMapping addressId = new FieldMapping("addressId", "address_id");

  /**
   * 实体属性 : 数据库字段 映射
   *  age : age
   */
  FieldMapping age = new FieldMapping("age", "age");

  /**
   * 实体属性 : 数据库字段 映射
   *  birthday : birthday
   */
  FieldMapping birthday = new FieldMapping("birthday", "birthday");

  /**
   * 实体属性 : 数据库字段 映射
   *  bonusPoints : bonus_points
   */
  FieldMapping bonusPoints = new FieldMapping("bonusPoints", "bonus_points");

  /**
   * 实体属性 : 数据库字段 映射
   *  genderMan : gender_man
   */
  FieldMapping genderMan = new FieldMapping("genderMan", "gender_man");

  /**
   * 实体属性 : 数据库字段 映射
   *  grade : grade
   */
  FieldMapping grade = new FieldMapping("grade", "grade");

  /**
   * 实体属性 : 数据库字段 映射
   *  homeCountyId : home_county_id
   */
  FieldMapping homeCountyId = new FieldMapping("homeCountyId", "home_county_id");

  /**
   * 实体属性 : 数据库字段 映射
   *  phone : phone
   */
  FieldMapping phone = new FieldMapping("phone", "phone");

  /**
   * 实体属性 : 数据库字段 映射
   *  status : status
   */
  FieldMapping status = new FieldMapping("status", "status");

  /**
   * 实体属性 : 数据库字段 映射
   *  userName : user_name
   */
  FieldMapping userName = new FieldMapping("userName", "user_name");

  /**
   * 实例属性和数据库字段对应表
   */
  Map<String, String> Property2Column = new HashMap<String, String>() {
    {
  		this.put(id.name, id.column);
  		this.put(gmtCreated.name, gmtCreated.column);
  		this.put(gmtModified.name, gmtModified.column);
  		this.put(isDeleted.name, isDeleted.column);
  		this.put(address.name, address.column);
  		this.put(addressId.name, addressId.column);
  		this.put(age.name, age.column);
  		this.put(birthday.name, birthday.column);
  		this.put(bonusPoints.name, bonusPoints.column);
  		this.put(genderMan.name, genderMan.column);
  		this.put(grade.name, grade.column);
  		this.put(homeCountyId.name, homeCountyId.column);
  		this.put(phone.name, phone.column);
  		this.put(status.name, status.column);
  		this.put(userName.name, userName.column);
    }
  };

  /**
   * 数据库字段对应的FieldMapping
   */
  Map<String, FieldMapping> Column2Mapping = new HashMap<String, FieldMapping>() {
    {
  		this.put(id.column, id);
  		this.put(gmtCreated.column, gmtCreated);
  		this.put(gmtModified.column, gmtModified);
  		this.put(isDeleted.column, isDeleted);
  		this.put(address.column, address);
  		this.put(addressId.column, addressId);
  		this.put(age.column, age);
  		this.put(birthday.column, birthday);
  		this.put(bonusPoints.column, bonusPoints);
  		this.put(genderMan.column, genderMan);
  		this.put(grade.column, grade);
  		this.put(homeCountyId.column, homeCountyId);
  		this.put(phone.column, phone);
  		this.put(status.column, status);
  		this.put(userName.column, userName);
    }
  };

  /**
   * 数据库所有字段列表
   */
  List<String> ALL_COLUMNS = Arrays.asList(
  		id.column,
  		gmtCreated.column,
  		gmtModified.column,
  		isDeleted.column,
  		address.column,
  		addressId.column,
  		age.column,
  		birthday.column,
  		bonusPoints.column,
  		genderMan.column,
  		grade.column,
  		homeCountyId.column,
  		phone.column,
  		status.column,
  		userName.column
  );

  /**
   * 数据库所有字段列表用逗号分隔
   */
  String ALL_JOIN_COLUMNS = String.join(", ", ALL_COLUMNS);

  @Override
  default String findColumnByField(String field) {
    return Property2Column.get(field);
  }

  @Override
  default Optional<FieldMapping> findField(FieldType type) {
    switch (type) {
    	case PRIMARY_ID:
    		return ofNullable(id);
    	case LOGIC_DELETED:
    		return ofNullable(isDeleted);
    	default:
    		return ofNullable(null);
    }
  }
}
