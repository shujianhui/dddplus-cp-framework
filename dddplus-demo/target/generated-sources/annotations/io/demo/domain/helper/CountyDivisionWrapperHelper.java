package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.assertNotNull;

import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.functions.IAggregate;
import cn.org.atool.fluent.mybatis.segment.GroupByBase;
import cn.org.atool.fluent.mybatis.segment.HavingBase;
import cn.org.atool.fluent.mybatis.segment.HavingOperator;
import cn.org.atool.fluent.mybatis.segment.OrderByApply;
import cn.org.atool.fluent.mybatis.segment.OrderByBase;
import cn.org.atool.fluent.mybatis.segment.SelectorBase;
import cn.org.atool.fluent.mybatis.segment.UpdateApply;
import cn.org.atool.fluent.mybatis.segment.UpdateBase;
import cn.org.atool.fluent.mybatis.segment.WhereBase;
import cn.org.atool.fluent.mybatis.segment.where.BooleanWhere;
import cn.org.atool.fluent.mybatis.segment.where.NumericWhere;
import cn.org.atool.fluent.mybatis.segment.where.ObjectWhere;
import cn.org.atool.fluent.mybatis.segment.where.StringWhere;
import io.demo.domain.wrapper.CountyDivisionQuery;
import io.demo.domain.wrapper.CountyDivisionUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * CountyDivisionWrapperHelper
 *
 * @author powered by FluentMybatis
 */
public class CountyDivisionWrapperHelper {
  /**
   * 默认设置器
   */
  private static final CountyDivisionDefaults defaults = CountyDivisionDefaults.INSTANCE;

  public interface ISegment<R> {
    R set(FieldMapping fieldMapping);

    default R id() {
      return this.set(CountyDivisionMapping.id);
    }

    default R gmtCreated() {
      return this.set(CountyDivisionMapping.gmtCreated);
    }

    default R gmtModified() {
      return this.set(CountyDivisionMapping.gmtModified);
    }

    default R isDeleted() {
      return this.set(CountyDivisionMapping.isDeleted);
    }

    default R city() {
      return this.set(CountyDivisionMapping.city);
    }

    default R county() {
      return this.set(CountyDivisionMapping.county);
    }

    default R province() {
      return this.set(CountyDivisionMapping.province);
    }
  }

  /**
   * select字段设置
   */
  public static final class Selector extends SelectorBase<Selector, CountyDivisionQuery> implements ISegment<Selector> {
    public Selector(CountyDivisionQuery query) {
      super(query);
    }

    protected Selector(Selector selector, IAggregate aggregate) {
      super(selector, aggregate);
    }

    @Override
    protected Selector aggregateSegment(IAggregate aggregate) {
      return new Selector(this, aggregate);
    }

    public Selector id(String _alias_) {
      return this.process(CountyDivisionMapping.id, _alias_);
    }

    public Selector gmtCreated(String _alias_) {
      return this.process(CountyDivisionMapping.gmtCreated, _alias_);
    }

    public Selector gmtModified(String _alias_) {
      return this.process(CountyDivisionMapping.gmtModified, _alias_);
    }

    public Selector isDeleted(String _alias_) {
      return this.process(CountyDivisionMapping.isDeleted, _alias_);
    }

    public Selector city(String _alias_) {
      return this.process(CountyDivisionMapping.city, _alias_);
    }

    public Selector county(String _alias_) {
      return this.process(CountyDivisionMapping.county, _alias_);
    }

    public Selector province(String _alias_) {
      return this.process(CountyDivisionMapping.province, _alias_);
    }
  }

  /**
   * query where条件设置
   */
  public static class QueryWhere extends WhereBase<QueryWhere, CountyDivisionQuery, CountyDivisionQuery> {
    public QueryWhere(CountyDivisionQuery query) {
      super(query);
    }

    private QueryWhere(CountyDivisionQuery query, QueryWhere where) {
      super(query, where);
    }

    @Override
    protected QueryWhere buildOr(QueryWhere and) {
      return new QueryWhere((CountyDivisionQuery) this.wrapper, and);
    }

    @Override
    public QueryWhere defaults() {
      defaults.setQueryDefault((CountyDivisionQuery) super.wrapper);
      return super.and;
    }

    public NumericWhere<QueryWhere, CountyDivisionQuery> id() {
      return this.set(CountyDivisionMapping.id);
    }

    public ObjectWhere<QueryWhere, CountyDivisionQuery> gmtCreated() {
      return this.set(CountyDivisionMapping.gmtCreated);
    }

    public ObjectWhere<QueryWhere, CountyDivisionQuery> gmtModified() {
      return this.set(CountyDivisionMapping.gmtModified);
    }

    public BooleanWhere<QueryWhere, CountyDivisionQuery> isDeleted() {
      return this.set(CountyDivisionMapping.isDeleted);
    }

    public StringWhere<QueryWhere, CountyDivisionQuery> city() {
      return this.set(CountyDivisionMapping.city);
    }

    public StringWhere<QueryWhere, CountyDivisionQuery> county() {
      return this.set(CountyDivisionMapping.county);
    }

    public StringWhere<QueryWhere, CountyDivisionQuery> province() {
      return this.set(CountyDivisionMapping.province);
    }
  }

  /**
   * update where条件设置
   */
  public static class UpdateWhere extends WhereBase<UpdateWhere, CountyDivisionUpdate, CountyDivisionQuery> {
    public UpdateWhere(CountyDivisionUpdate updater) {
      super(updater);
    }

    private UpdateWhere(CountyDivisionUpdate updater, UpdateWhere where) {
      super(updater, where);
    }

    @Override
    protected UpdateWhere buildOr(UpdateWhere and) {
      return new UpdateWhere((CountyDivisionUpdate) this.wrapper, and);
    }

    @Override
    public UpdateWhere defaults() {
      defaults.setUpdateDefault((CountyDivisionUpdate) super.wrapper);
      return super.and;
    }

    public NumericWhere<UpdateWhere, CountyDivisionQuery> id() {
      return this.set(CountyDivisionMapping.id);
    }

    public ObjectWhere<UpdateWhere, CountyDivisionQuery> gmtCreated() {
      return this.set(CountyDivisionMapping.gmtCreated);
    }

    public ObjectWhere<UpdateWhere, CountyDivisionQuery> gmtModified() {
      return this.set(CountyDivisionMapping.gmtModified);
    }

    public BooleanWhere<UpdateWhere, CountyDivisionQuery> isDeleted() {
      return this.set(CountyDivisionMapping.isDeleted);
    }

    public StringWhere<UpdateWhere, CountyDivisionQuery> city() {
      return this.set(CountyDivisionMapping.city);
    }

    public StringWhere<UpdateWhere, CountyDivisionQuery> county() {
      return this.set(CountyDivisionMapping.county);
    }

    public StringWhere<UpdateWhere, CountyDivisionQuery> province() {
      return this.set(CountyDivisionMapping.province);
    }
  }

  /**
   * 分组设置
   */
  public static final class GroupBy extends GroupByBase<GroupBy, CountyDivisionQuery> implements ISegment<GroupBy> {
    public GroupBy(CountyDivisionQuery query) {
      super(query);
    }
  }

  /**
   * 分组Having条件设置
   */
  public static final class Having extends HavingBase<Having, CountyDivisionQuery> implements ISegment<HavingOperator<Having>> {
    public Having(CountyDivisionQuery query) {
      super(query);
    }

    protected Having(Having having, IAggregate aggregate) {
      super(having, aggregate);
    }

    @Override
    protected Having aggregateSegment(IAggregate aggregate) {
      return new Having(this, aggregate);
    }
  }

  /**
   * Query OrderBy设置
   */
  public static final class QueryOrderBy extends OrderByBase<QueryOrderBy, CountyDivisionQuery> implements ISegment<OrderByApply<QueryOrderBy, CountyDivisionQuery>> {
    public QueryOrderBy(CountyDivisionQuery query) {
      super(query);
    }
  }

  /**
   * Update OrderBy设置
   */
  public static final class UpdateOrderBy extends OrderByBase<UpdateOrderBy, CountyDivisionUpdate> implements ISegment<OrderByApply<UpdateOrderBy, CountyDivisionUpdate>> {
    public UpdateOrderBy(CountyDivisionUpdate updater) {
      super(updater);
    }
  }

  /**
   * Update set 设置
   */
  public static final class UpdateSetter extends UpdateBase<UpdateSetter, CountyDivisionUpdate> implements ISegment<UpdateApply<UpdateSetter, CountyDivisionUpdate>> {
    public UpdateSetter(CountyDivisionUpdate updater) {
      super(updater);
    }
  }
}
