package io.demo.domain.helper;

import static io.demo.domain.helper.MemberFavoriteMapping.Table_Name;

import cn.org.atool.fluent.mybatis.base.crud.BaseDefault;
import cn.org.atool.fluent.mybatis.base.crud.IDefaultSetter;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import cn.org.atool.fluent.mybatis.segment.model.Parameters;
import io.demo.domain.entity.MemberFavoriteEntity;
import io.demo.domain.wrapper.MemberFavoriteQuery;
import io.demo.domain.wrapper.MemberFavoriteUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * MemberFavoriteDefaults
 *
 * @author powered by FluentMybatis
 */
public class MemberFavoriteDefaults extends BaseDefault<MemberFavoriteEntity, MemberFavoriteQuery, MemberFavoriteUpdate, MemberFavoriteDefaults> implements IDefaultSetter {
  public static final MemberFavoriteDefaults INSTANCE = new MemberFavoriteDefaults();

  private MemberFavoriteDefaults() {
    super(Table_Name, "demo", DbType.MYSQL);
  }

  @Override
  public MemberFavoriteQuery query() {
    return new MemberFavoriteQuery();
  }

  @Override
  public MemberFavoriteUpdate updater() {
    return new MemberFavoriteUpdate();
  }

  /**
   * 自动分配表别名查询构造器(join查询的时候需要定义表别名)
   * 如果要自定义别名, 使用方法 {@link #aliasQuery(String)}
   */
  @Override
  public MemberFavoriteQuery aliasQuery(String alias, Parameters parameters) {
    return new MemberFavoriteQuery(alias, parameters);
  }
}
