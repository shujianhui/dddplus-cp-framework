package io.demo.domain.helper;

import static java.util.Optional.ofNullable;

import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldType;
import java.lang.Override;
import java.lang.String;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 * StudentScoreMapping: Entity类字段和表结构映射
 *
 * @author powered by FluentMybatis
 */
public interface StudentScoreMapping extends IMapping {
  StudentScoreMapping MAPPING = new StudentScoreMapping(){};

  /**
   * 表名称
   */
  String Table_Name = "t_student_score";

  /**
   * Entity名称
   */
  String Entity_Name = "StudentScoreEntity";

  /**
   * 实体属性 : 数据库字段 映射
   *  id : id
   */
  FieldMapping id = new FieldMapping("id", "id");

  /**
   * 实体属性 : 数据库字段 映射
   *  gmtCreated : gmt_created
   */
  FieldMapping gmtCreated = new FieldMapping("gmtCreated", "gmt_created");

  /**
   * 实体属性 : 数据库字段 映射
   *  gmtModified : gmt_modified
   */
  FieldMapping gmtModified = new FieldMapping("gmtModified", "gmt_modified");

  /**
   * 实体属性 : 数据库字段 映射
   *  isDeleted : is_deleted
   */
  FieldMapping isDeleted = new FieldMapping("isDeleted", "is_deleted");

  /**
   * 实体属性 : 数据库字段 映射
   *  schoolTerm : school_term
   */
  FieldMapping schoolTerm = new FieldMapping("schoolTerm", "school_term");

  /**
   * 实体属性 : 数据库字段 映射
   *  score : score
   */
  FieldMapping score = new FieldMapping("score", "score");

  /**
   * 实体属性 : 数据库字段 映射
   *  studentId : student_id
   */
  FieldMapping studentId = new FieldMapping("studentId", "student_id");

  /**
   * 实体属性 : 数据库字段 映射
   *  subject : subject
   */
  FieldMapping subject = new FieldMapping("subject", "subject");

  /**
   * 实例属性和数据库字段对应表
   */
  Map<String, String> Property2Column = new HashMap<String, String>() {
    {
  		this.put(id.name, id.column);
  		this.put(gmtCreated.name, gmtCreated.column);
  		this.put(gmtModified.name, gmtModified.column);
  		this.put(isDeleted.name, isDeleted.column);
  		this.put(schoolTerm.name, schoolTerm.column);
  		this.put(score.name, score.column);
  		this.put(studentId.name, studentId.column);
  		this.put(subject.name, subject.column);
    }
  };

  /**
   * 数据库字段对应的FieldMapping
   */
  Map<String, FieldMapping> Column2Mapping = new HashMap<String, FieldMapping>() {
    {
  		this.put(id.column, id);
  		this.put(gmtCreated.column, gmtCreated);
  		this.put(gmtModified.column, gmtModified);
  		this.put(isDeleted.column, isDeleted);
  		this.put(schoolTerm.column, schoolTerm);
  		this.put(score.column, score);
  		this.put(studentId.column, studentId);
  		this.put(subject.column, subject);
    }
  };

  /**
   * 数据库所有字段列表
   */
  List<String> ALL_COLUMNS = Arrays.asList(
  		id.column,
  		gmtCreated.column,
  		gmtModified.column,
  		isDeleted.column,
  		schoolTerm.column,
  		score.column,
  		studentId.column,
  		subject.column
  );

  /**
   * 数据库所有字段列表用逗号分隔
   */
  String ALL_JOIN_COLUMNS = String.join(", ", ALL_COLUMNS);

  @Override
  default String findColumnByField(String field) {
    return Property2Column.get(field);
  }

  @Override
  default Optional<FieldMapping> findField(FieldType type) {
    switch (type) {
    	case PRIMARY_ID:
    		return ofNullable(id);
    	case LOGIC_DELETED:
    		return ofNullable(isDeleted);
    	default:
    		return ofNullable(null);
    }
  }
}
