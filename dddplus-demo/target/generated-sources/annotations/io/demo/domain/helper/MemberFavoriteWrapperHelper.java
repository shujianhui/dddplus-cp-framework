package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.assertNotNull;

import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.functions.IAggregate;
import cn.org.atool.fluent.mybatis.segment.GroupByBase;
import cn.org.atool.fluent.mybatis.segment.HavingBase;
import cn.org.atool.fluent.mybatis.segment.HavingOperator;
import cn.org.atool.fluent.mybatis.segment.OrderByApply;
import cn.org.atool.fluent.mybatis.segment.OrderByBase;
import cn.org.atool.fluent.mybatis.segment.SelectorBase;
import cn.org.atool.fluent.mybatis.segment.UpdateApply;
import cn.org.atool.fluent.mybatis.segment.UpdateBase;
import cn.org.atool.fluent.mybatis.segment.WhereBase;
import cn.org.atool.fluent.mybatis.segment.where.BooleanWhere;
import cn.org.atool.fluent.mybatis.segment.where.NumericWhere;
import cn.org.atool.fluent.mybatis.segment.where.ObjectWhere;
import cn.org.atool.fluent.mybatis.segment.where.StringWhere;
import io.demo.domain.wrapper.MemberFavoriteQuery;
import io.demo.domain.wrapper.MemberFavoriteUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * MemberFavoriteWrapperHelper
 *
 * @author powered by FluentMybatis
 */
public class MemberFavoriteWrapperHelper {
  /**
   * 默认设置器
   */
  private static final MemberFavoriteDefaults defaults = MemberFavoriteDefaults.INSTANCE;

  public interface ISegment<R> {
    R set(FieldMapping fieldMapping);

    default R id() {
      return this.set(MemberFavoriteMapping.id);
    }

    default R gmtCreated() {
      return this.set(MemberFavoriteMapping.gmtCreated);
    }

    default R gmtModified() {
      return this.set(MemberFavoriteMapping.gmtModified);
    }

    default R isDeleted() {
      return this.set(MemberFavoriteMapping.isDeleted);
    }

    default R favorite() {
      return this.set(MemberFavoriteMapping.favorite);
    }

    default R memberId() {
      return this.set(MemberFavoriteMapping.memberId);
    }
  }

  /**
   * select字段设置
   */
  public static final class Selector extends SelectorBase<Selector, MemberFavoriteQuery> implements ISegment<Selector> {
    public Selector(MemberFavoriteQuery query) {
      super(query);
    }

    protected Selector(Selector selector, IAggregate aggregate) {
      super(selector, aggregate);
    }

    @Override
    protected Selector aggregateSegment(IAggregate aggregate) {
      return new Selector(this, aggregate);
    }

    public Selector id(String _alias_) {
      return this.process(MemberFavoriteMapping.id, _alias_);
    }

    public Selector gmtCreated(String _alias_) {
      return this.process(MemberFavoriteMapping.gmtCreated, _alias_);
    }

    public Selector gmtModified(String _alias_) {
      return this.process(MemberFavoriteMapping.gmtModified, _alias_);
    }

    public Selector isDeleted(String _alias_) {
      return this.process(MemberFavoriteMapping.isDeleted, _alias_);
    }

    public Selector favorite(String _alias_) {
      return this.process(MemberFavoriteMapping.favorite, _alias_);
    }

    public Selector memberId(String _alias_) {
      return this.process(MemberFavoriteMapping.memberId, _alias_);
    }
  }

  /**
   * query where条件设置
   */
  public static class QueryWhere extends WhereBase<QueryWhere, MemberFavoriteQuery, MemberFavoriteQuery> {
    public QueryWhere(MemberFavoriteQuery query) {
      super(query);
    }

    private QueryWhere(MemberFavoriteQuery query, QueryWhere where) {
      super(query, where);
    }

    @Override
    protected QueryWhere buildOr(QueryWhere and) {
      return new QueryWhere((MemberFavoriteQuery) this.wrapper, and);
    }

    @Override
    public QueryWhere defaults() {
      defaults.setQueryDefault((MemberFavoriteQuery) super.wrapper);
      return super.and;
    }

    public NumericWhere<QueryWhere, MemberFavoriteQuery> id() {
      return this.set(MemberFavoriteMapping.id);
    }

    public ObjectWhere<QueryWhere, MemberFavoriteQuery> gmtCreated() {
      return this.set(MemberFavoriteMapping.gmtCreated);
    }

    public ObjectWhere<QueryWhere, MemberFavoriteQuery> gmtModified() {
      return this.set(MemberFavoriteMapping.gmtModified);
    }

    public BooleanWhere<QueryWhere, MemberFavoriteQuery> isDeleted() {
      return this.set(MemberFavoriteMapping.isDeleted);
    }

    public StringWhere<QueryWhere, MemberFavoriteQuery> favorite() {
      return this.set(MemberFavoriteMapping.favorite);
    }

    public NumericWhere<QueryWhere, MemberFavoriteQuery> memberId() {
      return this.set(MemberFavoriteMapping.memberId);
    }
  }

  /**
   * update where条件设置
   */
  public static class UpdateWhere extends WhereBase<UpdateWhere, MemberFavoriteUpdate, MemberFavoriteQuery> {
    public UpdateWhere(MemberFavoriteUpdate updater) {
      super(updater);
    }

    private UpdateWhere(MemberFavoriteUpdate updater, UpdateWhere where) {
      super(updater, where);
    }

    @Override
    protected UpdateWhere buildOr(UpdateWhere and) {
      return new UpdateWhere((MemberFavoriteUpdate) this.wrapper, and);
    }

    @Override
    public UpdateWhere defaults() {
      defaults.setUpdateDefault((MemberFavoriteUpdate) super.wrapper);
      return super.and;
    }

    public NumericWhere<UpdateWhere, MemberFavoriteQuery> id() {
      return this.set(MemberFavoriteMapping.id);
    }

    public ObjectWhere<UpdateWhere, MemberFavoriteQuery> gmtCreated() {
      return this.set(MemberFavoriteMapping.gmtCreated);
    }

    public ObjectWhere<UpdateWhere, MemberFavoriteQuery> gmtModified() {
      return this.set(MemberFavoriteMapping.gmtModified);
    }

    public BooleanWhere<UpdateWhere, MemberFavoriteQuery> isDeleted() {
      return this.set(MemberFavoriteMapping.isDeleted);
    }

    public StringWhere<UpdateWhere, MemberFavoriteQuery> favorite() {
      return this.set(MemberFavoriteMapping.favorite);
    }

    public NumericWhere<UpdateWhere, MemberFavoriteQuery> memberId() {
      return this.set(MemberFavoriteMapping.memberId);
    }
  }

  /**
   * 分组设置
   */
  public static final class GroupBy extends GroupByBase<GroupBy, MemberFavoriteQuery> implements ISegment<GroupBy> {
    public GroupBy(MemberFavoriteQuery query) {
      super(query);
    }
  }

  /**
   * 分组Having条件设置
   */
  public static final class Having extends HavingBase<Having, MemberFavoriteQuery> implements ISegment<HavingOperator<Having>> {
    public Having(MemberFavoriteQuery query) {
      super(query);
    }

    protected Having(Having having, IAggregate aggregate) {
      super(having, aggregate);
    }

    @Override
    protected Having aggregateSegment(IAggregate aggregate) {
      return new Having(this, aggregate);
    }
  }

  /**
   * Query OrderBy设置
   */
  public static final class QueryOrderBy extends OrderByBase<QueryOrderBy, MemberFavoriteQuery> implements ISegment<OrderByApply<QueryOrderBy, MemberFavoriteQuery>> {
    public QueryOrderBy(MemberFavoriteQuery query) {
      super(query);
    }
  }

  /**
   * Update OrderBy设置
   */
  public static final class UpdateOrderBy extends OrderByBase<UpdateOrderBy, MemberFavoriteUpdate> implements ISegment<OrderByApply<UpdateOrderBy, MemberFavoriteUpdate>> {
    public UpdateOrderBy(MemberFavoriteUpdate updater) {
      super(updater);
    }
  }

  /**
   * Update set 设置
   */
  public static final class UpdateSetter extends UpdateBase<UpdateSetter, MemberFavoriteUpdate> implements ISegment<UpdateApply<UpdateSetter, MemberFavoriteUpdate>> {
    public UpdateSetter(MemberFavoriteUpdate updater) {
      super(updater);
    }
  }
}
