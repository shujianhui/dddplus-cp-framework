package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.base.model.InsertList.el;
import static cn.org.atool.fluent.mybatis.mapper.FluentConst.*;
import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.*;
import static cn.org.atool.fluent.mybatis.utility.SqlProviderUtils.*;
import static io.demo.domain.helper.ReceivingAddressMapping.*;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.InsertList;
import cn.org.atool.fluent.mybatis.base.model.UpdateDefault;
import cn.org.atool.fluent.mybatis.base.model.UpdateSet;
import cn.org.atool.fluent.mybatis.base.provider.BaseSqlProvider;
import cn.org.atool.fluent.mybatis.mapper.MapperSql;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import io.demo.domain.entity.ReceivingAddressEntity;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * ReceivingAddressSqlProvider: 动态语句封装
 *
 * @author powered by FluentMybatis
 */
public class ReceivingAddressSqlProvider extends BaseSqlProvider<ReceivingAddressEntity> {
  /**
   * 默认设置器
   */
  private static final ReceivingAddressDefaults defaults = ReceivingAddressDefaults.INSTANCE;

  @Override
  public boolean primaryIsNull(ReceivingAddressEntity entity) {
    return entity.getId() == null;
  }

  @Override
  public boolean primaryNotNull(ReceivingAddressEntity entity) {
    return entity.getId() != null;
  }

  @Override
  protected void insertEntity(InsertList inserts, String prefix, ReceivingAddressEntity entity,
      boolean withPk) {
    if (withPk) {
    	inserts.add(prefix, id, entity.getId(), null);
    }
    inserts.add(prefix, gmtModified, entity.getGmtModified(), "now()");
    inserts.add(prefix, isDeleted, entity.getIsDeleted(), "0");
    inserts.add(prefix, city, entity.getCity(), "");
    inserts.add(prefix, detailAddress, entity.getDetailAddress(), "");
    inserts.add(prefix, district, entity.getDistrict(), "");
    inserts.add(prefix, gmtCreate, entity.getGmtCreate(), "");
    inserts.add(prefix, province, entity.getProvince(), "");
    inserts.add(prefix, userId, entity.getUserId(), "");
  }

  @Override
  protected List<String> insertBatchEntity(int index, ReceivingAddressEntity entity,
      boolean withPk) {
    List<String> values = new ArrayList<>();
    if (withPk) {
    	values.add(el("list[" + index + "].", id, entity.getId(), null));
    }
    values.add(el("list[" + index + "].", gmtModified, entity.getGmtModified(), "now()"));
    values.add(el("list[" + index + "].", isDeleted, entity.getIsDeleted(), "0"));
    values.add(el("list[" + index + "].", city, entity.getCity(), ""));
    values.add(el("list[" + index + "].", detailAddress, entity.getDetailAddress(), ""));
    values.add(el("list[" + index + "].", district, entity.getDistrict(), ""));
    values.add(el("list[" + index + "].", gmtCreate, entity.getGmtCreate(), ""));
    values.add(el("list[" + index + "].", province, entity.getProvince(), ""));
    values.add(el("list[" + index + "].", userId, entity.getUserId(), ""));
    return values;
  }

  public String updateById(Map<String, Object> map) {
    ReceivingAddressEntity entity = getParas(map, Param_ET);
    assertNotNull(Param_Entity, entity);
    MapperSql sql = new MapperSql();
    sql.UPDATE(this.tableName());
    UpdateSet updates = new UpdateSet()
    	.add(this.dbType(), gmtModified, entity.getGmtModified(), "now()")
    	.add(this.dbType(), isDeleted, entity.getIsDeleted(), "")
    	.add(this.dbType(), city, entity.getCity(), "")
    	.add(this.dbType(), detailAddress, entity.getDetailAddress(), "")
    	.add(this.dbType(), district, entity.getDistrict(), "")
    	.add(this.dbType(), gmtCreate, entity.getGmtCreate(), "")
    	.add(this.dbType(), province, entity.getProvince(), "")
    	.add(this.dbType(), userId, entity.getUserId(), "");
    sql.SET(updates.getUpdates());
    sql.WHERE(id.el(this.dbType(), Param_ET));
    return sql.toString();
  }

  @Override
  public List<String> updateDefaults(Map<String, String> updates, boolean ignoreLockVersion) {
    UpdateDefault defaults = new UpdateDefault(updates);
    defaults.add(dbType(), gmtModified, "now()");
    return defaults.getUpdateDefaults();
  }

  @Override
  public String tableName() {
    return defaults.table().get();
  }

  @Override
  protected IMapping mapping() {
    return MAPPING;
  }

  @Override
  public List<String> allFields(boolean withPk) {
    if (withPk) {
    	return Arrays.asList("id", "gmt_modified", "is_deleted", "city", "detail_address", "district", "gmt_create", "province", "user_id");
    } else {
    	return Arrays.asList("gmt_modified", "is_deleted", "city", "detail_address", "district", "gmt_create", "province", "user_id");
    }
  }

  @Override
  protected void setEntityByDefault(IEntity entity) {
    defaults.setInsertDefault(entity);
  }

  @Override
  public DbType dbType() {
    return DbType.MYSQL;
  }

  @Override
  protected boolean longTypeOfLogicDelete() {
    return false;
  }
}
