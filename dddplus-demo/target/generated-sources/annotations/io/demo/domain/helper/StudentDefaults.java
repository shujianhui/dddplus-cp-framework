package io.demo.domain.helper;

import static io.demo.domain.helper.StudentMapping.Table_Name;

import cn.org.atool.fluent.mybatis.base.crud.BaseDefault;
import cn.org.atool.fluent.mybatis.base.crud.IDefaultSetter;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import cn.org.atool.fluent.mybatis.segment.model.Parameters;
import io.demo.domain.entity.StudentEntity;
import io.demo.domain.wrapper.StudentQuery;
import io.demo.domain.wrapper.StudentUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * StudentDefaults
 *
 * @author powered by FluentMybatis
 */
public class StudentDefaults extends BaseDefault<StudentEntity, StudentQuery, StudentUpdate, StudentDefaults> implements IDefaultSetter {
  public static final StudentDefaults INSTANCE = new StudentDefaults();

  private StudentDefaults() {
    super(Table_Name, "demo", DbType.MYSQL);
  }

  @Override
  public StudentQuery query() {
    return new StudentQuery();
  }

  @Override
  public StudentUpdate updater() {
    return new StudentUpdate();
  }

  /**
   * 自动分配表别名查询构造器(join查询的时候需要定义表别名)
   * 如果要自定义别名, 使用方法 {@link #aliasQuery(String)}
   */
  @Override
  public StudentQuery aliasQuery(String alias, Parameters parameters) {
    return new StudentQuery(alias, parameters);
  }
}
