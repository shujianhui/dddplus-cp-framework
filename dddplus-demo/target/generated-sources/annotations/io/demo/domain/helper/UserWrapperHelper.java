package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.assertNotNull;

import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.functions.IAggregate;
import cn.org.atool.fluent.mybatis.segment.GroupByBase;
import cn.org.atool.fluent.mybatis.segment.HavingBase;
import cn.org.atool.fluent.mybatis.segment.HavingOperator;
import cn.org.atool.fluent.mybatis.segment.OrderByApply;
import cn.org.atool.fluent.mybatis.segment.OrderByBase;
import cn.org.atool.fluent.mybatis.segment.SelectorBase;
import cn.org.atool.fluent.mybatis.segment.UpdateApply;
import cn.org.atool.fluent.mybatis.segment.UpdateBase;
import cn.org.atool.fluent.mybatis.segment.WhereBase;
import cn.org.atool.fluent.mybatis.segment.where.BooleanWhere;
import cn.org.atool.fluent.mybatis.segment.where.NumericWhere;
import cn.org.atool.fluent.mybatis.segment.where.ObjectWhere;
import cn.org.atool.fluent.mybatis.segment.where.StringWhere;
import io.demo.domain.wrapper.UserQuery;
import io.demo.domain.wrapper.UserUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * UserWrapperHelper
 *
 * @author powered by FluentMybatis
 */
public class UserWrapperHelper {
  /**
   * 默认设置器
   */
  private static final UserDefaults defaults = UserDefaults.INSTANCE;

  public interface ISegment<R> {
    R set(FieldMapping fieldMapping);

    default R id() {
      return this.set(UserMapping.id);
    }

    default R gmtCreated() {
      return this.set(UserMapping.gmtCreated);
    }

    default R gmtModified() {
      return this.set(UserMapping.gmtModified);
    }

    default R isDeleted() {
      return this.set(UserMapping.isDeleted);
    }

    default R age() {
      return this.set(UserMapping.age);
    }

    default R email() {
      return this.set(UserMapping.email);
    }

    default R name() {
      return this.set(UserMapping.name);
    }
  }

  /**
   * select字段设置
   */
  public static final class Selector extends SelectorBase<Selector, UserQuery> implements ISegment<Selector> {
    public Selector(UserQuery query) {
      super(query);
    }

    protected Selector(Selector selector, IAggregate aggregate) {
      super(selector, aggregate);
    }

    @Override
    protected Selector aggregateSegment(IAggregate aggregate) {
      return new Selector(this, aggregate);
    }

    public Selector id(String _alias_) {
      return this.process(UserMapping.id, _alias_);
    }

    public Selector gmtCreated(String _alias_) {
      return this.process(UserMapping.gmtCreated, _alias_);
    }

    public Selector gmtModified(String _alias_) {
      return this.process(UserMapping.gmtModified, _alias_);
    }

    public Selector isDeleted(String _alias_) {
      return this.process(UserMapping.isDeleted, _alias_);
    }

    public Selector age(String _alias_) {
      return this.process(UserMapping.age, _alias_);
    }

    public Selector email(String _alias_) {
      return this.process(UserMapping.email, _alias_);
    }

    public Selector name(String _alias_) {
      return this.process(UserMapping.name, _alias_);
    }
  }

  /**
   * query where条件设置
   */
  public static class QueryWhere extends WhereBase<QueryWhere, UserQuery, UserQuery> {
    public QueryWhere(UserQuery query) {
      super(query);
    }

    private QueryWhere(UserQuery query, QueryWhere where) {
      super(query, where);
    }

    @Override
    protected QueryWhere buildOr(QueryWhere and) {
      return new QueryWhere((UserQuery) this.wrapper, and);
    }

    @Override
    public QueryWhere defaults() {
      defaults.setQueryDefault((UserQuery) super.wrapper);
      return super.and;
    }

    public NumericWhere<QueryWhere, UserQuery> id() {
      return this.set(UserMapping.id);
    }

    public ObjectWhere<QueryWhere, UserQuery> gmtCreated() {
      return this.set(UserMapping.gmtCreated);
    }

    public ObjectWhere<QueryWhere, UserQuery> gmtModified() {
      return this.set(UserMapping.gmtModified);
    }

    public BooleanWhere<QueryWhere, UserQuery> isDeleted() {
      return this.set(UserMapping.isDeleted);
    }

    public NumericWhere<QueryWhere, UserQuery> age() {
      return this.set(UserMapping.age);
    }

    public StringWhere<QueryWhere, UserQuery> email() {
      return this.set(UserMapping.email);
    }

    public StringWhere<QueryWhere, UserQuery> name() {
      return this.set(UserMapping.name);
    }
  }

  /**
   * update where条件设置
   */
  public static class UpdateWhere extends WhereBase<UpdateWhere, UserUpdate, UserQuery> {
    public UpdateWhere(UserUpdate updater) {
      super(updater);
    }

    private UpdateWhere(UserUpdate updater, UpdateWhere where) {
      super(updater, where);
    }

    @Override
    protected UpdateWhere buildOr(UpdateWhere and) {
      return new UpdateWhere((UserUpdate) this.wrapper, and);
    }

    @Override
    public UpdateWhere defaults() {
      defaults.setUpdateDefault((UserUpdate) super.wrapper);
      return super.and;
    }

    public NumericWhere<UpdateWhere, UserQuery> id() {
      return this.set(UserMapping.id);
    }

    public ObjectWhere<UpdateWhere, UserQuery> gmtCreated() {
      return this.set(UserMapping.gmtCreated);
    }

    public ObjectWhere<UpdateWhere, UserQuery> gmtModified() {
      return this.set(UserMapping.gmtModified);
    }

    public BooleanWhere<UpdateWhere, UserQuery> isDeleted() {
      return this.set(UserMapping.isDeleted);
    }

    public NumericWhere<UpdateWhere, UserQuery> age() {
      return this.set(UserMapping.age);
    }

    public StringWhere<UpdateWhere, UserQuery> email() {
      return this.set(UserMapping.email);
    }

    public StringWhere<UpdateWhere, UserQuery> name() {
      return this.set(UserMapping.name);
    }
  }

  /**
   * 分组设置
   */
  public static final class GroupBy extends GroupByBase<GroupBy, UserQuery> implements ISegment<GroupBy> {
    public GroupBy(UserQuery query) {
      super(query);
    }
  }

  /**
   * 分组Having条件设置
   */
  public static final class Having extends HavingBase<Having, UserQuery> implements ISegment<HavingOperator<Having>> {
    public Having(UserQuery query) {
      super(query);
    }

    protected Having(Having having, IAggregate aggregate) {
      super(having, aggregate);
    }

    @Override
    protected Having aggregateSegment(IAggregate aggregate) {
      return new Having(this, aggregate);
    }
  }

  /**
   * Query OrderBy设置
   */
  public static final class QueryOrderBy extends OrderByBase<QueryOrderBy, UserQuery> implements ISegment<OrderByApply<QueryOrderBy, UserQuery>> {
    public QueryOrderBy(UserQuery query) {
      super(query);
    }
  }

  /**
   * Update OrderBy设置
   */
  public static final class UpdateOrderBy extends OrderByBase<UpdateOrderBy, UserUpdate> implements ISegment<OrderByApply<UpdateOrderBy, UserUpdate>> {
    public UpdateOrderBy(UserUpdate updater) {
      super(updater);
    }
  }

  /**
   * Update set 设置
   */
  public static final class UpdateSetter extends UpdateBase<UpdateSetter, UserUpdate> implements ISegment<UpdateApply<UpdateSetter, UserUpdate>> {
    public UpdateSetter(UserUpdate updater) {
      super(updater);
    }
  }
}
