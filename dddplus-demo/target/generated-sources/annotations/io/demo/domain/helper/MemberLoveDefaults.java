package io.demo.domain.helper;

import static io.demo.domain.helper.MemberLoveMapping.Table_Name;

import cn.org.atool.fluent.mybatis.base.crud.BaseDefault;
import cn.org.atool.fluent.mybatis.base.crud.IDefaultSetter;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import cn.org.atool.fluent.mybatis.segment.model.Parameters;
import io.demo.domain.entity.MemberLoveEntity;
import io.demo.domain.wrapper.MemberLoveQuery;
import io.demo.domain.wrapper.MemberLoveUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * MemberLoveDefaults
 *
 * @author powered by FluentMybatis
 */
public class MemberLoveDefaults extends BaseDefault<MemberLoveEntity, MemberLoveQuery, MemberLoveUpdate, MemberLoveDefaults> implements IDefaultSetter {
  public static final MemberLoveDefaults INSTANCE = new MemberLoveDefaults();

  private MemberLoveDefaults() {
    super(Table_Name, "demo", DbType.MYSQL);
  }

  @Override
  public MemberLoveQuery query() {
    return new MemberLoveQuery();
  }

  @Override
  public MemberLoveUpdate updater() {
    return new MemberLoveUpdate();
  }

  /**
   * 自动分配表别名查询构造器(join查询的时候需要定义表别名)
   * 如果要自定义别名, 使用方法 {@link #aliasQuery(String)}
   */
  @Override
  public MemberLoveQuery aliasQuery(String alias, Parameters parameters) {
    return new MemberLoveQuery(alias, parameters);
  }
}
