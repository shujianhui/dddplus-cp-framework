package io.demo.domain.helper;

import static io.demo.domain.helper.CountyDivisionMapping.Table_Name;

import cn.org.atool.fluent.mybatis.base.crud.BaseDefault;
import cn.org.atool.fluent.mybatis.base.crud.IDefaultSetter;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import cn.org.atool.fluent.mybatis.segment.model.Parameters;
import io.demo.domain.entity.CountyDivisionEntity;
import io.demo.domain.wrapper.CountyDivisionQuery;
import io.demo.domain.wrapper.CountyDivisionUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * CountyDivisionDefaults
 *
 * @author powered by FluentMybatis
 */
public class CountyDivisionDefaults extends BaseDefault<CountyDivisionEntity, CountyDivisionQuery, CountyDivisionUpdate, CountyDivisionDefaults> implements IDefaultSetter {
  public static final CountyDivisionDefaults INSTANCE = new CountyDivisionDefaults();

  private CountyDivisionDefaults() {
    super(Table_Name, "demo", DbType.MYSQL);
  }

  @Override
  public CountyDivisionQuery query() {
    return new CountyDivisionQuery();
  }

  @Override
  public CountyDivisionUpdate updater() {
    return new CountyDivisionUpdate();
  }

  /**
   * 自动分配表别名查询构造器(join查询的时候需要定义表别名)
   * 如果要自定义别名, 使用方法 {@link #aliasQuery(String)}
   */
  @Override
  public CountyDivisionQuery aliasQuery(String alias, Parameters parameters) {
    return new CountyDivisionQuery(alias, parameters);
  }
}
