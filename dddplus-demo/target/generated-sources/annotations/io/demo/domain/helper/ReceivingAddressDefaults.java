package io.demo.domain.helper;

import static io.demo.domain.helper.ReceivingAddressMapping.Table_Name;

import cn.org.atool.fluent.mybatis.base.crud.BaseDefault;
import cn.org.atool.fluent.mybatis.base.crud.IDefaultSetter;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import cn.org.atool.fluent.mybatis.segment.model.Parameters;
import io.demo.domain.entity.ReceivingAddressEntity;
import io.demo.domain.wrapper.ReceivingAddressQuery;
import io.demo.domain.wrapper.ReceivingAddressUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * ReceivingAddressDefaults
 *
 * @author powered by FluentMybatis
 */
public class ReceivingAddressDefaults extends BaseDefault<ReceivingAddressEntity, ReceivingAddressQuery, ReceivingAddressUpdate, ReceivingAddressDefaults> implements IDefaultSetter {
  public static final ReceivingAddressDefaults INSTANCE = new ReceivingAddressDefaults();

  private ReceivingAddressDefaults() {
    super(Table_Name, "demo", DbType.MYSQL);
  }

  @Override
  public ReceivingAddressQuery query() {
    return new ReceivingAddressQuery();
  }

  @Override
  public ReceivingAddressUpdate updater() {
    return new ReceivingAddressUpdate();
  }

  /**
   * 自动分配表别名查询构造器(join查询的时候需要定义表别名)
   * 如果要自定义别名, 使用方法 {@link #aliasQuery(String)}
   */
  @Override
  public ReceivingAddressQuery aliasQuery(String alias, Parameters parameters) {
    return new ReceivingAddressQuery(alias, parameters);
  }
}
