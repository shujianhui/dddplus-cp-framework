package io.demo.domain.helper;

import static io.demo.domain.helper.UserMapping.Table_Name;

import cn.org.atool.fluent.mybatis.base.crud.BaseDefault;
import cn.org.atool.fluent.mybatis.base.crud.IDefaultSetter;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import cn.org.atool.fluent.mybatis.segment.model.Parameters;
import io.demo.domain.entity.UserEntity;
import io.demo.domain.wrapper.UserQuery;
import io.demo.domain.wrapper.UserUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * UserDefaults
 *
 * @author powered by FluentMybatis
 */
public class UserDefaults extends BaseDefault<UserEntity, UserQuery, UserUpdate, UserDefaults> implements IDefaultSetter {
  public static final UserDefaults INSTANCE = new UserDefaults();

  private UserDefaults() {
    super(Table_Name, "demo", DbType.MYSQL);
  }

  @Override
  public UserQuery query() {
    return new UserQuery();
  }

  @Override
  public UserUpdate updater() {
    return new UserUpdate();
  }

  /**
   * 自动分配表别名查询构造器(join查询的时候需要定义表别名)
   * 如果要自定义别名, 使用方法 {@link #aliasQuery(String)}
   */
  @Override
  public UserQuery aliasQuery(String alias, Parameters parameters) {
    return new UserQuery(alias, parameters);
  }
}
