package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.assertNotNull;

import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.functions.IAggregate;
import cn.org.atool.fluent.mybatis.segment.GroupByBase;
import cn.org.atool.fluent.mybatis.segment.HavingBase;
import cn.org.atool.fluent.mybatis.segment.HavingOperator;
import cn.org.atool.fluent.mybatis.segment.OrderByApply;
import cn.org.atool.fluent.mybatis.segment.OrderByBase;
import cn.org.atool.fluent.mybatis.segment.SelectorBase;
import cn.org.atool.fluent.mybatis.segment.UpdateApply;
import cn.org.atool.fluent.mybatis.segment.UpdateBase;
import cn.org.atool.fluent.mybatis.segment.WhereBase;
import cn.org.atool.fluent.mybatis.segment.where.BooleanWhere;
import cn.org.atool.fluent.mybatis.segment.where.NumericWhere;
import cn.org.atool.fluent.mybatis.segment.where.ObjectWhere;
import cn.org.atool.fluent.mybatis.segment.where.StringWhere;
import io.demo.domain.wrapper.MemberQuery;
import io.demo.domain.wrapper.MemberUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * MemberWrapperHelper
 *
 * @author powered by FluentMybatis
 */
public class MemberWrapperHelper {
  /**
   * 默认设置器
   */
  private static final MemberDefaults defaults = MemberDefaults.INSTANCE;

  public interface ISegment<R> {
    R set(FieldMapping fieldMapping);

    default R id() {
      return this.set(MemberMapping.id);
    }

    default R gmtCreated() {
      return this.set(MemberMapping.gmtCreated);
    }

    default R gmtModified() {
      return this.set(MemberMapping.gmtModified);
    }

    default R isDeleted() {
      return this.set(MemberMapping.isDeleted);
    }

    default R age() {
      return this.set(MemberMapping.age);
    }

    default R isGirl() {
      return this.set(MemberMapping.isGirl);
    }

    default R school() {
      return this.set(MemberMapping.school);
    }

    default R userName() {
      return this.set(MemberMapping.userName);
    }
  }

  /**
   * select字段设置
   */
  public static final class Selector extends SelectorBase<Selector, MemberQuery> implements ISegment<Selector> {
    public Selector(MemberQuery query) {
      super(query);
    }

    protected Selector(Selector selector, IAggregate aggregate) {
      super(selector, aggregate);
    }

    @Override
    protected Selector aggregateSegment(IAggregate aggregate) {
      return new Selector(this, aggregate);
    }

    public Selector id(String _alias_) {
      return this.process(MemberMapping.id, _alias_);
    }

    public Selector gmtCreated(String _alias_) {
      return this.process(MemberMapping.gmtCreated, _alias_);
    }

    public Selector gmtModified(String _alias_) {
      return this.process(MemberMapping.gmtModified, _alias_);
    }

    public Selector isDeleted(String _alias_) {
      return this.process(MemberMapping.isDeleted, _alias_);
    }

    public Selector age(String _alias_) {
      return this.process(MemberMapping.age, _alias_);
    }

    public Selector isGirl(String _alias_) {
      return this.process(MemberMapping.isGirl, _alias_);
    }

    public Selector school(String _alias_) {
      return this.process(MemberMapping.school, _alias_);
    }

    public Selector userName(String _alias_) {
      return this.process(MemberMapping.userName, _alias_);
    }
  }

  /**
   * query where条件设置
   */
  public static class QueryWhere extends WhereBase<QueryWhere, MemberQuery, MemberQuery> {
    public QueryWhere(MemberQuery query) {
      super(query);
    }

    private QueryWhere(MemberQuery query, QueryWhere where) {
      super(query, where);
    }

    @Override
    protected QueryWhere buildOr(QueryWhere and) {
      return new QueryWhere((MemberQuery) this.wrapper, and);
    }

    @Override
    public QueryWhere defaults() {
      defaults.setQueryDefault((MemberQuery) super.wrapper);
      return super.and;
    }

    public NumericWhere<QueryWhere, MemberQuery> id() {
      return this.set(MemberMapping.id);
    }

    public ObjectWhere<QueryWhere, MemberQuery> gmtCreated() {
      return this.set(MemberMapping.gmtCreated);
    }

    public ObjectWhere<QueryWhere, MemberQuery> gmtModified() {
      return this.set(MemberMapping.gmtModified);
    }

    public BooleanWhere<QueryWhere, MemberQuery> isDeleted() {
      return this.set(MemberMapping.isDeleted);
    }

    public NumericWhere<QueryWhere, MemberQuery> age() {
      return this.set(MemberMapping.age);
    }

    public BooleanWhere<QueryWhere, MemberQuery> isGirl() {
      return this.set(MemberMapping.isGirl);
    }

    public StringWhere<QueryWhere, MemberQuery> school() {
      return this.set(MemberMapping.school);
    }

    public StringWhere<QueryWhere, MemberQuery> userName() {
      return this.set(MemberMapping.userName);
    }
  }

  /**
   * update where条件设置
   */
  public static class UpdateWhere extends WhereBase<UpdateWhere, MemberUpdate, MemberQuery> {
    public UpdateWhere(MemberUpdate updater) {
      super(updater);
    }

    private UpdateWhere(MemberUpdate updater, UpdateWhere where) {
      super(updater, where);
    }

    @Override
    protected UpdateWhere buildOr(UpdateWhere and) {
      return new UpdateWhere((MemberUpdate) this.wrapper, and);
    }

    @Override
    public UpdateWhere defaults() {
      defaults.setUpdateDefault((MemberUpdate) super.wrapper);
      return super.and;
    }

    public NumericWhere<UpdateWhere, MemberQuery> id() {
      return this.set(MemberMapping.id);
    }

    public ObjectWhere<UpdateWhere, MemberQuery> gmtCreated() {
      return this.set(MemberMapping.gmtCreated);
    }

    public ObjectWhere<UpdateWhere, MemberQuery> gmtModified() {
      return this.set(MemberMapping.gmtModified);
    }

    public BooleanWhere<UpdateWhere, MemberQuery> isDeleted() {
      return this.set(MemberMapping.isDeleted);
    }

    public NumericWhere<UpdateWhere, MemberQuery> age() {
      return this.set(MemberMapping.age);
    }

    public BooleanWhere<UpdateWhere, MemberQuery> isGirl() {
      return this.set(MemberMapping.isGirl);
    }

    public StringWhere<UpdateWhere, MemberQuery> school() {
      return this.set(MemberMapping.school);
    }

    public StringWhere<UpdateWhere, MemberQuery> userName() {
      return this.set(MemberMapping.userName);
    }
  }

  /**
   * 分组设置
   */
  public static final class GroupBy extends GroupByBase<GroupBy, MemberQuery> implements ISegment<GroupBy> {
    public GroupBy(MemberQuery query) {
      super(query);
    }
  }

  /**
   * 分组Having条件设置
   */
  public static final class Having extends HavingBase<Having, MemberQuery> implements ISegment<HavingOperator<Having>> {
    public Having(MemberQuery query) {
      super(query);
    }

    protected Having(Having having, IAggregate aggregate) {
      super(having, aggregate);
    }

    @Override
    protected Having aggregateSegment(IAggregate aggregate) {
      return new Having(this, aggregate);
    }
  }

  /**
   * Query OrderBy设置
   */
  public static final class QueryOrderBy extends OrderByBase<QueryOrderBy, MemberQuery> implements ISegment<OrderByApply<QueryOrderBy, MemberQuery>> {
    public QueryOrderBy(MemberQuery query) {
      super(query);
    }
  }

  /**
   * Update OrderBy设置
   */
  public static final class UpdateOrderBy extends OrderByBase<UpdateOrderBy, MemberUpdate> implements ISegment<OrderByApply<UpdateOrderBy, MemberUpdate>> {
    public UpdateOrderBy(MemberUpdate updater) {
      super(updater);
    }
  }

  /**
   * Update set 设置
   */
  public static final class UpdateSetter extends UpdateBase<UpdateSetter, MemberUpdate> implements ISegment<UpdateApply<UpdateSetter, MemberUpdate>> {
    public UpdateSetter(MemberUpdate updater) {
      super(updater);
    }
  }
}
