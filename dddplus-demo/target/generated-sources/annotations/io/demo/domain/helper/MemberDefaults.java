package io.demo.domain.helper;

import static io.demo.domain.helper.MemberMapping.Table_Name;

import cn.org.atool.fluent.mybatis.base.crud.BaseDefault;
import cn.org.atool.fluent.mybatis.base.crud.IDefaultSetter;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import cn.org.atool.fluent.mybatis.segment.model.Parameters;
import io.demo.domain.entity.MemberEntity;
import io.demo.domain.wrapper.MemberQuery;
import io.demo.domain.wrapper.MemberUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * MemberDefaults
 *
 * @author powered by FluentMybatis
 */
public class MemberDefaults extends BaseDefault<MemberEntity, MemberQuery, MemberUpdate, MemberDefaults> implements IDefaultSetter {
  public static final MemberDefaults INSTANCE = new MemberDefaults();

  private MemberDefaults() {
    super(Table_Name, "demo", DbType.MYSQL);
  }

  @Override
  public MemberQuery query() {
    return new MemberQuery();
  }

  @Override
  public MemberUpdate updater() {
    return new MemberUpdate();
  }

  /**
   * 自动分配表别名查询构造器(join查询的时候需要定义表别名)
   * 如果要自定义别名, 使用方法 {@link #aliasQuery(String)}
   */
  @Override
  public MemberQuery aliasQuery(String alias, Parameters parameters) {
    return new MemberQuery(alias, parameters);
  }
}
