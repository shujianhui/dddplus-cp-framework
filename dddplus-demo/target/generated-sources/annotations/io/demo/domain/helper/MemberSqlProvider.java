package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.base.model.InsertList.el;
import static cn.org.atool.fluent.mybatis.mapper.FluentConst.*;
import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.*;
import static cn.org.atool.fluent.mybatis.utility.SqlProviderUtils.*;
import static io.demo.domain.helper.MemberMapping.*;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.InsertList;
import cn.org.atool.fluent.mybatis.base.model.UpdateDefault;
import cn.org.atool.fluent.mybatis.base.model.UpdateSet;
import cn.org.atool.fluent.mybatis.base.provider.BaseSqlProvider;
import cn.org.atool.fluent.mybatis.mapper.MapperSql;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import io.demo.domain.entity.MemberEntity;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * MemberSqlProvider: 动态语句封装
 *
 * @author powered by FluentMybatis
 */
public class MemberSqlProvider extends BaseSqlProvider<MemberEntity> {
  /**
   * 默认设置器
   */
  private static final MemberDefaults defaults = MemberDefaults.INSTANCE;

  @Override
  public boolean primaryIsNull(MemberEntity entity) {
    return entity.getId() == null;
  }

  @Override
  public boolean primaryNotNull(MemberEntity entity) {
    return entity.getId() != null;
  }

  @Override
  protected void insertEntity(InsertList inserts, String prefix, MemberEntity entity,
      boolean withPk) {
    if (withPk) {
    	inserts.add(prefix, id, entity.getId(), null);
    }
    inserts.add(prefix, gmtCreated, entity.getGmtCreated(), "now()");
    inserts.add(prefix, gmtModified, entity.getGmtModified(), "now()");
    inserts.add(prefix, isDeleted, entity.getIsDeleted(), "0");
    inserts.add(prefix, age, entity.getAge(), "");
    inserts.add(prefix, isGirl, entity.getIsGirl(), "");
    inserts.add(prefix, school, entity.getSchool(), "");
    inserts.add(prefix, userName, entity.getUserName(), "");
  }

  @Override
  protected List<String> insertBatchEntity(int index, MemberEntity entity, boolean withPk) {
    List<String> values = new ArrayList<>();
    if (withPk) {
    	values.add(el("list[" + index + "].", id, entity.getId(), null));
    }
    values.add(el("list[" + index + "].", gmtCreated, entity.getGmtCreated(), "now()"));
    values.add(el("list[" + index + "].", gmtModified, entity.getGmtModified(), "now()"));
    values.add(el("list[" + index + "].", isDeleted, entity.getIsDeleted(), "0"));
    values.add(el("list[" + index + "].", age, entity.getAge(), ""));
    values.add(el("list[" + index + "].", isGirl, entity.getIsGirl(), ""));
    values.add(el("list[" + index + "].", school, entity.getSchool(), ""));
    values.add(el("list[" + index + "].", userName, entity.getUserName(), ""));
    return values;
  }

  public String updateById(Map<String, Object> map) {
    MemberEntity entity = getParas(map, Param_ET);
    assertNotNull(Param_Entity, entity);
    MapperSql sql = new MapperSql();
    sql.UPDATE(this.tableName());
    UpdateSet updates = new UpdateSet()
    	.add(this.dbType(), gmtCreated, entity.getGmtCreated(), "")
    	.add(this.dbType(), gmtModified, entity.getGmtModified(), "now()")
    	.add(this.dbType(), isDeleted, entity.getIsDeleted(), "")
    	.add(this.dbType(), age, entity.getAge(), "")
    	.add(this.dbType(), isGirl, entity.getIsGirl(), "")
    	.add(this.dbType(), school, entity.getSchool(), "")
    	.add(this.dbType(), userName, entity.getUserName(), "");
    sql.SET(updates.getUpdates());
    sql.WHERE(id.el(this.dbType(), Param_ET));
    return sql.toString();
  }

  @Override
  public List<String> updateDefaults(Map<String, String> updates, boolean ignoreLockVersion) {
    UpdateDefault defaults = new UpdateDefault(updates);
    defaults.add(dbType(), gmtModified, "now()");
    return defaults.getUpdateDefaults();
  }

  @Override
  public String tableName() {
    return defaults.table().get();
  }

  @Override
  protected IMapping mapping() {
    return MAPPING;
  }

  @Override
  public List<String> allFields(boolean withPk) {
    if (withPk) {
    	return Arrays.asList("id", "gmt_created", "gmt_modified", "is_deleted", "age", "is_girl", "school", "user_name");
    } else {
    	return Arrays.asList("gmt_created", "gmt_modified", "is_deleted", "age", "is_girl", "school", "user_name");
    }
  }

  @Override
  protected void setEntityByDefault(IEntity entity) {
    defaults.setInsertDefault(entity);
  }

  @Override
  public DbType dbType() {
    return DbType.MYSQL;
  }

  @Override
  protected boolean longTypeOfLogicDelete() {
    return false;
  }
}
