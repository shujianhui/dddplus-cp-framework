package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.assertNotNull;

import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.functions.IAggregate;
import cn.org.atool.fluent.mybatis.segment.GroupByBase;
import cn.org.atool.fluent.mybatis.segment.HavingBase;
import cn.org.atool.fluent.mybatis.segment.HavingOperator;
import cn.org.atool.fluent.mybatis.segment.OrderByApply;
import cn.org.atool.fluent.mybatis.segment.OrderByBase;
import cn.org.atool.fluent.mybatis.segment.SelectorBase;
import cn.org.atool.fluent.mybatis.segment.UpdateApply;
import cn.org.atool.fluent.mybatis.segment.UpdateBase;
import cn.org.atool.fluent.mybatis.segment.WhereBase;
import cn.org.atool.fluent.mybatis.segment.where.BooleanWhere;
import cn.org.atool.fluent.mybatis.segment.where.NumericWhere;
import cn.org.atool.fluent.mybatis.segment.where.ObjectWhere;
import cn.org.atool.fluent.mybatis.segment.where.StringWhere;
import io.demo.domain.wrapper.StudentScoreQuery;
import io.demo.domain.wrapper.StudentScoreUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * StudentScoreWrapperHelper
 *
 * @author powered by FluentMybatis
 */
public class StudentScoreWrapperHelper {
  /**
   * 默认设置器
   */
  private static final StudentScoreDefaults defaults = StudentScoreDefaults.INSTANCE;

  public interface ISegment<R> {
    R set(FieldMapping fieldMapping);

    default R id() {
      return this.set(StudentScoreMapping.id);
    }

    default R gmtCreated() {
      return this.set(StudentScoreMapping.gmtCreated);
    }

    default R gmtModified() {
      return this.set(StudentScoreMapping.gmtModified);
    }

    default R isDeleted() {
      return this.set(StudentScoreMapping.isDeleted);
    }

    default R schoolTerm() {
      return this.set(StudentScoreMapping.schoolTerm);
    }

    default R score() {
      return this.set(StudentScoreMapping.score);
    }

    default R studentId() {
      return this.set(StudentScoreMapping.studentId);
    }

    default R subject() {
      return this.set(StudentScoreMapping.subject);
    }
  }

  /**
   * select字段设置
   */
  public static final class Selector extends SelectorBase<Selector, StudentScoreQuery> implements ISegment<Selector> {
    public Selector(StudentScoreQuery query) {
      super(query);
    }

    protected Selector(Selector selector, IAggregate aggregate) {
      super(selector, aggregate);
    }

    @Override
    protected Selector aggregateSegment(IAggregate aggregate) {
      return new Selector(this, aggregate);
    }

    public Selector id(String _alias_) {
      return this.process(StudentScoreMapping.id, _alias_);
    }

    public Selector gmtCreated(String _alias_) {
      return this.process(StudentScoreMapping.gmtCreated, _alias_);
    }

    public Selector gmtModified(String _alias_) {
      return this.process(StudentScoreMapping.gmtModified, _alias_);
    }

    public Selector isDeleted(String _alias_) {
      return this.process(StudentScoreMapping.isDeleted, _alias_);
    }

    public Selector schoolTerm(String _alias_) {
      return this.process(StudentScoreMapping.schoolTerm, _alias_);
    }

    public Selector score(String _alias_) {
      return this.process(StudentScoreMapping.score, _alias_);
    }

    public Selector studentId(String _alias_) {
      return this.process(StudentScoreMapping.studentId, _alias_);
    }

    public Selector subject(String _alias_) {
      return this.process(StudentScoreMapping.subject, _alias_);
    }
  }

  /**
   * query where条件设置
   */
  public static class QueryWhere extends WhereBase<QueryWhere, StudentScoreQuery, StudentScoreQuery> {
    public QueryWhere(StudentScoreQuery query) {
      super(query);
    }

    private QueryWhere(StudentScoreQuery query, QueryWhere where) {
      super(query, where);
    }

    @Override
    protected QueryWhere buildOr(QueryWhere and) {
      return new QueryWhere((StudentScoreQuery) this.wrapper, and);
    }

    @Override
    public QueryWhere defaults() {
      defaults.setQueryDefault((StudentScoreQuery) super.wrapper);
      return super.and;
    }

    public NumericWhere<QueryWhere, StudentScoreQuery> id() {
      return this.set(StudentScoreMapping.id);
    }

    public ObjectWhere<QueryWhere, StudentScoreQuery> gmtCreated() {
      return this.set(StudentScoreMapping.gmtCreated);
    }

    public ObjectWhere<QueryWhere, StudentScoreQuery> gmtModified() {
      return this.set(StudentScoreMapping.gmtModified);
    }

    public BooleanWhere<QueryWhere, StudentScoreQuery> isDeleted() {
      return this.set(StudentScoreMapping.isDeleted);
    }

    public NumericWhere<QueryWhere, StudentScoreQuery> schoolTerm() {
      return this.set(StudentScoreMapping.schoolTerm);
    }

    public NumericWhere<QueryWhere, StudentScoreQuery> score() {
      return this.set(StudentScoreMapping.score);
    }

    public NumericWhere<QueryWhere, StudentScoreQuery> studentId() {
      return this.set(StudentScoreMapping.studentId);
    }

    public StringWhere<QueryWhere, StudentScoreQuery> subject() {
      return this.set(StudentScoreMapping.subject);
    }
  }

  /**
   * update where条件设置
   */
  public static class UpdateWhere extends WhereBase<UpdateWhere, StudentScoreUpdate, StudentScoreQuery> {
    public UpdateWhere(StudentScoreUpdate updater) {
      super(updater);
    }

    private UpdateWhere(StudentScoreUpdate updater, UpdateWhere where) {
      super(updater, where);
    }

    @Override
    protected UpdateWhere buildOr(UpdateWhere and) {
      return new UpdateWhere((StudentScoreUpdate) this.wrapper, and);
    }

    @Override
    public UpdateWhere defaults() {
      defaults.setUpdateDefault((StudentScoreUpdate) super.wrapper);
      return super.and;
    }

    public NumericWhere<UpdateWhere, StudentScoreQuery> id() {
      return this.set(StudentScoreMapping.id);
    }

    public ObjectWhere<UpdateWhere, StudentScoreQuery> gmtCreated() {
      return this.set(StudentScoreMapping.gmtCreated);
    }

    public ObjectWhere<UpdateWhere, StudentScoreQuery> gmtModified() {
      return this.set(StudentScoreMapping.gmtModified);
    }

    public BooleanWhere<UpdateWhere, StudentScoreQuery> isDeleted() {
      return this.set(StudentScoreMapping.isDeleted);
    }

    public NumericWhere<UpdateWhere, StudentScoreQuery> schoolTerm() {
      return this.set(StudentScoreMapping.schoolTerm);
    }

    public NumericWhere<UpdateWhere, StudentScoreQuery> score() {
      return this.set(StudentScoreMapping.score);
    }

    public NumericWhere<UpdateWhere, StudentScoreQuery> studentId() {
      return this.set(StudentScoreMapping.studentId);
    }

    public StringWhere<UpdateWhere, StudentScoreQuery> subject() {
      return this.set(StudentScoreMapping.subject);
    }
  }

  /**
   * 分组设置
   */
  public static final class GroupBy extends GroupByBase<GroupBy, StudentScoreQuery> implements ISegment<GroupBy> {
    public GroupBy(StudentScoreQuery query) {
      super(query);
    }
  }

  /**
   * 分组Having条件设置
   */
  public static final class Having extends HavingBase<Having, StudentScoreQuery> implements ISegment<HavingOperator<Having>> {
    public Having(StudentScoreQuery query) {
      super(query);
    }

    protected Having(Having having, IAggregate aggregate) {
      super(having, aggregate);
    }

    @Override
    protected Having aggregateSegment(IAggregate aggregate) {
      return new Having(this, aggregate);
    }
  }

  /**
   * Query OrderBy设置
   */
  public static final class QueryOrderBy extends OrderByBase<QueryOrderBy, StudentScoreQuery> implements ISegment<OrderByApply<QueryOrderBy, StudentScoreQuery>> {
    public QueryOrderBy(StudentScoreQuery query) {
      super(query);
    }
  }

  /**
   * Update OrderBy设置
   */
  public static final class UpdateOrderBy extends OrderByBase<UpdateOrderBy, StudentScoreUpdate> implements ISegment<OrderByApply<UpdateOrderBy, StudentScoreUpdate>> {
    public UpdateOrderBy(StudentScoreUpdate updater) {
      super(updater);
    }
  }

  /**
   * Update set 设置
   */
  public static final class UpdateSetter extends UpdateBase<UpdateSetter, StudentScoreUpdate> implements ISegment<UpdateApply<UpdateSetter, StudentScoreUpdate>> {
    public UpdateSetter(StudentScoreUpdate updater) {
      super(updater);
    }
  }
}
