package io.demo.domain.helper;

import static io.demo.domain.helper.StudentScoreMapping.Table_Name;

import cn.org.atool.fluent.mybatis.base.crud.BaseDefault;
import cn.org.atool.fluent.mybatis.base.crud.IDefaultSetter;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import cn.org.atool.fluent.mybatis.segment.model.Parameters;
import io.demo.domain.entity.StudentScoreEntity;
import io.demo.domain.wrapper.StudentScoreQuery;
import io.demo.domain.wrapper.StudentScoreUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * StudentScoreDefaults
 *
 * @author powered by FluentMybatis
 */
public class StudentScoreDefaults extends BaseDefault<StudentScoreEntity, StudentScoreQuery, StudentScoreUpdate, StudentScoreDefaults> implements IDefaultSetter {
  public static final StudentScoreDefaults INSTANCE = new StudentScoreDefaults();

  private StudentScoreDefaults() {
    super(Table_Name, "demo", DbType.MYSQL);
  }

  @Override
  public StudentScoreQuery query() {
    return new StudentScoreQuery();
  }

  @Override
  public StudentScoreUpdate updater() {
    return new StudentScoreUpdate();
  }

  /**
   * 自动分配表别名查询构造器(join查询的时候需要定义表别名)
   * 如果要自定义别名, 使用方法 {@link #aliasQuery(String)}
   */
  @Override
  public StudentScoreQuery aliasQuery(String alias, Parameters parameters) {
    return new StudentScoreQuery(alias, parameters);
  }
}
