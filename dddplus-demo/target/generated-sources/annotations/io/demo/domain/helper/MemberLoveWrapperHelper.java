package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.assertNotNull;

import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.functions.IAggregate;
import cn.org.atool.fluent.mybatis.segment.GroupByBase;
import cn.org.atool.fluent.mybatis.segment.HavingBase;
import cn.org.atool.fluent.mybatis.segment.HavingOperator;
import cn.org.atool.fluent.mybatis.segment.OrderByApply;
import cn.org.atool.fluent.mybatis.segment.OrderByBase;
import cn.org.atool.fluent.mybatis.segment.SelectorBase;
import cn.org.atool.fluent.mybatis.segment.UpdateApply;
import cn.org.atool.fluent.mybatis.segment.UpdateBase;
import cn.org.atool.fluent.mybatis.segment.WhereBase;
import cn.org.atool.fluent.mybatis.segment.where.BooleanWhere;
import cn.org.atool.fluent.mybatis.segment.where.NumericWhere;
import cn.org.atool.fluent.mybatis.segment.where.ObjectWhere;
import cn.org.atool.fluent.mybatis.segment.where.StringWhere;
import io.demo.domain.wrapper.MemberLoveQuery;
import io.demo.domain.wrapper.MemberLoveUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * MemberLoveWrapperHelper
 *
 * @author powered by FluentMybatis
 */
public class MemberLoveWrapperHelper {
  /**
   * 默认设置器
   */
  private static final MemberLoveDefaults defaults = MemberLoveDefaults.INSTANCE;

  public interface ISegment<R> {
    R set(FieldMapping fieldMapping);

    default R id() {
      return this.set(MemberLoveMapping.id);
    }

    default R gmtCreated() {
      return this.set(MemberLoveMapping.gmtCreated);
    }

    default R gmtModified() {
      return this.set(MemberLoveMapping.gmtModified);
    }

    default R isDeleted() {
      return this.set(MemberLoveMapping.isDeleted);
    }

    default R boyId() {
      return this.set(MemberLoveMapping.boyId);
    }

    default R girlId() {
      return this.set(MemberLoveMapping.girlId);
    }

    default R status() {
      return this.set(MemberLoveMapping.status);
    }
  }

  /**
   * select字段设置
   */
  public static final class Selector extends SelectorBase<Selector, MemberLoveQuery> implements ISegment<Selector> {
    public Selector(MemberLoveQuery query) {
      super(query);
    }

    protected Selector(Selector selector, IAggregate aggregate) {
      super(selector, aggregate);
    }

    @Override
    protected Selector aggregateSegment(IAggregate aggregate) {
      return new Selector(this, aggregate);
    }

    public Selector id(String _alias_) {
      return this.process(MemberLoveMapping.id, _alias_);
    }

    public Selector gmtCreated(String _alias_) {
      return this.process(MemberLoveMapping.gmtCreated, _alias_);
    }

    public Selector gmtModified(String _alias_) {
      return this.process(MemberLoveMapping.gmtModified, _alias_);
    }

    public Selector isDeleted(String _alias_) {
      return this.process(MemberLoveMapping.isDeleted, _alias_);
    }

    public Selector boyId(String _alias_) {
      return this.process(MemberLoveMapping.boyId, _alias_);
    }

    public Selector girlId(String _alias_) {
      return this.process(MemberLoveMapping.girlId, _alias_);
    }

    public Selector status(String _alias_) {
      return this.process(MemberLoveMapping.status, _alias_);
    }
  }

  /**
   * query where条件设置
   */
  public static class QueryWhere extends WhereBase<QueryWhere, MemberLoveQuery, MemberLoveQuery> {
    public QueryWhere(MemberLoveQuery query) {
      super(query);
    }

    private QueryWhere(MemberLoveQuery query, QueryWhere where) {
      super(query, where);
    }

    @Override
    protected QueryWhere buildOr(QueryWhere and) {
      return new QueryWhere((MemberLoveQuery) this.wrapper, and);
    }

    @Override
    public QueryWhere defaults() {
      defaults.setQueryDefault((MemberLoveQuery) super.wrapper);
      return super.and;
    }

    public NumericWhere<QueryWhere, MemberLoveQuery> id() {
      return this.set(MemberLoveMapping.id);
    }

    public ObjectWhere<QueryWhere, MemberLoveQuery> gmtCreated() {
      return this.set(MemberLoveMapping.gmtCreated);
    }

    public ObjectWhere<QueryWhere, MemberLoveQuery> gmtModified() {
      return this.set(MemberLoveMapping.gmtModified);
    }

    public BooleanWhere<QueryWhere, MemberLoveQuery> isDeleted() {
      return this.set(MemberLoveMapping.isDeleted);
    }

    public NumericWhere<QueryWhere, MemberLoveQuery> boyId() {
      return this.set(MemberLoveMapping.boyId);
    }

    public NumericWhere<QueryWhere, MemberLoveQuery> girlId() {
      return this.set(MemberLoveMapping.girlId);
    }

    public StringWhere<QueryWhere, MemberLoveQuery> status() {
      return this.set(MemberLoveMapping.status);
    }
  }

  /**
   * update where条件设置
   */
  public static class UpdateWhere extends WhereBase<UpdateWhere, MemberLoveUpdate, MemberLoveQuery> {
    public UpdateWhere(MemberLoveUpdate updater) {
      super(updater);
    }

    private UpdateWhere(MemberLoveUpdate updater, UpdateWhere where) {
      super(updater, where);
    }

    @Override
    protected UpdateWhere buildOr(UpdateWhere and) {
      return new UpdateWhere((MemberLoveUpdate) this.wrapper, and);
    }

    @Override
    public UpdateWhere defaults() {
      defaults.setUpdateDefault((MemberLoveUpdate) super.wrapper);
      return super.and;
    }

    public NumericWhere<UpdateWhere, MemberLoveQuery> id() {
      return this.set(MemberLoveMapping.id);
    }

    public ObjectWhere<UpdateWhere, MemberLoveQuery> gmtCreated() {
      return this.set(MemberLoveMapping.gmtCreated);
    }

    public ObjectWhere<UpdateWhere, MemberLoveQuery> gmtModified() {
      return this.set(MemberLoveMapping.gmtModified);
    }

    public BooleanWhere<UpdateWhere, MemberLoveQuery> isDeleted() {
      return this.set(MemberLoveMapping.isDeleted);
    }

    public NumericWhere<UpdateWhere, MemberLoveQuery> boyId() {
      return this.set(MemberLoveMapping.boyId);
    }

    public NumericWhere<UpdateWhere, MemberLoveQuery> girlId() {
      return this.set(MemberLoveMapping.girlId);
    }

    public StringWhere<UpdateWhere, MemberLoveQuery> status() {
      return this.set(MemberLoveMapping.status);
    }
  }

  /**
   * 分组设置
   */
  public static final class GroupBy extends GroupByBase<GroupBy, MemberLoveQuery> implements ISegment<GroupBy> {
    public GroupBy(MemberLoveQuery query) {
      super(query);
    }
  }

  /**
   * 分组Having条件设置
   */
  public static final class Having extends HavingBase<Having, MemberLoveQuery> implements ISegment<HavingOperator<Having>> {
    public Having(MemberLoveQuery query) {
      super(query);
    }

    protected Having(Having having, IAggregate aggregate) {
      super(having, aggregate);
    }

    @Override
    protected Having aggregateSegment(IAggregate aggregate) {
      return new Having(this, aggregate);
    }
  }

  /**
   * Query OrderBy设置
   */
  public static final class QueryOrderBy extends OrderByBase<QueryOrderBy, MemberLoveQuery> implements ISegment<OrderByApply<QueryOrderBy, MemberLoveQuery>> {
    public QueryOrderBy(MemberLoveQuery query) {
      super(query);
    }
  }

  /**
   * Update OrderBy设置
   */
  public static final class UpdateOrderBy extends OrderByBase<UpdateOrderBy, MemberLoveUpdate> implements ISegment<OrderByApply<UpdateOrderBy, MemberLoveUpdate>> {
    public UpdateOrderBy(MemberLoveUpdate updater) {
      super(updater);
    }
  }

  /**
   * Update set 设置
   */
  public static final class UpdateSetter extends UpdateBase<UpdateSetter, MemberLoveUpdate> implements ISegment<UpdateApply<UpdateSetter, MemberLoveUpdate>> {
    public UpdateSetter(MemberLoveUpdate updater) {
      super(updater);
    }
  }
}
