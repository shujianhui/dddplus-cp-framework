package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.assertNotNull;

import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.functions.IAggregate;
import cn.org.atool.fluent.mybatis.segment.GroupByBase;
import cn.org.atool.fluent.mybatis.segment.HavingBase;
import cn.org.atool.fluent.mybatis.segment.HavingOperator;
import cn.org.atool.fluent.mybatis.segment.OrderByApply;
import cn.org.atool.fluent.mybatis.segment.OrderByBase;
import cn.org.atool.fluent.mybatis.segment.SelectorBase;
import cn.org.atool.fluent.mybatis.segment.UpdateApply;
import cn.org.atool.fluent.mybatis.segment.UpdateBase;
import cn.org.atool.fluent.mybatis.segment.WhereBase;
import cn.org.atool.fluent.mybatis.segment.where.BooleanWhere;
import cn.org.atool.fluent.mybatis.segment.where.NumericWhere;
import cn.org.atool.fluent.mybatis.segment.where.ObjectWhere;
import cn.org.atool.fluent.mybatis.segment.where.StringWhere;
import io.demo.domain.wrapper.StudentQuery;
import io.demo.domain.wrapper.StudentUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * StudentWrapperHelper
 *
 * @author powered by FluentMybatis
 */
public class StudentWrapperHelper {
  /**
   * 默认设置器
   */
  private static final StudentDefaults defaults = StudentDefaults.INSTANCE;

  public interface ISegment<R> {
    R set(FieldMapping fieldMapping);

    default R id() {
      return this.set(StudentMapping.id);
    }

    default R gmtCreated() {
      return this.set(StudentMapping.gmtCreated);
    }

    default R gmtModified() {
      return this.set(StudentMapping.gmtModified);
    }

    default R isDeleted() {
      return this.set(StudentMapping.isDeleted);
    }

    default R address() {
      return this.set(StudentMapping.address);
    }

    default R addressId() {
      return this.set(StudentMapping.addressId);
    }

    default R age() {
      return this.set(StudentMapping.age);
    }

    default R birthday() {
      return this.set(StudentMapping.birthday);
    }

    default R bonusPoints() {
      return this.set(StudentMapping.bonusPoints);
    }

    default R genderMan() {
      return this.set(StudentMapping.genderMan);
    }

    default R grade() {
      return this.set(StudentMapping.grade);
    }

    default R homeCountyId() {
      return this.set(StudentMapping.homeCountyId);
    }

    default R phone() {
      return this.set(StudentMapping.phone);
    }

    default R status() {
      return this.set(StudentMapping.status);
    }

    default R userName() {
      return this.set(StudentMapping.userName);
    }
  }

  /**
   * select字段设置
   */
  public static final class Selector extends SelectorBase<Selector, StudentQuery> implements ISegment<Selector> {
    public Selector(StudentQuery query) {
      super(query);
    }

    protected Selector(Selector selector, IAggregate aggregate) {
      super(selector, aggregate);
    }

    @Override
    protected Selector aggregateSegment(IAggregate aggregate) {
      return new Selector(this, aggregate);
    }

    public Selector id(String _alias_) {
      return this.process(StudentMapping.id, _alias_);
    }

    public Selector gmtCreated(String _alias_) {
      return this.process(StudentMapping.gmtCreated, _alias_);
    }

    public Selector gmtModified(String _alias_) {
      return this.process(StudentMapping.gmtModified, _alias_);
    }

    public Selector isDeleted(String _alias_) {
      return this.process(StudentMapping.isDeleted, _alias_);
    }

    public Selector address(String _alias_) {
      return this.process(StudentMapping.address, _alias_);
    }

    public Selector addressId(String _alias_) {
      return this.process(StudentMapping.addressId, _alias_);
    }

    public Selector age(String _alias_) {
      return this.process(StudentMapping.age, _alias_);
    }

    public Selector birthday(String _alias_) {
      return this.process(StudentMapping.birthday, _alias_);
    }

    public Selector bonusPoints(String _alias_) {
      return this.process(StudentMapping.bonusPoints, _alias_);
    }

    public Selector genderMan(String _alias_) {
      return this.process(StudentMapping.genderMan, _alias_);
    }

    public Selector grade(String _alias_) {
      return this.process(StudentMapping.grade, _alias_);
    }

    public Selector homeCountyId(String _alias_) {
      return this.process(StudentMapping.homeCountyId, _alias_);
    }

    public Selector phone(String _alias_) {
      return this.process(StudentMapping.phone, _alias_);
    }

    public Selector status(String _alias_) {
      return this.process(StudentMapping.status, _alias_);
    }

    public Selector userName(String _alias_) {
      return this.process(StudentMapping.userName, _alias_);
    }
  }

  /**
   * query where条件设置
   */
  public static class QueryWhere extends WhereBase<QueryWhere, StudentQuery, StudentQuery> {
    public QueryWhere(StudentQuery query) {
      super(query);
    }

    private QueryWhere(StudentQuery query, QueryWhere where) {
      super(query, where);
    }

    @Override
    protected QueryWhere buildOr(QueryWhere and) {
      return new QueryWhere((StudentQuery) this.wrapper, and);
    }

    @Override
    public QueryWhere defaults() {
      defaults.setQueryDefault((StudentQuery) super.wrapper);
      return super.and;
    }

    public NumericWhere<QueryWhere, StudentQuery> id() {
      return this.set(StudentMapping.id);
    }

    public ObjectWhere<QueryWhere, StudentQuery> gmtCreated() {
      return this.set(StudentMapping.gmtCreated);
    }

    public ObjectWhere<QueryWhere, StudentQuery> gmtModified() {
      return this.set(StudentMapping.gmtModified);
    }

    public BooleanWhere<QueryWhere, StudentQuery> isDeleted() {
      return this.set(StudentMapping.isDeleted);
    }

    public StringWhere<QueryWhere, StudentQuery> address() {
      return this.set(StudentMapping.address);
    }

    public NumericWhere<QueryWhere, StudentQuery> addressId() {
      return this.set(StudentMapping.addressId);
    }

    public NumericWhere<QueryWhere, StudentQuery> age() {
      return this.set(StudentMapping.age);
    }

    public ObjectWhere<QueryWhere, StudentQuery> birthday() {
      return this.set(StudentMapping.birthday);
    }

    public NumericWhere<QueryWhere, StudentQuery> bonusPoints() {
      return this.set(StudentMapping.bonusPoints);
    }

    public BooleanWhere<QueryWhere, StudentQuery> genderMan() {
      return this.set(StudentMapping.genderMan);
    }

    public NumericWhere<QueryWhere, StudentQuery> grade() {
      return this.set(StudentMapping.grade);
    }

    public NumericWhere<QueryWhere, StudentQuery> homeCountyId() {
      return this.set(StudentMapping.homeCountyId);
    }

    public StringWhere<QueryWhere, StudentQuery> phone() {
      return this.set(StudentMapping.phone);
    }

    public StringWhere<QueryWhere, StudentQuery> status() {
      return this.set(StudentMapping.status);
    }

    public StringWhere<QueryWhere, StudentQuery> userName() {
      return this.set(StudentMapping.userName);
    }
  }

  /**
   * update where条件设置
   */
  public static class UpdateWhere extends WhereBase<UpdateWhere, StudentUpdate, StudentQuery> {
    public UpdateWhere(StudentUpdate updater) {
      super(updater);
    }

    private UpdateWhere(StudentUpdate updater, UpdateWhere where) {
      super(updater, where);
    }

    @Override
    protected UpdateWhere buildOr(UpdateWhere and) {
      return new UpdateWhere((StudentUpdate) this.wrapper, and);
    }

    @Override
    public UpdateWhere defaults() {
      defaults.setUpdateDefault((StudentUpdate) super.wrapper);
      return super.and;
    }

    public NumericWhere<UpdateWhere, StudentQuery> id() {
      return this.set(StudentMapping.id);
    }

    public ObjectWhere<UpdateWhere, StudentQuery> gmtCreated() {
      return this.set(StudentMapping.gmtCreated);
    }

    public ObjectWhere<UpdateWhere, StudentQuery> gmtModified() {
      return this.set(StudentMapping.gmtModified);
    }

    public BooleanWhere<UpdateWhere, StudentQuery> isDeleted() {
      return this.set(StudentMapping.isDeleted);
    }

    public StringWhere<UpdateWhere, StudentQuery> address() {
      return this.set(StudentMapping.address);
    }

    public NumericWhere<UpdateWhere, StudentQuery> addressId() {
      return this.set(StudentMapping.addressId);
    }

    public NumericWhere<UpdateWhere, StudentQuery> age() {
      return this.set(StudentMapping.age);
    }

    public ObjectWhere<UpdateWhere, StudentQuery> birthday() {
      return this.set(StudentMapping.birthday);
    }

    public NumericWhere<UpdateWhere, StudentQuery> bonusPoints() {
      return this.set(StudentMapping.bonusPoints);
    }

    public BooleanWhere<UpdateWhere, StudentQuery> genderMan() {
      return this.set(StudentMapping.genderMan);
    }

    public NumericWhere<UpdateWhere, StudentQuery> grade() {
      return this.set(StudentMapping.grade);
    }

    public NumericWhere<UpdateWhere, StudentQuery> homeCountyId() {
      return this.set(StudentMapping.homeCountyId);
    }

    public StringWhere<UpdateWhere, StudentQuery> phone() {
      return this.set(StudentMapping.phone);
    }

    public StringWhere<UpdateWhere, StudentQuery> status() {
      return this.set(StudentMapping.status);
    }

    public StringWhere<UpdateWhere, StudentQuery> userName() {
      return this.set(StudentMapping.userName);
    }
  }

  /**
   * 分组设置
   */
  public static final class GroupBy extends GroupByBase<GroupBy, StudentQuery> implements ISegment<GroupBy> {
    public GroupBy(StudentQuery query) {
      super(query);
    }
  }

  /**
   * 分组Having条件设置
   */
  public static final class Having extends HavingBase<Having, StudentQuery> implements ISegment<HavingOperator<Having>> {
    public Having(StudentQuery query) {
      super(query);
    }

    protected Having(Having having, IAggregate aggregate) {
      super(having, aggregate);
    }

    @Override
    protected Having aggregateSegment(IAggregate aggregate) {
      return new Having(this, aggregate);
    }
  }

  /**
   * Query OrderBy设置
   */
  public static final class QueryOrderBy extends OrderByBase<QueryOrderBy, StudentQuery> implements ISegment<OrderByApply<QueryOrderBy, StudentQuery>> {
    public QueryOrderBy(StudentQuery query) {
      super(query);
    }
  }

  /**
   * Update OrderBy设置
   */
  public static final class UpdateOrderBy extends OrderByBase<UpdateOrderBy, StudentUpdate> implements ISegment<OrderByApply<UpdateOrderBy, StudentUpdate>> {
    public UpdateOrderBy(StudentUpdate updater) {
      super(updater);
    }
  }

  /**
   * Update set 设置
   */
  public static final class UpdateSetter extends UpdateBase<UpdateSetter, StudentUpdate> implements ISegment<UpdateApply<UpdateSetter, StudentUpdate>> {
    public UpdateSetter(StudentUpdate updater) {
      super(updater);
    }
  }
}
