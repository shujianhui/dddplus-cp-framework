package io.demo.domain.helper;

import static java.util.Optional.ofNullable;

import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldType;
import java.lang.Override;
import java.lang.String;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 * UserMapping: Entity类字段和表结构映射
 *
 * @author powered by FluentMybatis
 */
public interface UserMapping extends IMapping {
  UserMapping MAPPING = new UserMapping(){};

  /**
   * 表名称
   */
  String Table_Name = "t_user_table";

  /**
   * Entity名称
   */
  String Entity_Name = "UserEntity";

  /**
   * 实体属性 : 数据库字段 映射
   *  id : id
   */
  FieldMapping id = new FieldMapping("id", "id");

  /**
   * 实体属性 : 数据库字段 映射
   *  gmtCreated : gmt_created
   */
  FieldMapping gmtCreated = new FieldMapping("gmtCreated", "gmt_created");

  /**
   * 实体属性 : 数据库字段 映射
   *  gmtModified : gmt_modified
   */
  FieldMapping gmtModified = new FieldMapping("gmtModified", "gmt_modified");

  /**
   * 实体属性 : 数据库字段 映射
   *  isDeleted : is_deleted
   */
  FieldMapping isDeleted = new FieldMapping("isDeleted", "is_deleted");

  /**
   * 实体属性 : 数据库字段 映射
   *  age : age
   */
  FieldMapping age = new FieldMapping("age", "age");

  /**
   * 实体属性 : 数据库字段 映射
   *  email : email
   */
  FieldMapping email = new FieldMapping("email", "email");

  /**
   * 实体属性 : 数据库字段 映射
   *  name : name
   */
  FieldMapping name = new FieldMapping("name", "name");

  /**
   * 实例属性和数据库字段对应表
   */
  Map<String, String> Property2Column = new HashMap<String, String>() {
    {
  		this.put(id.name, id.column);
  		this.put(gmtCreated.name, gmtCreated.column);
  		this.put(gmtModified.name, gmtModified.column);
  		this.put(isDeleted.name, isDeleted.column);
  		this.put(age.name, age.column);
  		this.put(email.name, email.column);
  		this.put(name.name, name.column);
    }
  };

  /**
   * 数据库字段对应的FieldMapping
   */
  Map<String, FieldMapping> Column2Mapping = new HashMap<String, FieldMapping>() {
    {
  		this.put(id.column, id);
  		this.put(gmtCreated.column, gmtCreated);
  		this.put(gmtModified.column, gmtModified);
  		this.put(isDeleted.column, isDeleted);
  		this.put(age.column, age);
  		this.put(email.column, email);
  		this.put(name.column, name);
    }
  };

  /**
   * 数据库所有字段列表
   */
  List<String> ALL_COLUMNS = Arrays.asList(
  		id.column,
  		gmtCreated.column,
  		gmtModified.column,
  		isDeleted.column,
  		age.column,
  		email.column,
  		name.column
  );

  /**
   * 数据库所有字段列表用逗号分隔
   */
  String ALL_JOIN_COLUMNS = String.join(", ", ALL_COLUMNS);

  @Override
  default String findColumnByField(String field) {
    return Property2Column.get(field);
  }

  @Override
  default Optional<FieldMapping> findField(FieldType type) {
    switch (type) {
    	case PRIMARY_ID:
    		return ofNullable(id);
    	case LOGIC_DELETED:
    		return ofNullable(isDeleted);
    	default:
    		return ofNullable(null);
    }
  }
}
