package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.assertNotNull;

import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.functions.IAggregate;
import cn.org.atool.fluent.mybatis.segment.GroupByBase;
import cn.org.atool.fluent.mybatis.segment.HavingBase;
import cn.org.atool.fluent.mybatis.segment.HavingOperator;
import cn.org.atool.fluent.mybatis.segment.OrderByApply;
import cn.org.atool.fluent.mybatis.segment.OrderByBase;
import cn.org.atool.fluent.mybatis.segment.SelectorBase;
import cn.org.atool.fluent.mybatis.segment.UpdateApply;
import cn.org.atool.fluent.mybatis.segment.UpdateBase;
import cn.org.atool.fluent.mybatis.segment.WhereBase;
import cn.org.atool.fluent.mybatis.segment.where.BooleanWhere;
import cn.org.atool.fluent.mybatis.segment.where.NumericWhere;
import cn.org.atool.fluent.mybatis.segment.where.ObjectWhere;
import cn.org.atool.fluent.mybatis.segment.where.StringWhere;
import io.demo.domain.wrapper.ReceivingAddressQuery;
import io.demo.domain.wrapper.ReceivingAddressUpdate;
import java.lang.Override;
import java.lang.String;

/**
 *
 * ReceivingAddressWrapperHelper
 *
 * @author powered by FluentMybatis
 */
public class ReceivingAddressWrapperHelper {
  /**
   * 默认设置器
   */
  private static final ReceivingAddressDefaults defaults = ReceivingAddressDefaults.INSTANCE;

  public interface ISegment<R> {
    R set(FieldMapping fieldMapping);

    default R id() {
      return this.set(ReceivingAddressMapping.id);
    }

    default R gmtModified() {
      return this.set(ReceivingAddressMapping.gmtModified);
    }

    default R isDeleted() {
      return this.set(ReceivingAddressMapping.isDeleted);
    }

    default R city() {
      return this.set(ReceivingAddressMapping.city);
    }

    default R detailAddress() {
      return this.set(ReceivingAddressMapping.detailAddress);
    }

    default R district() {
      return this.set(ReceivingAddressMapping.district);
    }

    default R gmtCreate() {
      return this.set(ReceivingAddressMapping.gmtCreate);
    }

    default R province() {
      return this.set(ReceivingAddressMapping.province);
    }

    default R userId() {
      return this.set(ReceivingAddressMapping.userId);
    }
  }

  /**
   * select字段设置
   */
  public static final class Selector extends SelectorBase<Selector, ReceivingAddressQuery> implements ISegment<Selector> {
    public Selector(ReceivingAddressQuery query) {
      super(query);
    }

    protected Selector(Selector selector, IAggregate aggregate) {
      super(selector, aggregate);
    }

    @Override
    protected Selector aggregateSegment(IAggregate aggregate) {
      return new Selector(this, aggregate);
    }

    public Selector id(String _alias_) {
      return this.process(ReceivingAddressMapping.id, _alias_);
    }

    public Selector gmtModified(String _alias_) {
      return this.process(ReceivingAddressMapping.gmtModified, _alias_);
    }

    public Selector isDeleted(String _alias_) {
      return this.process(ReceivingAddressMapping.isDeleted, _alias_);
    }

    public Selector city(String _alias_) {
      return this.process(ReceivingAddressMapping.city, _alias_);
    }

    public Selector detailAddress(String _alias_) {
      return this.process(ReceivingAddressMapping.detailAddress, _alias_);
    }

    public Selector district(String _alias_) {
      return this.process(ReceivingAddressMapping.district, _alias_);
    }

    public Selector gmtCreate(String _alias_) {
      return this.process(ReceivingAddressMapping.gmtCreate, _alias_);
    }

    public Selector province(String _alias_) {
      return this.process(ReceivingAddressMapping.province, _alias_);
    }

    public Selector userId(String _alias_) {
      return this.process(ReceivingAddressMapping.userId, _alias_);
    }
  }

  /**
   * query where条件设置
   */
  public static class QueryWhere extends WhereBase<QueryWhere, ReceivingAddressQuery, ReceivingAddressQuery> {
    public QueryWhere(ReceivingAddressQuery query) {
      super(query);
    }

    private QueryWhere(ReceivingAddressQuery query, QueryWhere where) {
      super(query, where);
    }

    @Override
    protected QueryWhere buildOr(QueryWhere and) {
      return new QueryWhere((ReceivingAddressQuery) this.wrapper, and);
    }

    @Override
    public QueryWhere defaults() {
      defaults.setQueryDefault((ReceivingAddressQuery) super.wrapper);
      return super.and;
    }

    public NumericWhere<QueryWhere, ReceivingAddressQuery> id() {
      return this.set(ReceivingAddressMapping.id);
    }

    public ObjectWhere<QueryWhere, ReceivingAddressQuery> gmtModified() {
      return this.set(ReceivingAddressMapping.gmtModified);
    }

    public BooleanWhere<QueryWhere, ReceivingAddressQuery> isDeleted() {
      return this.set(ReceivingAddressMapping.isDeleted);
    }

    public StringWhere<QueryWhere, ReceivingAddressQuery> city() {
      return this.set(ReceivingAddressMapping.city);
    }

    public StringWhere<QueryWhere, ReceivingAddressQuery> detailAddress() {
      return this.set(ReceivingAddressMapping.detailAddress);
    }

    public StringWhere<QueryWhere, ReceivingAddressQuery> district() {
      return this.set(ReceivingAddressMapping.district);
    }

    public ObjectWhere<QueryWhere, ReceivingAddressQuery> gmtCreate() {
      return this.set(ReceivingAddressMapping.gmtCreate);
    }

    public StringWhere<QueryWhere, ReceivingAddressQuery> province() {
      return this.set(ReceivingAddressMapping.province);
    }

    public NumericWhere<QueryWhere, ReceivingAddressQuery> userId() {
      return this.set(ReceivingAddressMapping.userId);
    }
  }

  /**
   * update where条件设置
   */
  public static class UpdateWhere extends WhereBase<UpdateWhere, ReceivingAddressUpdate, ReceivingAddressQuery> {
    public UpdateWhere(ReceivingAddressUpdate updater) {
      super(updater);
    }

    private UpdateWhere(ReceivingAddressUpdate updater, UpdateWhere where) {
      super(updater, where);
    }

    @Override
    protected UpdateWhere buildOr(UpdateWhere and) {
      return new UpdateWhere((ReceivingAddressUpdate) this.wrapper, and);
    }

    @Override
    public UpdateWhere defaults() {
      defaults.setUpdateDefault((ReceivingAddressUpdate) super.wrapper);
      return super.and;
    }

    public NumericWhere<UpdateWhere, ReceivingAddressQuery> id() {
      return this.set(ReceivingAddressMapping.id);
    }

    public ObjectWhere<UpdateWhere, ReceivingAddressQuery> gmtModified() {
      return this.set(ReceivingAddressMapping.gmtModified);
    }

    public BooleanWhere<UpdateWhere, ReceivingAddressQuery> isDeleted() {
      return this.set(ReceivingAddressMapping.isDeleted);
    }

    public StringWhere<UpdateWhere, ReceivingAddressQuery> city() {
      return this.set(ReceivingAddressMapping.city);
    }

    public StringWhere<UpdateWhere, ReceivingAddressQuery> detailAddress() {
      return this.set(ReceivingAddressMapping.detailAddress);
    }

    public StringWhere<UpdateWhere, ReceivingAddressQuery> district() {
      return this.set(ReceivingAddressMapping.district);
    }

    public ObjectWhere<UpdateWhere, ReceivingAddressQuery> gmtCreate() {
      return this.set(ReceivingAddressMapping.gmtCreate);
    }

    public StringWhere<UpdateWhere, ReceivingAddressQuery> province() {
      return this.set(ReceivingAddressMapping.province);
    }

    public NumericWhere<UpdateWhere, ReceivingAddressQuery> userId() {
      return this.set(ReceivingAddressMapping.userId);
    }
  }

  /**
   * 分组设置
   */
  public static final class GroupBy extends GroupByBase<GroupBy, ReceivingAddressQuery> implements ISegment<GroupBy> {
    public GroupBy(ReceivingAddressQuery query) {
      super(query);
    }
  }

  /**
   * 分组Having条件设置
   */
  public static final class Having extends HavingBase<Having, ReceivingAddressQuery> implements ISegment<HavingOperator<Having>> {
    public Having(ReceivingAddressQuery query) {
      super(query);
    }

    protected Having(Having having, IAggregate aggregate) {
      super(having, aggregate);
    }

    @Override
    protected Having aggregateSegment(IAggregate aggregate) {
      return new Having(this, aggregate);
    }
  }

  /**
   * Query OrderBy设置
   */
  public static final class QueryOrderBy extends OrderByBase<QueryOrderBy, ReceivingAddressQuery> implements ISegment<OrderByApply<QueryOrderBy, ReceivingAddressQuery>> {
    public QueryOrderBy(ReceivingAddressQuery query) {
      super(query);
    }
  }

  /**
   * Update OrderBy设置
   */
  public static final class UpdateOrderBy extends OrderByBase<UpdateOrderBy, ReceivingAddressUpdate> implements ISegment<OrderByApply<UpdateOrderBy, ReceivingAddressUpdate>> {
    public UpdateOrderBy(ReceivingAddressUpdate updater) {
      super(updater);
    }
  }

  /**
   * Update set 设置
   */
  public static final class UpdateSetter extends UpdateBase<UpdateSetter, ReceivingAddressUpdate> implements ISegment<UpdateApply<UpdateSetter, ReceivingAddressUpdate>> {
    public UpdateSetter(ReceivingAddressUpdate updater) {
      super(updater);
    }
  }
}
