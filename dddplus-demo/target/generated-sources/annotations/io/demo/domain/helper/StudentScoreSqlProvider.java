package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.base.model.InsertList.el;
import static cn.org.atool.fluent.mybatis.mapper.FluentConst.*;
import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.*;
import static cn.org.atool.fluent.mybatis.utility.SqlProviderUtils.*;
import static io.demo.domain.helper.StudentScoreMapping.*;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.InsertList;
import cn.org.atool.fluent.mybatis.base.model.UpdateDefault;
import cn.org.atool.fluent.mybatis.base.model.UpdateSet;
import cn.org.atool.fluent.mybatis.base.provider.BaseSqlProvider;
import cn.org.atool.fluent.mybatis.mapper.MapperSql;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import io.demo.domain.entity.StudentScoreEntity;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * StudentScoreSqlProvider: 动态语句封装
 *
 * @author powered by FluentMybatis
 */
public class StudentScoreSqlProvider extends BaseSqlProvider<StudentScoreEntity> {
  /**
   * 默认设置器
   */
  private static final StudentScoreDefaults defaults = StudentScoreDefaults.INSTANCE;

  @Override
  public boolean primaryIsNull(StudentScoreEntity entity) {
    return entity.getId() == null;
  }

  @Override
  public boolean primaryNotNull(StudentScoreEntity entity) {
    return entity.getId() != null;
  }

  @Override
  protected void insertEntity(InsertList inserts, String prefix, StudentScoreEntity entity,
      boolean withPk) {
    if (withPk) {
    	inserts.add(prefix, id, entity.getId(), null);
    }
    inserts.add(prefix, gmtCreated, entity.getGmtCreated(), "now()");
    inserts.add(prefix, gmtModified, entity.getGmtModified(), "now()");
    inserts.add(prefix, isDeleted, entity.getIsDeleted(), "0");
    inserts.add(prefix, schoolTerm, entity.getSchoolTerm(), "");
    inserts.add(prefix, score, entity.getScore(), "");
    inserts.add(prefix, studentId, entity.getStudentId(), "");
    inserts.add(prefix, subject, entity.getSubject(), "");
  }

  @Override
  protected List<String> insertBatchEntity(int index, StudentScoreEntity entity, boolean withPk) {
    List<String> values = new ArrayList<>();
    if (withPk) {
    	values.add(el("list[" + index + "].", id, entity.getId(), null));
    }
    values.add(el("list[" + index + "].", gmtCreated, entity.getGmtCreated(), "now()"));
    values.add(el("list[" + index + "].", gmtModified, entity.getGmtModified(), "now()"));
    values.add(el("list[" + index + "].", isDeleted, entity.getIsDeleted(), "0"));
    values.add(el("list[" + index + "].", schoolTerm, entity.getSchoolTerm(), ""));
    values.add(el("list[" + index + "].", score, entity.getScore(), ""));
    values.add(el("list[" + index + "].", studentId, entity.getStudentId(), ""));
    values.add(el("list[" + index + "].", subject, entity.getSubject(), ""));
    return values;
  }

  public String updateById(Map<String, Object> map) {
    StudentScoreEntity entity = getParas(map, Param_ET);
    assertNotNull(Param_Entity, entity);
    MapperSql sql = new MapperSql();
    sql.UPDATE(this.tableName());
    UpdateSet updates = new UpdateSet()
    	.add(this.dbType(), gmtCreated, entity.getGmtCreated(), "")
    	.add(this.dbType(), gmtModified, entity.getGmtModified(), "now()")
    	.add(this.dbType(), isDeleted, entity.getIsDeleted(), "")
    	.add(this.dbType(), schoolTerm, entity.getSchoolTerm(), "")
    	.add(this.dbType(), score, entity.getScore(), "")
    	.add(this.dbType(), studentId, entity.getStudentId(), "")
    	.add(this.dbType(), subject, entity.getSubject(), "");
    sql.SET(updates.getUpdates());
    sql.WHERE(id.el(this.dbType(), Param_ET));
    return sql.toString();
  }

  @Override
  public List<String> updateDefaults(Map<String, String> updates, boolean ignoreLockVersion) {
    UpdateDefault defaults = new UpdateDefault(updates);
    defaults.add(dbType(), gmtModified, "now()");
    return defaults.getUpdateDefaults();
  }

  @Override
  public String tableName() {
    return defaults.table().get();
  }

  @Override
  protected IMapping mapping() {
    return MAPPING;
  }

  @Override
  public List<String> allFields(boolean withPk) {
    if (withPk) {
    	return Arrays.asList("id", "gmt_created", "gmt_modified", "is_deleted", "school_term", "score", "student_id", "subject");
    } else {
    	return Arrays.asList("gmt_created", "gmt_modified", "is_deleted", "school_term", "score", "student_id", "subject");
    }
  }

  @Override
  protected void setEntityByDefault(IEntity entity) {
    defaults.setInsertDefault(entity);
  }

  @Override
  public DbType dbType() {
    return DbType.MYSQL;
  }

  @Override
  protected boolean longTypeOfLogicDelete() {
    return false;
  }
}
