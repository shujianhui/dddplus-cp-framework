package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.assertNotNull;

import cn.org.atool.fluent.mybatis.base.crud.FormSetter;
import cn.org.atool.fluent.mybatis.functions.FormApply;
import cn.org.atool.fluent.mybatis.model.Form;
import cn.org.atool.fluent.mybatis.model.IFormApply;
import cn.org.atool.fluent.mybatis.utility.PoJoHelper;
import io.demo.domain.entity.MemberFavoriteEntity;
import io.demo.domain.helper.MemberFavoriteWrapperHelper.ISegment;
import java.lang.Class;
import java.lang.Object;
import java.lang.Override;
import java.util.Map;
import java.util.function.Function;

/**
 *
 * MemberFavoriteFormSetter: Form Column Setter
 *
 * @author powered by FluentMybatis
 */
public final class MemberFavoriteFormSetter extends FormSetter implements ISegment<IFormApply<MemberFavoriteEntity, MemberFavoriteFormSetter>> {
  private MemberFavoriteFormSetter(FormApply apply) {
    super.formApply = apply;
  }

  @Override
  public Class entityClass() {
    return MemberFavoriteEntity.class;
  }

  public static IFormApply<MemberFavoriteEntity, MemberFavoriteFormSetter> by(Object object,
      Form form) {
    assertNotNull("object", object);
    Map map = PoJoHelper.toMap(object);
    Function<FormApply, FormSetter> apply = MemberFavoriteFormSetter::new;
    return new FormApply<>(apply, map, form);
  }
}
