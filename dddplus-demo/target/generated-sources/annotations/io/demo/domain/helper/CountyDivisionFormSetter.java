package io.demo.domain.helper;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.assertNotNull;

import cn.org.atool.fluent.mybatis.base.crud.FormSetter;
import cn.org.atool.fluent.mybatis.functions.FormApply;
import cn.org.atool.fluent.mybatis.model.Form;
import cn.org.atool.fluent.mybatis.model.IFormApply;
import cn.org.atool.fluent.mybatis.utility.PoJoHelper;
import io.demo.domain.entity.CountyDivisionEntity;
import io.demo.domain.helper.CountyDivisionWrapperHelper.ISegment;
import java.lang.Class;
import java.lang.Object;
import java.lang.Override;
import java.util.Map;
import java.util.function.Function;

/**
 *
 * CountyDivisionFormSetter: Form Column Setter
 *
 * @author powered by FluentMybatis
 */
public final class CountyDivisionFormSetter extends FormSetter implements ISegment<IFormApply<CountyDivisionEntity, CountyDivisionFormSetter>> {
  private CountyDivisionFormSetter(FormApply apply) {
    super.formApply = apply;
  }

  @Override
  public Class entityClass() {
    return CountyDivisionEntity.class;
  }

  public static IFormApply<CountyDivisionEntity, CountyDivisionFormSetter> by(Object object,
      Form form) {
    assertNotNull("object", object);
    Map map = PoJoHelper.toMap(object);
    Function<FormApply, FormSetter> apply = CountyDivisionFormSetter::new;
    return new FormApply<>(apply, map, form);
  }
}
