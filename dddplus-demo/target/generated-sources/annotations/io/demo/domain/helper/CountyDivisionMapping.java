package io.demo.domain.helper;

import static java.util.Optional.ofNullable;

import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldType;
import java.lang.Override;
import java.lang.String;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 * CountyDivisionMapping: Entity类字段和表结构映射
 *
 * @author powered by FluentMybatis
 */
public interface CountyDivisionMapping extends IMapping {
  CountyDivisionMapping MAPPING = new CountyDivisionMapping(){};

  /**
   * 表名称
   */
  String Table_Name = "t_county_division";

  /**
   * Entity名称
   */
  String Entity_Name = "CountyDivisionEntity";

  /**
   * 实体属性 : 数据库字段 映射
   *  id : id
   */
  FieldMapping id = new FieldMapping("id", "id");

  /**
   * 实体属性 : 数据库字段 映射
   *  gmtCreated : gmt_created
   */
  FieldMapping gmtCreated = new FieldMapping("gmtCreated", "gmt_created");

  /**
   * 实体属性 : 数据库字段 映射
   *  gmtModified : gmt_modified
   */
  FieldMapping gmtModified = new FieldMapping("gmtModified", "gmt_modified");

  /**
   * 实体属性 : 数据库字段 映射
   *  isDeleted : is_deleted
   */
  FieldMapping isDeleted = new FieldMapping("isDeleted", "is_deleted");

  /**
   * 实体属性 : 数据库字段 映射
   *  city : city
   */
  FieldMapping city = new FieldMapping("city", "city");

  /**
   * 实体属性 : 数据库字段 映射
   *  county : county
   */
  FieldMapping county = new FieldMapping("county", "county");

  /**
   * 实体属性 : 数据库字段 映射
   *  province : province
   */
  FieldMapping province = new FieldMapping("province", "province");

  /**
   * 实例属性和数据库字段对应表
   */
  Map<String, String> Property2Column = new HashMap<String, String>() {
    {
  		this.put(id.name, id.column);
  		this.put(gmtCreated.name, gmtCreated.column);
  		this.put(gmtModified.name, gmtModified.column);
  		this.put(isDeleted.name, isDeleted.column);
  		this.put(city.name, city.column);
  		this.put(county.name, county.column);
  		this.put(province.name, province.column);
    }
  };

  /**
   * 数据库字段对应的FieldMapping
   */
  Map<String, FieldMapping> Column2Mapping = new HashMap<String, FieldMapping>() {
    {
  		this.put(id.column, id);
  		this.put(gmtCreated.column, gmtCreated);
  		this.put(gmtModified.column, gmtModified);
  		this.put(isDeleted.column, isDeleted);
  		this.put(city.column, city);
  		this.put(county.column, county);
  		this.put(province.column, province);
    }
  };

  /**
   * 数据库所有字段列表
   */
  List<String> ALL_COLUMNS = Arrays.asList(
  		id.column,
  		gmtCreated.column,
  		gmtModified.column,
  		isDeleted.column,
  		city.column,
  		county.column,
  		province.column
  );

  /**
   * 数据库所有字段列表用逗号分隔
   */
  String ALL_JOIN_COLUMNS = String.join(", ", ALL_COLUMNS);

  @Override
  default String findColumnByField(String field) {
    return Property2Column.get(field);
  }

  @Override
  default Optional<FieldMapping> findField(FieldType type) {
    switch (type) {
    	case PRIMARY_ID:
    		return ofNullable(id);
    	case LOGIC_DELETED:
    		return ofNullable(isDeleted);
    	default:
    		return ofNullable(null);
    }
  }
}
