package io.demo.domain.entity;

import static io.demo.domain.helper.ReceivingAddressMapping.*;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.entity.IEntityHelper;
import cn.org.atool.fluent.mybatis.base.model.EntityToMap;
import java.lang.Boolean;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Date;
import java.util.Map;

/**
 *
 * ReceivingAddressEntityHelper: Entity帮助类
 *
 * @author powered by FluentMybatis
 */
public class ReceivingAddressEntityHelper implements IEntityHelper {
  @Override
  public Map<String, Object> toColumnMap(IEntity entity, boolean isNoN) {
    return this.toMap((ReceivingAddressEntity)entity, false, isNoN);
  }

  @Override
  public Map<String, Object> toEntityMap(IEntity entity, boolean isNoN) {
    return this.toMap((ReceivingAddressEntity)entity, true, isNoN);
  }

  public Map<String, Object> toMap(ReceivingAddressEntity entity, boolean isProperty,
      boolean isNoN) {
    return new EntityToMap(isProperty)
    	.put(id, entity.getId(), isNoN)
    	.put(gmtModified, entity.getGmtModified(), isNoN)
    	.put(isDeleted, entity.getIsDeleted(), isNoN)
    	.put(city, entity.getCity(), isNoN)
    	.put(detailAddress, entity.getDetailAddress(), isNoN)
    	.put(district, entity.getDistrict(), isNoN)
    	.put(gmtCreate, entity.getGmtCreate(), isNoN)
    	.put(province, entity.getProvince(), isNoN)
    	.put(userId, entity.getUserId(), isNoN)
    	.getMap();
  }

  @Override
  public <E extends IEntity> E toEntity(Map<String, Object> map) {
    ReceivingAddressEntity entity = new ReceivingAddressEntity();
    if (map.containsKey(id.name)) {
    	entity.setId((Long) map.get(id.name));
    }
    if (map.containsKey(gmtModified.name)) {
    	entity.setGmtModified((Date) map.get(gmtModified.name));
    }
    if (map.containsKey(isDeleted.name)) {
    	entity.setIsDeleted((Boolean) map.get(isDeleted.name));
    }
    if (map.containsKey(city.name)) {
    	entity.setCity((String) map.get(city.name));
    }
    if (map.containsKey(detailAddress.name)) {
    	entity.setDetailAddress((String) map.get(detailAddress.name));
    }
    if (map.containsKey(district.name)) {
    	entity.setDistrict((String) map.get(district.name));
    }
    if (map.containsKey(gmtCreate.name)) {
    	entity.setGmtCreate((Date) map.get(gmtCreate.name));
    }
    if (map.containsKey(province.name)) {
    	entity.setProvince((String) map.get(province.name));
    }
    if (map.containsKey(userId.name)) {
    	entity.setUserId((Long) map.get(userId.name));
    }
    return (E)entity;
  }

  @Override
  public ReceivingAddressEntity copy(IEntity iEntity) {
    ReceivingAddressEntity entity = (ReceivingAddressEntity) iEntity;
    ReceivingAddressEntity copy = new ReceivingAddressEntity();
    {
    	copy.setId(entity.getId());
    	copy.setGmtModified(entity.getGmtModified());
    	copy.setIsDeleted(entity.getIsDeleted());
    	copy.setCity(entity.getCity());
    	copy.setDetailAddress(entity.getDetailAddress());
    	copy.setDistrict(entity.getDistrict());
    	copy.setGmtCreate(entity.getGmtCreate());
    	copy.setProvince(entity.getProvince());
    	copy.setUserId(entity.getUserId());
    }
    return copy;
  }
}
