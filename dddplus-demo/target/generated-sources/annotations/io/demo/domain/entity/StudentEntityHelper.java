package io.demo.domain.entity;

import static io.demo.domain.helper.StudentMapping.*;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.entity.IEntityHelper;
import cn.org.atool.fluent.mybatis.base.model.EntityToMap;
import java.lang.Boolean;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Date;
import java.util.Map;

/**
 *
 * StudentEntityHelper: Entity帮助类
 *
 * @author powered by FluentMybatis
 */
public class StudentEntityHelper implements IEntityHelper {
  @Override
  public Map<String, Object> toColumnMap(IEntity entity, boolean isNoN) {
    return this.toMap((StudentEntity)entity, false, isNoN);
  }

  @Override
  public Map<String, Object> toEntityMap(IEntity entity, boolean isNoN) {
    return this.toMap((StudentEntity)entity, true, isNoN);
  }

  public Map<String, Object> toMap(StudentEntity entity, boolean isProperty, boolean isNoN) {
    return new EntityToMap(isProperty)
    	.put(id, entity.getId(), isNoN)
    	.put(gmtCreated, entity.getGmtCreated(), isNoN)
    	.put(gmtModified, entity.getGmtModified(), isNoN)
    	.put(isDeleted, entity.getIsDeleted(), isNoN)
    	.put(address, entity.getAddress(), isNoN)
    	.put(addressId, entity.getAddressId(), isNoN)
    	.put(age, entity.getAge(), isNoN)
    	.put(birthday, entity.getBirthday(), isNoN)
    	.put(bonusPoints, entity.getBonusPoints(), isNoN)
    	.put(genderMan, entity.getGenderMan(), isNoN)
    	.put(grade, entity.getGrade(), isNoN)
    	.put(homeCountyId, entity.getHomeCountyId(), isNoN)
    	.put(phone, entity.getPhone(), isNoN)
    	.put(status, entity.getStatus(), isNoN)
    	.put(userName, entity.getUserName(), isNoN)
    	.getMap();
  }

  @Override
  public <E extends IEntity> E toEntity(Map<String, Object> map) {
    StudentEntity entity = new StudentEntity();
    if (map.containsKey(id.name)) {
    	entity.setId((Long) map.get(id.name));
    }
    if (map.containsKey(gmtCreated.name)) {
    	entity.setGmtCreated((Date) map.get(gmtCreated.name));
    }
    if (map.containsKey(gmtModified.name)) {
    	entity.setGmtModified((Date) map.get(gmtModified.name));
    }
    if (map.containsKey(isDeleted.name)) {
    	entity.setIsDeleted((Boolean) map.get(isDeleted.name));
    }
    if (map.containsKey(address.name)) {
    	entity.setAddress((String) map.get(address.name));
    }
    if (map.containsKey(addressId.name)) {
    	entity.setAddressId((Long) map.get(addressId.name));
    }
    if (map.containsKey(age.name)) {
    	entity.setAge((Integer) map.get(age.name));
    }
    if (map.containsKey(birthday.name)) {
    	entity.setBirthday((Date) map.get(birthday.name));
    }
    if (map.containsKey(bonusPoints.name)) {
    	entity.setBonusPoints((Long) map.get(bonusPoints.name));
    }
    if (map.containsKey(genderMan.name)) {
    	entity.setGenderMan((Boolean) map.get(genderMan.name));
    }
    if (map.containsKey(grade.name)) {
    	entity.setGrade((Integer) map.get(grade.name));
    }
    if (map.containsKey(homeCountyId.name)) {
    	entity.setHomeCountyId((Long) map.get(homeCountyId.name));
    }
    if (map.containsKey(phone.name)) {
    	entity.setPhone((String) map.get(phone.name));
    }
    if (map.containsKey(status.name)) {
    	entity.setStatus((String) map.get(status.name));
    }
    if (map.containsKey(userName.name)) {
    	entity.setUserName((String) map.get(userName.name));
    }
    return (E)entity;
  }

  @Override
  public StudentEntity copy(IEntity iEntity) {
    StudentEntity entity = (StudentEntity) iEntity;
    StudentEntity copy = new StudentEntity();
    {
    	copy.setId(entity.getId());
    	copy.setGmtCreated(entity.getGmtCreated());
    	copy.setGmtModified(entity.getGmtModified());
    	copy.setIsDeleted(entity.getIsDeleted());
    	copy.setAddress(entity.getAddress());
    	copy.setAddressId(entity.getAddressId());
    	copy.setAge(entity.getAge());
    	copy.setBirthday(entity.getBirthday());
    	copy.setBonusPoints(entity.getBonusPoints());
    	copy.setGenderMan(entity.getGenderMan());
    	copy.setGrade(entity.getGrade());
    	copy.setHomeCountyId(entity.getHomeCountyId());
    	copy.setPhone(entity.getPhone());
    	copy.setStatus(entity.getStatus());
    	copy.setUserName(entity.getUserName());
    }
    return copy;
  }
}
