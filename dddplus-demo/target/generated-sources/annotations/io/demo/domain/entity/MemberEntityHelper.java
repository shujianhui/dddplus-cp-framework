package io.demo.domain.entity;

import static io.demo.domain.helper.MemberMapping.*;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.entity.IEntityHelper;
import cn.org.atool.fluent.mybatis.base.model.EntityToMap;
import java.lang.Boolean;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Date;
import java.util.Map;

/**
 *
 * MemberEntityHelper: Entity帮助类
 *
 * @author powered by FluentMybatis
 */
public class MemberEntityHelper implements IEntityHelper {
  @Override
  public Map<String, Object> toColumnMap(IEntity entity, boolean isNoN) {
    return this.toMap((MemberEntity)entity, false, isNoN);
  }

  @Override
  public Map<String, Object> toEntityMap(IEntity entity, boolean isNoN) {
    return this.toMap((MemberEntity)entity, true, isNoN);
  }

  public Map<String, Object> toMap(MemberEntity entity, boolean isProperty, boolean isNoN) {
    return new EntityToMap(isProperty)
    	.put(id, entity.getId(), isNoN)
    	.put(gmtCreated, entity.getGmtCreated(), isNoN)
    	.put(gmtModified, entity.getGmtModified(), isNoN)
    	.put(isDeleted, entity.getIsDeleted(), isNoN)
    	.put(age, entity.getAge(), isNoN)
    	.put(isGirl, entity.getIsGirl(), isNoN)
    	.put(school, entity.getSchool(), isNoN)
    	.put(userName, entity.getUserName(), isNoN)
    	.getMap();
  }

  @Override
  public <E extends IEntity> E toEntity(Map<String, Object> map) {
    MemberEntity entity = new MemberEntity();
    if (map.containsKey(id.name)) {
    	entity.setId((Long) map.get(id.name));
    }
    if (map.containsKey(gmtCreated.name)) {
    	entity.setGmtCreated((Date) map.get(gmtCreated.name));
    }
    if (map.containsKey(gmtModified.name)) {
    	entity.setGmtModified((Date) map.get(gmtModified.name));
    }
    if (map.containsKey(isDeleted.name)) {
    	entity.setIsDeleted((Boolean) map.get(isDeleted.name));
    }
    if (map.containsKey(age.name)) {
    	entity.setAge((Integer) map.get(age.name));
    }
    if (map.containsKey(isGirl.name)) {
    	entity.setIsGirl((Boolean) map.get(isGirl.name));
    }
    if (map.containsKey(school.name)) {
    	entity.setSchool((String) map.get(school.name));
    }
    if (map.containsKey(userName.name)) {
    	entity.setUserName((String) map.get(userName.name));
    }
    return (E)entity;
  }

  @Override
  public MemberEntity copy(IEntity iEntity) {
    MemberEntity entity = (MemberEntity) iEntity;
    MemberEntity copy = new MemberEntity();
    {
    	copy.setId(entity.getId());
    	copy.setGmtCreated(entity.getGmtCreated());
    	copy.setGmtModified(entity.getGmtModified());
    	copy.setIsDeleted(entity.getIsDeleted());
    	copy.setAge(entity.getAge());
    	copy.setIsGirl(entity.getIsGirl());
    	copy.setSchool(entity.getSchool());
    	copy.setUserName(entity.getUserName());
    }
    return copy;
  }
}
