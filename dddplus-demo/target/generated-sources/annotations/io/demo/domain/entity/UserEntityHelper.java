package io.demo.domain.entity;

import static io.demo.domain.helper.UserMapping.*;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.entity.IEntityHelper;
import cn.org.atool.fluent.mybatis.base.model.EntityToMap;
import java.lang.Boolean;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Date;
import java.util.Map;

/**
 *
 * UserEntityHelper: Entity帮助类
 *
 * @author powered by FluentMybatis
 */
public class UserEntityHelper implements IEntityHelper {
  @Override
  public Map<String, Object> toColumnMap(IEntity entity, boolean isNoN) {
    return this.toMap((UserEntity)entity, false, isNoN);
  }

  @Override
  public Map<String, Object> toEntityMap(IEntity entity, boolean isNoN) {
    return this.toMap((UserEntity)entity, true, isNoN);
  }

  public Map<String, Object> toMap(UserEntity entity, boolean isProperty, boolean isNoN) {
    return new EntityToMap(isProperty)
    	.put(id, entity.getId(), isNoN)
    	.put(gmtCreated, entity.getGmtCreated(), isNoN)
    	.put(gmtModified, entity.getGmtModified(), isNoN)
    	.put(isDeleted, entity.getIsDeleted(), isNoN)
    	.put(age, entity.getAge(), isNoN)
    	.put(email, entity.getEmail(), isNoN)
    	.put(name, entity.getName(), isNoN)
    	.getMap();
  }

  @Override
  public <E extends IEntity> E toEntity(Map<String, Object> map) {
    UserEntity entity = new UserEntity();
    if (map.containsKey(id.name)) {
    	entity.setId((Long) map.get(id.name));
    }
    if (map.containsKey(gmtCreated.name)) {
    	entity.setGmtCreated((Date) map.get(gmtCreated.name));
    }
    if (map.containsKey(gmtModified.name)) {
    	entity.setGmtModified((Date) map.get(gmtModified.name));
    }
    if (map.containsKey(isDeleted.name)) {
    	entity.setIsDeleted((Boolean) map.get(isDeleted.name));
    }
    if (map.containsKey(age.name)) {
    	entity.setAge((Integer) map.get(age.name));
    }
    if (map.containsKey(email.name)) {
    	entity.setEmail((String) map.get(email.name));
    }
    if (map.containsKey(name.name)) {
    	entity.setName((String) map.get(name.name));
    }
    return (E)entity;
  }

  @Override
  public UserEntity copy(IEntity iEntity) {
    UserEntity entity = (UserEntity) iEntity;
    UserEntity copy = new UserEntity();
    {
    	copy.setId(entity.getId());
    	copy.setGmtCreated(entity.getGmtCreated());
    	copy.setGmtModified(entity.getGmtModified());
    	copy.setIsDeleted(entity.getIsDeleted());
    	copy.setAge(entity.getAge());
    	copy.setEmail(entity.getEmail());
    	copy.setName(entity.getName());
    }
    return copy;
  }
}
