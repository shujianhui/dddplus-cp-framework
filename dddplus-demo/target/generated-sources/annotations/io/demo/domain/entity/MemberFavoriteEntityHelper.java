package io.demo.domain.entity;

import static io.demo.domain.helper.MemberFavoriteMapping.*;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.entity.IEntityHelper;
import cn.org.atool.fluent.mybatis.base.model.EntityToMap;
import java.lang.Boolean;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Date;
import java.util.Map;

/**
 *
 * MemberFavoriteEntityHelper: Entity帮助类
 *
 * @author powered by FluentMybatis
 */
public class MemberFavoriteEntityHelper implements IEntityHelper {
  @Override
  public Map<String, Object> toColumnMap(IEntity entity, boolean isNoN) {
    return this.toMap((MemberFavoriteEntity)entity, false, isNoN);
  }

  @Override
  public Map<String, Object> toEntityMap(IEntity entity, boolean isNoN) {
    return this.toMap((MemberFavoriteEntity)entity, true, isNoN);
  }

  public Map<String, Object> toMap(MemberFavoriteEntity entity, boolean isProperty, boolean isNoN) {
    return new EntityToMap(isProperty)
    	.put(id, entity.getId(), isNoN)
    	.put(gmtCreated, entity.getGmtCreated(), isNoN)
    	.put(gmtModified, entity.getGmtModified(), isNoN)
    	.put(isDeleted, entity.getIsDeleted(), isNoN)
    	.put(favorite, entity.getFavorite(), isNoN)
    	.put(memberId, entity.getMemberId(), isNoN)
    	.getMap();
  }

  @Override
  public <E extends IEntity> E toEntity(Map<String, Object> map) {
    MemberFavoriteEntity entity = new MemberFavoriteEntity();
    if (map.containsKey(id.name)) {
    	entity.setId((Long) map.get(id.name));
    }
    if (map.containsKey(gmtCreated.name)) {
    	entity.setGmtCreated((Date) map.get(gmtCreated.name));
    }
    if (map.containsKey(gmtModified.name)) {
    	entity.setGmtModified((Date) map.get(gmtModified.name));
    }
    if (map.containsKey(isDeleted.name)) {
    	entity.setIsDeleted((Boolean) map.get(isDeleted.name));
    }
    if (map.containsKey(favorite.name)) {
    	entity.setFavorite((String) map.get(favorite.name));
    }
    if (map.containsKey(memberId.name)) {
    	entity.setMemberId((Long) map.get(memberId.name));
    }
    return (E)entity;
  }

  @Override
  public MemberFavoriteEntity copy(IEntity iEntity) {
    MemberFavoriteEntity entity = (MemberFavoriteEntity) iEntity;
    MemberFavoriteEntity copy = new MemberFavoriteEntity();
    {
    	copy.setId(entity.getId());
    	copy.setGmtCreated(entity.getGmtCreated());
    	copy.setGmtModified(entity.getGmtModified());
    	copy.setIsDeleted(entity.getIsDeleted());
    	copy.setFavorite(entity.getFavorite());
    	copy.setMemberId(entity.getMemberId());
    }
    return copy;
  }
}
