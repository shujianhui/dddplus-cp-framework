package io.demo.domain.entity;

import static io.demo.domain.helper.CountyDivisionMapping.*;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.entity.IEntityHelper;
import cn.org.atool.fluent.mybatis.base.model.EntityToMap;
import java.lang.Boolean;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Date;
import java.util.Map;

/**
 *
 * CountyDivisionEntityHelper: Entity帮助类
 *
 * @author powered by FluentMybatis
 */
public class CountyDivisionEntityHelper implements IEntityHelper {
  @Override
  public Map<String, Object> toColumnMap(IEntity entity, boolean isNoN) {
    return this.toMap((CountyDivisionEntity)entity, false, isNoN);
  }

  @Override
  public Map<String, Object> toEntityMap(IEntity entity, boolean isNoN) {
    return this.toMap((CountyDivisionEntity)entity, true, isNoN);
  }

  public Map<String, Object> toMap(CountyDivisionEntity entity, boolean isProperty, boolean isNoN) {
    return new EntityToMap(isProperty)
    	.put(id, entity.getId(), isNoN)
    	.put(gmtCreated, entity.getGmtCreated(), isNoN)
    	.put(gmtModified, entity.getGmtModified(), isNoN)
    	.put(isDeleted, entity.getIsDeleted(), isNoN)
    	.put(city, entity.getCity(), isNoN)
    	.put(county, entity.getCounty(), isNoN)
    	.put(province, entity.getProvince(), isNoN)
    	.getMap();
  }

  @Override
  public <E extends IEntity> E toEntity(Map<String, Object> map) {
    CountyDivisionEntity entity = new CountyDivisionEntity();
    if (map.containsKey(id.name)) {
    	entity.setId((Long) map.get(id.name));
    }
    if (map.containsKey(gmtCreated.name)) {
    	entity.setGmtCreated((Date) map.get(gmtCreated.name));
    }
    if (map.containsKey(gmtModified.name)) {
    	entity.setGmtModified((Date) map.get(gmtModified.name));
    }
    if (map.containsKey(isDeleted.name)) {
    	entity.setIsDeleted((Boolean) map.get(isDeleted.name));
    }
    if (map.containsKey(city.name)) {
    	entity.setCity((String) map.get(city.name));
    }
    if (map.containsKey(county.name)) {
    	entity.setCounty((String) map.get(county.name));
    }
    if (map.containsKey(province.name)) {
    	entity.setProvince((String) map.get(province.name));
    }
    return (E)entity;
  }

  @Override
  public CountyDivisionEntity copy(IEntity iEntity) {
    CountyDivisionEntity entity = (CountyDivisionEntity) iEntity;
    CountyDivisionEntity copy = new CountyDivisionEntity();
    {
    	copy.setId(entity.getId());
    	copy.setGmtCreated(entity.getGmtCreated());
    	copy.setGmtModified(entity.getGmtModified());
    	copy.setIsDeleted(entity.getIsDeleted());
    	copy.setCity(entity.getCity());
    	copy.setCounty(entity.getCounty());
    	copy.setProvince(entity.getProvince());
    }
    return copy;
  }
}
