package io.demo.domain.entity;

import static io.demo.domain.helper.MemberLoveMapping.*;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.entity.IEntityHelper;
import cn.org.atool.fluent.mybatis.base.model.EntityToMap;
import java.lang.Boolean;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Date;
import java.util.Map;

/**
 *
 * MemberLoveEntityHelper: Entity帮助类
 *
 * @author powered by FluentMybatis
 */
public class MemberLoveEntityHelper implements IEntityHelper {
  @Override
  public Map<String, Object> toColumnMap(IEntity entity, boolean isNoN) {
    return this.toMap((MemberLoveEntity)entity, false, isNoN);
  }

  @Override
  public Map<String, Object> toEntityMap(IEntity entity, boolean isNoN) {
    return this.toMap((MemberLoveEntity)entity, true, isNoN);
  }

  public Map<String, Object> toMap(MemberLoveEntity entity, boolean isProperty, boolean isNoN) {
    return new EntityToMap(isProperty)
    	.put(id, entity.getId(), isNoN)
    	.put(gmtCreated, entity.getGmtCreated(), isNoN)
    	.put(gmtModified, entity.getGmtModified(), isNoN)
    	.put(isDeleted, entity.getIsDeleted(), isNoN)
    	.put(boyId, entity.getBoyId(), isNoN)
    	.put(girlId, entity.getGirlId(), isNoN)
    	.put(status, entity.getStatus(), isNoN)
    	.getMap();
  }

  @Override
  public <E extends IEntity> E toEntity(Map<String, Object> map) {
    MemberLoveEntity entity = new MemberLoveEntity();
    if (map.containsKey(id.name)) {
    	entity.setId((Long) map.get(id.name));
    }
    if (map.containsKey(gmtCreated.name)) {
    	entity.setGmtCreated((Date) map.get(gmtCreated.name));
    }
    if (map.containsKey(gmtModified.name)) {
    	entity.setGmtModified((Date) map.get(gmtModified.name));
    }
    if (map.containsKey(isDeleted.name)) {
    	entity.setIsDeleted((Boolean) map.get(isDeleted.name));
    }
    if (map.containsKey(boyId.name)) {
    	entity.setBoyId((Long) map.get(boyId.name));
    }
    if (map.containsKey(girlId.name)) {
    	entity.setGirlId((Long) map.get(girlId.name));
    }
    if (map.containsKey(status.name)) {
    	entity.setStatus((String) map.get(status.name));
    }
    return (E)entity;
  }

  @Override
  public MemberLoveEntity copy(IEntity iEntity) {
    MemberLoveEntity entity = (MemberLoveEntity) iEntity;
    MemberLoveEntity copy = new MemberLoveEntity();
    {
    	copy.setId(entity.getId());
    	copy.setGmtCreated(entity.getGmtCreated());
    	copy.setGmtModified(entity.getGmtModified());
    	copy.setIsDeleted(entity.getIsDeleted());
    	copy.setBoyId(entity.getBoyId());
    	copy.setGirlId(entity.getGirlId());
    	copy.setStatus(entity.getStatus());
    }
    return copy;
  }
}
