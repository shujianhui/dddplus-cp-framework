package io.demo.domain.entity;

import static io.demo.domain.helper.StudentScoreMapping.*;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.entity.IEntityHelper;
import cn.org.atool.fluent.mybatis.base.model.EntityToMap;
import java.lang.Boolean;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Date;
import java.util.Map;

/**
 *
 * StudentScoreEntityHelper: Entity帮助类
 *
 * @author powered by FluentMybatis
 */
public class StudentScoreEntityHelper implements IEntityHelper {
  @Override
  public Map<String, Object> toColumnMap(IEntity entity, boolean isNoN) {
    return this.toMap((StudentScoreEntity)entity, false, isNoN);
  }

  @Override
  public Map<String, Object> toEntityMap(IEntity entity, boolean isNoN) {
    return this.toMap((StudentScoreEntity)entity, true, isNoN);
  }

  public Map<String, Object> toMap(StudentScoreEntity entity, boolean isProperty, boolean isNoN) {
    return new EntityToMap(isProperty)
    	.put(id, entity.getId(), isNoN)
    	.put(gmtCreated, entity.getGmtCreated(), isNoN)
    	.put(gmtModified, entity.getGmtModified(), isNoN)
    	.put(isDeleted, entity.getIsDeleted(), isNoN)
    	.put(schoolTerm, entity.getSchoolTerm(), isNoN)
    	.put(score, entity.getScore(), isNoN)
    	.put(studentId, entity.getStudentId(), isNoN)
    	.put(subject, entity.getSubject(), isNoN)
    	.getMap();
  }

  @Override
  public <E extends IEntity> E toEntity(Map<String, Object> map) {
    StudentScoreEntity entity = new StudentScoreEntity();
    if (map.containsKey(id.name)) {
    	entity.setId((Long) map.get(id.name));
    }
    if (map.containsKey(gmtCreated.name)) {
    	entity.setGmtCreated((Date) map.get(gmtCreated.name));
    }
    if (map.containsKey(gmtModified.name)) {
    	entity.setGmtModified((Date) map.get(gmtModified.name));
    }
    if (map.containsKey(isDeleted.name)) {
    	entity.setIsDeleted((Boolean) map.get(isDeleted.name));
    }
    if (map.containsKey(schoolTerm.name)) {
    	entity.setSchoolTerm((Integer) map.get(schoolTerm.name));
    }
    if (map.containsKey(score.name)) {
    	entity.setScore((Integer) map.get(score.name));
    }
    if (map.containsKey(studentId.name)) {
    	entity.setStudentId((Long) map.get(studentId.name));
    }
    if (map.containsKey(subject.name)) {
    	entity.setSubject((String) map.get(subject.name));
    }
    return (E)entity;
  }

  @Override
  public StudentScoreEntity copy(IEntity iEntity) {
    StudentScoreEntity entity = (StudentScoreEntity) iEntity;
    StudentScoreEntity copy = new StudentScoreEntity();
    {
    	copy.setId(entity.getId());
    	copy.setGmtCreated(entity.getGmtCreated());
    	copy.setGmtModified(entity.getGmtModified());
    	copy.setIsDeleted(entity.getIsDeleted());
    	copy.setSchoolTerm(entity.getSchoolTerm());
    	copy.setScore(entity.getScore());
    	copy.setStudentId(entity.getStudentId());
    	copy.setSubject(entity.getSubject());
    }
    return copy;
  }
}
