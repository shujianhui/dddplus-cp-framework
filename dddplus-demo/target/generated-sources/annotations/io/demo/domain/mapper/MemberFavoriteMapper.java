package io.demo.domain.mapper;

import static cn.org.atool.fluent.mybatis.mapper.FluentConst.*;

import cn.org.atool.fluent.mybatis.base.crud.IQuery;
import cn.org.atool.fluent.mybatis.base.crud.IUpdate;
import cn.org.atool.fluent.mybatis.base.mapper.IEntityMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IRichMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IWrapperMapper;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import io.demo.domain.entity.MemberFavoriteEntity;
import io.demo.domain.helper.MemberFavoriteDefaults;
import io.demo.domain.helper.MemberFavoriteMapping;
import io.demo.domain.helper.MemberFavoriteSqlProvider;
import io.demo.domain.wrapper.MemberFavoriteQuery;
import io.demo.domain.wrapper.MemberFavoriteUpdate;
import java.io.Serializable;
import java.lang.Boolean;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Component;

/**
 *
 * MemberFavoriteMapper: Mapper接口
 *
 * @author powered by FluentMybatis
 */
@Mapper
@Component("memberFavoriteMapper")
public interface MemberFavoriteMapper extends IEntityMapper<MemberFavoriteEntity>, IRichMapper<MemberFavoriteEntity>, IWrapperMapper<MemberFavoriteEntity>, IMapper<MemberFavoriteEntity> {
  String ResultMap = "MemberFavoriteEntityResultMap";

  /**
   * {@link cn.org.atool.fluent.mybatis.base.provider.BaseSqlProvider#insert(cn.org.atool.fluent.mybatis.base.IEntity)}
   */
  @Override
  @InsertProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "insert"
  )
  @Options(
      useGeneratedKeys = true,
      keyProperty = "id",
      keyColumn = "id"
  )
  int insert(MemberFavoriteEntity entity);

  @Override
  @InsertProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "insertWithPk"
  )
  int insertWithPk(MemberFavoriteEntity entity);

  @Override
  @InsertProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "insertBatch"
  )
  @Options(
      useGeneratedKeys = true,
      keyProperty = "id",
      keyColumn = "id"
  )
  int insertBatch(@Param(Param_List) Collection<MemberFavoriteEntity> entities);

  @Override
  @InsertProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "insertBatchWithPk"
  )
  int insertBatchWithPk(@Param(Param_List) Collection<MemberFavoriteEntity> entities);

  /**
   * @see MemberFavoriteSqlProvider#insertSelect(Map)
   */
  @Override
  @InsertProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "insertSelect"
  )
  int insertSelect(@Param(Param_Fields) String[] fields, @Param(Param_EW) IQuery ew);

  /**
   * @see MemberFavoriteSqlProvider#deleteById(Serializable[])
   */
  @Override
  @DeleteProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "deleteById"
  )
  int deleteById(@Param(Param_List) Serializable... ids);

  /**
   * @see MemberFavoriteSqlProvider#logicDeleteById(Serializable[])
   */
  @Override
  @DeleteProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "logicDeleteById"
  )
  int logicDeleteById(@Param(Param_List) Serializable... ids);

  /**
   * @see MemberFavoriteSqlProvider#deleteByIds(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "deleteByIds"
  )
  int deleteByIds(@Param(Param_List) Collection<? extends Serializable> idList);

  /**
   * @see MemberFavoriteSqlProvider#logicDeleteByIds(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "logicDeleteByIds"
  )
  int logicDeleteByIds(@Param(Param_List) Collection<? extends Serializable> idList);

  /**
   * @see MemberFavoriteSqlProvider#deleteByMap(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "deleteByMap"
  )
  int deleteByMap(@Param(Param_CM) Map<String, Object> cm);

  /**
   * @see MemberFavoriteSqlProvider#logicDeleteByMap(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "logicDeleteByMap"
  )
  int logicDeleteByMap(@Param(Param_CM) Map<String, Object> cm);

  /**
   * @see MemberFavoriteSqlProvider#delete(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "delete"
  )
  int delete(@Param(Param_EW) IQuery wrapper);

  /**
   * @see MemberFavoriteSqlProvider#logicDelete(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "logicDelete"
  )
  int logicDelete(@Param(Param_EW) IQuery wrapper);

  @Override
  @UpdateProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "updateById"
  )
  int updateById(@Param(Param_ET) MemberFavoriteEntity entity);

  /**
   *  {@link MemberFavoriteSqlProvider#updateBy(Map)}
   */
  @Override
  @UpdateProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "updateBy"
  )
  int updateBy(@Param(Param_EW) IUpdate... updates);

  @Override
  @SelectProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "findById"
  )
  @Results(
      id = ResultMap,
      value = {
          @Result(column = "id", property = "id", javaType = Long.class, id = true, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "gmt_created", property = "gmtCreated", javaType = Date.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "gmt_modified", property = "gmtModified", javaType = Date.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "is_deleted", property = "isDeleted", javaType = Boolean.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "favorite", property = "favorite", javaType = String.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "member_id", property = "memberId", javaType = Long.class, jdbcType = JdbcType.UNDEFINED)
          }
  )
  MemberFavoriteEntity findById(Serializable id);

  @Override
  @SelectProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "findOne"
  )
  @ResultMap(ResultMap)
  MemberFavoriteEntity findOne(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "listByIds"
  )
  @ResultMap(ResultMap)
  List<MemberFavoriteEntity> listByIds(@Param(Param_List) Collection ids);

  @Override
  @SelectProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "listByMap"
  )
  @ResultMap(ResultMap)
  List<MemberFavoriteEntity> listByMap(@Param(Param_CM) Map<String, Object> columnMap);

  @Override
  @SelectProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "listEntity"
  )
  @ResultMap(ResultMap)
  List<MemberFavoriteEntity> listEntity(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "listMaps"
  )
  @ResultType(Map.class)
  List<Map<String, Object>> listMaps(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "listObjs"
  )
  <O> List<O> listObjs(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "count"
  )
  Integer count(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberFavoriteSqlProvider.class,
      method = "countNoLimit"
  )
  Integer countNoLimit(@Param(Param_EW) IQuery query);

  default MemberFavoriteQuery query() {
    return new MemberFavoriteQuery();
  }

  default MemberFavoriteUpdate updater() {
    return new MemberFavoriteUpdate();
  }

  default MemberFavoriteQuery defaultQuery() {
    return MemberFavoriteDefaults.INSTANCE.defaultQuery();
  }

  default MemberFavoriteUpdate defaultUpdater() {
    return MemberFavoriteDefaults.INSTANCE.defaultUpdater();
  }

  @Override
  default FieldMapping primaryField() {
    return MemberFavoriteMapping.id;
  }

  @Override
  default Class<MemberFavoriteEntity> entityClass() {
    return MemberFavoriteEntity.class;
  }
}
