package io.demo.domain.mapper;

import static cn.org.atool.fluent.mybatis.mapper.FluentConst.*;

import cn.org.atool.fluent.mybatis.base.crud.IQuery;
import cn.org.atool.fluent.mybatis.base.crud.IUpdate;
import cn.org.atool.fluent.mybatis.base.mapper.IEntityMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IRichMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IWrapperMapper;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import io.demo.domain.entity.MemberLoveEntity;
import io.demo.domain.helper.MemberLoveDefaults;
import io.demo.domain.helper.MemberLoveMapping;
import io.demo.domain.helper.MemberLoveSqlProvider;
import io.demo.domain.wrapper.MemberLoveQuery;
import io.demo.domain.wrapper.MemberLoveUpdate;
import java.io.Serializable;
import java.lang.Boolean;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Component;

/**
 *
 * MemberLoveMapper: Mapper接口
 *
 * @author powered by FluentMybatis
 */
@Mapper
@Component("memberLoveMapper")
public interface MemberLoveMapper extends IEntityMapper<MemberLoveEntity>, IRichMapper<MemberLoveEntity>, IWrapperMapper<MemberLoveEntity>, IMapper<MemberLoveEntity> {
  String ResultMap = "MemberLoveEntityResultMap";

  /**
   * {@link cn.org.atool.fluent.mybatis.base.provider.BaseSqlProvider#insert(cn.org.atool.fluent.mybatis.base.IEntity)}
   */
  @Override
  @InsertProvider(
      type = MemberLoveSqlProvider.class,
      method = "insert"
  )
  @Options(
      useGeneratedKeys = true,
      keyProperty = "id",
      keyColumn = "id"
  )
  int insert(MemberLoveEntity entity);

  @Override
  @InsertProvider(
      type = MemberLoveSqlProvider.class,
      method = "insertWithPk"
  )
  int insertWithPk(MemberLoveEntity entity);

  @Override
  @InsertProvider(
      type = MemberLoveSqlProvider.class,
      method = "insertBatch"
  )
  @Options(
      useGeneratedKeys = true,
      keyProperty = "id",
      keyColumn = "id"
  )
  int insertBatch(@Param(Param_List) Collection<MemberLoveEntity> entities);

  @Override
  @InsertProvider(
      type = MemberLoveSqlProvider.class,
      method = "insertBatchWithPk"
  )
  int insertBatchWithPk(@Param(Param_List) Collection<MemberLoveEntity> entities);

  /**
   * @see MemberLoveSqlProvider#insertSelect(Map)
   */
  @Override
  @InsertProvider(
      type = MemberLoveSqlProvider.class,
      method = "insertSelect"
  )
  int insertSelect(@Param(Param_Fields) String[] fields, @Param(Param_EW) IQuery ew);

  /**
   * @see MemberLoveSqlProvider#deleteById(Serializable[])
   */
  @Override
  @DeleteProvider(
      type = MemberLoveSqlProvider.class,
      method = "deleteById"
  )
  int deleteById(@Param(Param_List) Serializable... ids);

  /**
   * @see MemberLoveSqlProvider#logicDeleteById(Serializable[])
   */
  @Override
  @DeleteProvider(
      type = MemberLoveSqlProvider.class,
      method = "logicDeleteById"
  )
  int logicDeleteById(@Param(Param_List) Serializable... ids);

  /**
   * @see MemberLoveSqlProvider#deleteByIds(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberLoveSqlProvider.class,
      method = "deleteByIds"
  )
  int deleteByIds(@Param(Param_List) Collection<? extends Serializable> idList);

  /**
   * @see MemberLoveSqlProvider#logicDeleteByIds(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberLoveSqlProvider.class,
      method = "logicDeleteByIds"
  )
  int logicDeleteByIds(@Param(Param_List) Collection<? extends Serializable> idList);

  /**
   * @see MemberLoveSqlProvider#deleteByMap(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberLoveSqlProvider.class,
      method = "deleteByMap"
  )
  int deleteByMap(@Param(Param_CM) Map<String, Object> cm);

  /**
   * @see MemberLoveSqlProvider#logicDeleteByMap(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberLoveSqlProvider.class,
      method = "logicDeleteByMap"
  )
  int logicDeleteByMap(@Param(Param_CM) Map<String, Object> cm);

  /**
   * @see MemberLoveSqlProvider#delete(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberLoveSqlProvider.class,
      method = "delete"
  )
  int delete(@Param(Param_EW) IQuery wrapper);

  /**
   * @see MemberLoveSqlProvider#logicDelete(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberLoveSqlProvider.class,
      method = "logicDelete"
  )
  int logicDelete(@Param(Param_EW) IQuery wrapper);

  @Override
  @UpdateProvider(
      type = MemberLoveSqlProvider.class,
      method = "updateById"
  )
  int updateById(@Param(Param_ET) MemberLoveEntity entity);

  /**
   *  {@link MemberLoveSqlProvider#updateBy(Map)}
   */
  @Override
  @UpdateProvider(
      type = MemberLoveSqlProvider.class,
      method = "updateBy"
  )
  int updateBy(@Param(Param_EW) IUpdate... updates);

  @Override
  @SelectProvider(
      type = MemberLoveSqlProvider.class,
      method = "findById"
  )
  @Results(
      id = ResultMap,
      value = {
          @Result(column = "id", property = "id", javaType = Long.class, id = true, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "gmt_created", property = "gmtCreated", javaType = Date.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "gmt_modified", property = "gmtModified", javaType = Date.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "is_deleted", property = "isDeleted", javaType = Boolean.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "boy_id", property = "boyId", javaType = Long.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "girl_id", property = "girlId", javaType = Long.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "status", property = "status", javaType = String.class, jdbcType = JdbcType.UNDEFINED)
          }
  )
  MemberLoveEntity findById(Serializable id);

  @Override
  @SelectProvider(
      type = MemberLoveSqlProvider.class,
      method = "findOne"
  )
  @ResultMap(ResultMap)
  MemberLoveEntity findOne(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberLoveSqlProvider.class,
      method = "listByIds"
  )
  @ResultMap(ResultMap)
  List<MemberLoveEntity> listByIds(@Param(Param_List) Collection ids);

  @Override
  @SelectProvider(
      type = MemberLoveSqlProvider.class,
      method = "listByMap"
  )
  @ResultMap(ResultMap)
  List<MemberLoveEntity> listByMap(@Param(Param_CM) Map<String, Object> columnMap);

  @Override
  @SelectProvider(
      type = MemberLoveSqlProvider.class,
      method = "listEntity"
  )
  @ResultMap(ResultMap)
  List<MemberLoveEntity> listEntity(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberLoveSqlProvider.class,
      method = "listMaps"
  )
  @ResultType(Map.class)
  List<Map<String, Object>> listMaps(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberLoveSqlProvider.class,
      method = "listObjs"
  )
  <O> List<O> listObjs(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberLoveSqlProvider.class,
      method = "count"
  )
  Integer count(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberLoveSqlProvider.class,
      method = "countNoLimit"
  )
  Integer countNoLimit(@Param(Param_EW) IQuery query);

  default MemberLoveQuery query() {
    return new MemberLoveQuery();
  }

  default MemberLoveUpdate updater() {
    return new MemberLoveUpdate();
  }

  default MemberLoveQuery defaultQuery() {
    return MemberLoveDefaults.INSTANCE.defaultQuery();
  }

  default MemberLoveUpdate defaultUpdater() {
    return MemberLoveDefaults.INSTANCE.defaultUpdater();
  }

  @Override
  default FieldMapping primaryField() {
    return MemberLoveMapping.id;
  }

  @Override
  default Class<MemberLoveEntity> entityClass() {
    return MemberLoveEntity.class;
  }
}
