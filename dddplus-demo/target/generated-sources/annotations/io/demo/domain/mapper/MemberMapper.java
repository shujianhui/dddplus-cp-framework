package io.demo.domain.mapper;

import static cn.org.atool.fluent.mybatis.mapper.FluentConst.*;

import cn.org.atool.fluent.mybatis.base.crud.IQuery;
import cn.org.atool.fluent.mybatis.base.crud.IUpdate;
import cn.org.atool.fluent.mybatis.base.mapper.IEntityMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IRichMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IWrapperMapper;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import io.demo.domain.entity.MemberEntity;
import io.demo.domain.helper.MemberDefaults;
import io.demo.domain.helper.MemberMapping;
import io.demo.domain.helper.MemberSqlProvider;
import io.demo.domain.wrapper.MemberQuery;
import io.demo.domain.wrapper.MemberUpdate;
import java.io.Serializable;
import java.lang.Boolean;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Component;

/**
 *
 * MemberMapper: Mapper接口
 *
 * @author powered by FluentMybatis
 */
@Mapper
@Component("memberMapper")
public interface MemberMapper extends IEntityMapper<MemberEntity>, IRichMapper<MemberEntity>, IWrapperMapper<MemberEntity>, IMapper<MemberEntity> {
  String ResultMap = "MemberEntityResultMap";

  /**
   * {@link cn.org.atool.fluent.mybatis.base.provider.BaseSqlProvider#insert(cn.org.atool.fluent.mybatis.base.IEntity)}
   */
  @Override
  @InsertProvider(
      type = MemberSqlProvider.class,
      method = "insert"
  )
  @Options(
      useGeneratedKeys = true,
      keyProperty = "id",
      keyColumn = "id"
  )
  int insert(MemberEntity entity);

  @Override
  @InsertProvider(
      type = MemberSqlProvider.class,
      method = "insertWithPk"
  )
  int insertWithPk(MemberEntity entity);

  @Override
  @InsertProvider(
      type = MemberSqlProvider.class,
      method = "insertBatch"
  )
  @Options(
      useGeneratedKeys = true,
      keyProperty = "id",
      keyColumn = "id"
  )
  int insertBatch(@Param(Param_List) Collection<MemberEntity> entities);

  @Override
  @InsertProvider(
      type = MemberSqlProvider.class,
      method = "insertBatchWithPk"
  )
  int insertBatchWithPk(@Param(Param_List) Collection<MemberEntity> entities);

  /**
   * @see MemberSqlProvider#insertSelect(Map)
   */
  @Override
  @InsertProvider(
      type = MemberSqlProvider.class,
      method = "insertSelect"
  )
  int insertSelect(@Param(Param_Fields) String[] fields, @Param(Param_EW) IQuery ew);

  /**
   * @see MemberSqlProvider#deleteById(Serializable[])
   */
  @Override
  @DeleteProvider(
      type = MemberSqlProvider.class,
      method = "deleteById"
  )
  int deleteById(@Param(Param_List) Serializable... ids);

  /**
   * @see MemberSqlProvider#logicDeleteById(Serializable[])
   */
  @Override
  @DeleteProvider(
      type = MemberSqlProvider.class,
      method = "logicDeleteById"
  )
  int logicDeleteById(@Param(Param_List) Serializable... ids);

  /**
   * @see MemberSqlProvider#deleteByIds(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberSqlProvider.class,
      method = "deleteByIds"
  )
  int deleteByIds(@Param(Param_List) Collection<? extends Serializable> idList);

  /**
   * @see MemberSqlProvider#logicDeleteByIds(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberSqlProvider.class,
      method = "logicDeleteByIds"
  )
  int logicDeleteByIds(@Param(Param_List) Collection<? extends Serializable> idList);

  /**
   * @see MemberSqlProvider#deleteByMap(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberSqlProvider.class,
      method = "deleteByMap"
  )
  int deleteByMap(@Param(Param_CM) Map<String, Object> cm);

  /**
   * @see MemberSqlProvider#logicDeleteByMap(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberSqlProvider.class,
      method = "logicDeleteByMap"
  )
  int logicDeleteByMap(@Param(Param_CM) Map<String, Object> cm);

  /**
   * @see MemberSqlProvider#delete(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberSqlProvider.class,
      method = "delete"
  )
  int delete(@Param(Param_EW) IQuery wrapper);

  /**
   * @see MemberSqlProvider#logicDelete(Map)
   */
  @Override
  @DeleteProvider(
      type = MemberSqlProvider.class,
      method = "logicDelete"
  )
  int logicDelete(@Param(Param_EW) IQuery wrapper);

  @Override
  @UpdateProvider(
      type = MemberSqlProvider.class,
      method = "updateById"
  )
  int updateById(@Param(Param_ET) MemberEntity entity);

  /**
   *  {@link MemberSqlProvider#updateBy(Map)}
   */
  @Override
  @UpdateProvider(
      type = MemberSqlProvider.class,
      method = "updateBy"
  )
  int updateBy(@Param(Param_EW) IUpdate... updates);

  @Override
  @SelectProvider(
      type = MemberSqlProvider.class,
      method = "findById"
  )
  @Results(
      id = ResultMap,
      value = {
          @Result(column = "id", property = "id", javaType = Long.class, id = true, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "gmt_created", property = "gmtCreated", javaType = Date.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "gmt_modified", property = "gmtModified", javaType = Date.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "is_deleted", property = "isDeleted", javaType = Boolean.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "age", property = "age", javaType = Integer.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "is_girl", property = "isGirl", javaType = Boolean.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "school", property = "school", javaType = String.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "user_name", property = "userName", javaType = String.class, jdbcType = JdbcType.UNDEFINED)
          }
  )
  MemberEntity findById(Serializable id);

  @Override
  @SelectProvider(
      type = MemberSqlProvider.class,
      method = "findOne"
  )
  @ResultMap(ResultMap)
  MemberEntity findOne(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberSqlProvider.class,
      method = "listByIds"
  )
  @ResultMap(ResultMap)
  List<MemberEntity> listByIds(@Param(Param_List) Collection ids);

  @Override
  @SelectProvider(
      type = MemberSqlProvider.class,
      method = "listByMap"
  )
  @ResultMap(ResultMap)
  List<MemberEntity> listByMap(@Param(Param_CM) Map<String, Object> columnMap);

  @Override
  @SelectProvider(
      type = MemberSqlProvider.class,
      method = "listEntity"
  )
  @ResultMap(ResultMap)
  List<MemberEntity> listEntity(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberSqlProvider.class,
      method = "listMaps"
  )
  @ResultType(Map.class)
  List<Map<String, Object>> listMaps(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberSqlProvider.class,
      method = "listObjs"
  )
  <O> List<O> listObjs(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberSqlProvider.class,
      method = "count"
  )
  Integer count(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = MemberSqlProvider.class,
      method = "countNoLimit"
  )
  Integer countNoLimit(@Param(Param_EW) IQuery query);

  default MemberQuery query() {
    return new MemberQuery();
  }

  default MemberUpdate updater() {
    return new MemberUpdate();
  }

  default MemberQuery defaultQuery() {
    return MemberDefaults.INSTANCE.defaultQuery();
  }

  default MemberUpdate defaultUpdater() {
    return MemberDefaults.INSTANCE.defaultUpdater();
  }

  @Override
  default FieldMapping primaryField() {
    return MemberMapping.id;
  }

  @Override
  default Class<MemberEntity> entityClass() {
    return MemberEntity.class;
  }
}
