package io.demo.domain.mapper;

import static cn.org.atool.fluent.mybatis.mapper.FluentConst.*;

import cn.org.atool.fluent.mybatis.base.crud.IQuery;
import cn.org.atool.fluent.mybatis.base.crud.IUpdate;
import cn.org.atool.fluent.mybatis.base.mapper.IEntityMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IRichMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IWrapperMapper;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import io.demo.domain.entity.ReceivingAddressEntity;
import io.demo.domain.helper.ReceivingAddressDefaults;
import io.demo.domain.helper.ReceivingAddressMapping;
import io.demo.domain.helper.ReceivingAddressSqlProvider;
import io.demo.domain.wrapper.ReceivingAddressQuery;
import io.demo.domain.wrapper.ReceivingAddressUpdate;
import java.io.Serializable;
import java.lang.Boolean;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Component;

/**
 *
 * ReceivingAddressMapper: Mapper接口
 *
 * @author powered by FluentMybatis
 */
@Mapper
@Component("receivingAddressMapper")
public interface ReceivingAddressMapper extends IEntityMapper<ReceivingAddressEntity>, IRichMapper<ReceivingAddressEntity>, IWrapperMapper<ReceivingAddressEntity>, IMapper<ReceivingAddressEntity> {
  String ResultMap = "ReceivingAddressEntityResultMap";

  /**
   * {@link cn.org.atool.fluent.mybatis.base.provider.BaseSqlProvider#insert(cn.org.atool.fluent.mybatis.base.IEntity)}
   */
  @Override
  @InsertProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "insert"
  )
  @Options(
      useGeneratedKeys = true,
      keyProperty = "id",
      keyColumn = "id"
  )
  int insert(ReceivingAddressEntity entity);

  @Override
  @InsertProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "insertWithPk"
  )
  int insertWithPk(ReceivingAddressEntity entity);

  @Override
  @InsertProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "insertBatch"
  )
  @Options(
      useGeneratedKeys = true,
      keyProperty = "id",
      keyColumn = "id"
  )
  int insertBatch(@Param(Param_List) Collection<ReceivingAddressEntity> entities);

  @Override
  @InsertProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "insertBatchWithPk"
  )
  int insertBatchWithPk(@Param(Param_List) Collection<ReceivingAddressEntity> entities);

  /**
   * @see ReceivingAddressSqlProvider#insertSelect(Map)
   */
  @Override
  @InsertProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "insertSelect"
  )
  int insertSelect(@Param(Param_Fields) String[] fields, @Param(Param_EW) IQuery ew);

  /**
   * @see ReceivingAddressSqlProvider#deleteById(Serializable[])
   */
  @Override
  @DeleteProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "deleteById"
  )
  int deleteById(@Param(Param_List) Serializable... ids);

  /**
   * @see ReceivingAddressSqlProvider#logicDeleteById(Serializable[])
   */
  @Override
  @DeleteProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "logicDeleteById"
  )
  int logicDeleteById(@Param(Param_List) Serializable... ids);

  /**
   * @see ReceivingAddressSqlProvider#deleteByIds(Map)
   */
  @Override
  @DeleteProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "deleteByIds"
  )
  int deleteByIds(@Param(Param_List) Collection<? extends Serializable> idList);

  /**
   * @see ReceivingAddressSqlProvider#logicDeleteByIds(Map)
   */
  @Override
  @DeleteProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "logicDeleteByIds"
  )
  int logicDeleteByIds(@Param(Param_List) Collection<? extends Serializable> idList);

  /**
   * @see ReceivingAddressSqlProvider#deleteByMap(Map)
   */
  @Override
  @DeleteProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "deleteByMap"
  )
  int deleteByMap(@Param(Param_CM) Map<String, Object> cm);

  /**
   * @see ReceivingAddressSqlProvider#logicDeleteByMap(Map)
   */
  @Override
  @DeleteProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "logicDeleteByMap"
  )
  int logicDeleteByMap(@Param(Param_CM) Map<String, Object> cm);

  /**
   * @see ReceivingAddressSqlProvider#delete(Map)
   */
  @Override
  @DeleteProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "delete"
  )
  int delete(@Param(Param_EW) IQuery wrapper);

  /**
   * @see ReceivingAddressSqlProvider#logicDelete(Map)
   */
  @Override
  @DeleteProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "logicDelete"
  )
  int logicDelete(@Param(Param_EW) IQuery wrapper);

  @Override
  @UpdateProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "updateById"
  )
  int updateById(@Param(Param_ET) ReceivingAddressEntity entity);

  /**
   *  {@link ReceivingAddressSqlProvider#updateBy(Map)}
   */
  @Override
  @UpdateProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "updateBy"
  )
  int updateBy(@Param(Param_EW) IUpdate... updates);

  @Override
  @SelectProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "findById"
  )
  @Results(
      id = ResultMap,
      value = {
          @Result(column = "id", property = "id", javaType = Long.class, id = true, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "gmt_modified", property = "gmtModified", javaType = Date.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "is_deleted", property = "isDeleted", javaType = Boolean.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "city", property = "city", javaType = String.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "detail_address", property = "detailAddress", javaType = String.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "district", property = "district", javaType = String.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "gmt_create", property = "gmtCreate", javaType = Date.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "province", property = "province", javaType = String.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "user_id", property = "userId", javaType = Long.class, jdbcType = JdbcType.UNDEFINED)
          }
  )
  ReceivingAddressEntity findById(Serializable id);

  @Override
  @SelectProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "findOne"
  )
  @ResultMap(ResultMap)
  ReceivingAddressEntity findOne(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "listByIds"
  )
  @ResultMap(ResultMap)
  List<ReceivingAddressEntity> listByIds(@Param(Param_List) Collection ids);

  @Override
  @SelectProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "listByMap"
  )
  @ResultMap(ResultMap)
  List<ReceivingAddressEntity> listByMap(@Param(Param_CM) Map<String, Object> columnMap);

  @Override
  @SelectProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "listEntity"
  )
  @ResultMap(ResultMap)
  List<ReceivingAddressEntity> listEntity(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "listMaps"
  )
  @ResultType(Map.class)
  List<Map<String, Object>> listMaps(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "listObjs"
  )
  <O> List<O> listObjs(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "count"
  )
  Integer count(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = ReceivingAddressSqlProvider.class,
      method = "countNoLimit"
  )
  Integer countNoLimit(@Param(Param_EW) IQuery query);

  default ReceivingAddressQuery query() {
    return new ReceivingAddressQuery();
  }

  default ReceivingAddressUpdate updater() {
    return new ReceivingAddressUpdate();
  }

  default ReceivingAddressQuery defaultQuery() {
    return ReceivingAddressDefaults.INSTANCE.defaultQuery();
  }

  default ReceivingAddressUpdate defaultUpdater() {
    return ReceivingAddressDefaults.INSTANCE.defaultUpdater();
  }

  @Override
  default FieldMapping primaryField() {
    return ReceivingAddressMapping.id;
  }

  @Override
  default Class<ReceivingAddressEntity> entityClass() {
    return ReceivingAddressEntity.class;
  }
}
