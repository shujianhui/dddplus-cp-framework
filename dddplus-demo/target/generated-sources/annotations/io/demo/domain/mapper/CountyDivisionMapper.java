package io.demo.domain.mapper;

import static cn.org.atool.fluent.mybatis.mapper.FluentConst.*;

import cn.org.atool.fluent.mybatis.base.crud.IQuery;
import cn.org.atool.fluent.mybatis.base.crud.IUpdate;
import cn.org.atool.fluent.mybatis.base.mapper.IEntityMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IRichMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IWrapperMapper;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import io.demo.domain.entity.CountyDivisionEntity;
import io.demo.domain.helper.CountyDivisionDefaults;
import io.demo.domain.helper.CountyDivisionMapping;
import io.demo.domain.helper.CountyDivisionSqlProvider;
import io.demo.domain.wrapper.CountyDivisionQuery;
import io.demo.domain.wrapper.CountyDivisionUpdate;
import java.io.Serializable;
import java.lang.Boolean;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Component;

/**
 *
 * CountyDivisionMapper: Mapper接口
 *
 * @author powered by FluentMybatis
 */
@Mapper
@Component("countyDivisionMapper")
public interface CountyDivisionMapper extends IEntityMapper<CountyDivisionEntity>, IRichMapper<CountyDivisionEntity>, IWrapperMapper<CountyDivisionEntity>, IMapper<CountyDivisionEntity> {
  String ResultMap = "CountyDivisionEntityResultMap";

  /**
   * {@link cn.org.atool.fluent.mybatis.base.provider.BaseSqlProvider#insert(cn.org.atool.fluent.mybatis.base.IEntity)}
   */
  @Override
  @InsertProvider(
      type = CountyDivisionSqlProvider.class,
      method = "insert"
  )
  @Options(
      useGeneratedKeys = true,
      keyProperty = "id",
      keyColumn = "id"
  )
  int insert(CountyDivisionEntity entity);

  @Override
  @InsertProvider(
      type = CountyDivisionSqlProvider.class,
      method = "insertWithPk"
  )
  int insertWithPk(CountyDivisionEntity entity);

  @Override
  @InsertProvider(
      type = CountyDivisionSqlProvider.class,
      method = "insertBatch"
  )
  @Options(
      useGeneratedKeys = true,
      keyProperty = "id",
      keyColumn = "id"
  )
  int insertBatch(@Param(Param_List) Collection<CountyDivisionEntity> entities);

  @Override
  @InsertProvider(
      type = CountyDivisionSqlProvider.class,
      method = "insertBatchWithPk"
  )
  int insertBatchWithPk(@Param(Param_List) Collection<CountyDivisionEntity> entities);

  /**
   * @see CountyDivisionSqlProvider#insertSelect(Map)
   */
  @Override
  @InsertProvider(
      type = CountyDivisionSqlProvider.class,
      method = "insertSelect"
  )
  int insertSelect(@Param(Param_Fields) String[] fields, @Param(Param_EW) IQuery ew);

  /**
   * @see CountyDivisionSqlProvider#deleteById(Serializable[])
   */
  @Override
  @DeleteProvider(
      type = CountyDivisionSqlProvider.class,
      method = "deleteById"
  )
  int deleteById(@Param(Param_List) Serializable... ids);

  /**
   * @see CountyDivisionSqlProvider#logicDeleteById(Serializable[])
   */
  @Override
  @DeleteProvider(
      type = CountyDivisionSqlProvider.class,
      method = "logicDeleteById"
  )
  int logicDeleteById(@Param(Param_List) Serializable... ids);

  /**
   * @see CountyDivisionSqlProvider#deleteByIds(Map)
   */
  @Override
  @DeleteProvider(
      type = CountyDivisionSqlProvider.class,
      method = "deleteByIds"
  )
  int deleteByIds(@Param(Param_List) Collection<? extends Serializable> idList);

  /**
   * @see CountyDivisionSqlProvider#logicDeleteByIds(Map)
   */
  @Override
  @DeleteProvider(
      type = CountyDivisionSqlProvider.class,
      method = "logicDeleteByIds"
  )
  int logicDeleteByIds(@Param(Param_List) Collection<? extends Serializable> idList);

  /**
   * @see CountyDivisionSqlProvider#deleteByMap(Map)
   */
  @Override
  @DeleteProvider(
      type = CountyDivisionSqlProvider.class,
      method = "deleteByMap"
  )
  int deleteByMap(@Param(Param_CM) Map<String, Object> cm);

  /**
   * @see CountyDivisionSqlProvider#logicDeleteByMap(Map)
   */
  @Override
  @DeleteProvider(
      type = CountyDivisionSqlProvider.class,
      method = "logicDeleteByMap"
  )
  int logicDeleteByMap(@Param(Param_CM) Map<String, Object> cm);

  /**
   * @see CountyDivisionSqlProvider#delete(Map)
   */
  @Override
  @DeleteProvider(
      type = CountyDivisionSqlProvider.class,
      method = "delete"
  )
  int delete(@Param(Param_EW) IQuery wrapper);

  /**
   * @see CountyDivisionSqlProvider#logicDelete(Map)
   */
  @Override
  @DeleteProvider(
      type = CountyDivisionSqlProvider.class,
      method = "logicDelete"
  )
  int logicDelete(@Param(Param_EW) IQuery wrapper);

  @Override
  @UpdateProvider(
      type = CountyDivisionSqlProvider.class,
      method = "updateById"
  )
  int updateById(@Param(Param_ET) CountyDivisionEntity entity);

  /**
   *  {@link CountyDivisionSqlProvider#updateBy(Map)}
   */
  @Override
  @UpdateProvider(
      type = CountyDivisionSqlProvider.class,
      method = "updateBy"
  )
  int updateBy(@Param(Param_EW) IUpdate... updates);

  @Override
  @SelectProvider(
      type = CountyDivisionSqlProvider.class,
      method = "findById"
  )
  @Results(
      id = ResultMap,
      value = {
          @Result(column = "id", property = "id", javaType = Long.class, id = true, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "gmt_created", property = "gmtCreated", javaType = Date.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "gmt_modified", property = "gmtModified", javaType = Date.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "is_deleted", property = "isDeleted", javaType = Boolean.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "city", property = "city", javaType = String.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "county", property = "county", javaType = String.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "province", property = "province", javaType = String.class, jdbcType = JdbcType.UNDEFINED)
          }
  )
  CountyDivisionEntity findById(Serializable id);

  @Override
  @SelectProvider(
      type = CountyDivisionSqlProvider.class,
      method = "findOne"
  )
  @ResultMap(ResultMap)
  CountyDivisionEntity findOne(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = CountyDivisionSqlProvider.class,
      method = "listByIds"
  )
  @ResultMap(ResultMap)
  List<CountyDivisionEntity> listByIds(@Param(Param_List) Collection ids);

  @Override
  @SelectProvider(
      type = CountyDivisionSqlProvider.class,
      method = "listByMap"
  )
  @ResultMap(ResultMap)
  List<CountyDivisionEntity> listByMap(@Param(Param_CM) Map<String, Object> columnMap);

  @Override
  @SelectProvider(
      type = CountyDivisionSqlProvider.class,
      method = "listEntity"
  )
  @ResultMap(ResultMap)
  List<CountyDivisionEntity> listEntity(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = CountyDivisionSqlProvider.class,
      method = "listMaps"
  )
  @ResultType(Map.class)
  List<Map<String, Object>> listMaps(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = CountyDivisionSqlProvider.class,
      method = "listObjs"
  )
  <O> List<O> listObjs(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = CountyDivisionSqlProvider.class,
      method = "count"
  )
  Integer count(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = CountyDivisionSqlProvider.class,
      method = "countNoLimit"
  )
  Integer countNoLimit(@Param(Param_EW) IQuery query);

  default CountyDivisionQuery query() {
    return new CountyDivisionQuery();
  }

  default CountyDivisionUpdate updater() {
    return new CountyDivisionUpdate();
  }

  default CountyDivisionQuery defaultQuery() {
    return CountyDivisionDefaults.INSTANCE.defaultQuery();
  }

  default CountyDivisionUpdate defaultUpdater() {
    return CountyDivisionDefaults.INSTANCE.defaultUpdater();
  }

  @Override
  default FieldMapping primaryField() {
    return CountyDivisionMapping.id;
  }

  @Override
  default Class<CountyDivisionEntity> entityClass() {
    return CountyDivisionEntity.class;
  }
}
