package io.demo.domain.mapper;

import static cn.org.atool.fluent.mybatis.mapper.FluentConst.*;

import cn.org.atool.fluent.mybatis.base.crud.IQuery;
import cn.org.atool.fluent.mybatis.base.crud.IUpdate;
import cn.org.atool.fluent.mybatis.base.mapper.IEntityMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IRichMapper;
import cn.org.atool.fluent.mybatis.base.mapper.IWrapperMapper;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import io.demo.domain.entity.StudentScoreEntity;
import io.demo.domain.helper.StudentScoreDefaults;
import io.demo.domain.helper.StudentScoreMapping;
import io.demo.domain.helper.StudentScoreSqlProvider;
import io.demo.domain.wrapper.StudentScoreQuery;
import io.demo.domain.wrapper.StudentScoreUpdate;
import java.io.Serializable;
import java.lang.Boolean;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Component;

/**
 *
 * StudentScoreMapper: Mapper接口
 *
 * @author powered by FluentMybatis
 */
@Mapper
@Component("studentScoreMapper")
public interface StudentScoreMapper extends IEntityMapper<StudentScoreEntity>, IRichMapper<StudentScoreEntity>, IWrapperMapper<StudentScoreEntity>, IMapper<StudentScoreEntity> {
  String ResultMap = "StudentScoreEntityResultMap";

  /**
   * {@link cn.org.atool.fluent.mybatis.base.provider.BaseSqlProvider#insert(cn.org.atool.fluent.mybatis.base.IEntity)}
   */
  @Override
  @InsertProvider(
      type = StudentScoreSqlProvider.class,
      method = "insert"
  )
  @Options(
      useGeneratedKeys = true,
      keyProperty = "id",
      keyColumn = "id"
  )
  int insert(StudentScoreEntity entity);

  @Override
  @InsertProvider(
      type = StudentScoreSqlProvider.class,
      method = "insertWithPk"
  )
  int insertWithPk(StudentScoreEntity entity);

  @Override
  @InsertProvider(
      type = StudentScoreSqlProvider.class,
      method = "insertBatch"
  )
  @Options(
      useGeneratedKeys = true,
      keyProperty = "id",
      keyColumn = "id"
  )
  int insertBatch(@Param(Param_List) Collection<StudentScoreEntity> entities);

  @Override
  @InsertProvider(
      type = StudentScoreSqlProvider.class,
      method = "insertBatchWithPk"
  )
  int insertBatchWithPk(@Param(Param_List) Collection<StudentScoreEntity> entities);

  /**
   * @see StudentScoreSqlProvider#insertSelect(Map)
   */
  @Override
  @InsertProvider(
      type = StudentScoreSqlProvider.class,
      method = "insertSelect"
  )
  int insertSelect(@Param(Param_Fields) String[] fields, @Param(Param_EW) IQuery ew);

  /**
   * @see StudentScoreSqlProvider#deleteById(Serializable[])
   */
  @Override
  @DeleteProvider(
      type = StudentScoreSqlProvider.class,
      method = "deleteById"
  )
  int deleteById(@Param(Param_List) Serializable... ids);

  /**
   * @see StudentScoreSqlProvider#logicDeleteById(Serializable[])
   */
  @Override
  @DeleteProvider(
      type = StudentScoreSqlProvider.class,
      method = "logicDeleteById"
  )
  int logicDeleteById(@Param(Param_List) Serializable... ids);

  /**
   * @see StudentScoreSqlProvider#deleteByIds(Map)
   */
  @Override
  @DeleteProvider(
      type = StudentScoreSqlProvider.class,
      method = "deleteByIds"
  )
  int deleteByIds(@Param(Param_List) Collection<? extends Serializable> idList);

  /**
   * @see StudentScoreSqlProvider#logicDeleteByIds(Map)
   */
  @Override
  @DeleteProvider(
      type = StudentScoreSqlProvider.class,
      method = "logicDeleteByIds"
  )
  int logicDeleteByIds(@Param(Param_List) Collection<? extends Serializable> idList);

  /**
   * @see StudentScoreSqlProvider#deleteByMap(Map)
   */
  @Override
  @DeleteProvider(
      type = StudentScoreSqlProvider.class,
      method = "deleteByMap"
  )
  int deleteByMap(@Param(Param_CM) Map<String, Object> cm);

  /**
   * @see StudentScoreSqlProvider#logicDeleteByMap(Map)
   */
  @Override
  @DeleteProvider(
      type = StudentScoreSqlProvider.class,
      method = "logicDeleteByMap"
  )
  int logicDeleteByMap(@Param(Param_CM) Map<String, Object> cm);

  /**
   * @see StudentScoreSqlProvider#delete(Map)
   */
  @Override
  @DeleteProvider(
      type = StudentScoreSqlProvider.class,
      method = "delete"
  )
  int delete(@Param(Param_EW) IQuery wrapper);

  /**
   * @see StudentScoreSqlProvider#logicDelete(Map)
   */
  @Override
  @DeleteProvider(
      type = StudentScoreSqlProvider.class,
      method = "logicDelete"
  )
  int logicDelete(@Param(Param_EW) IQuery wrapper);

  @Override
  @UpdateProvider(
      type = StudentScoreSqlProvider.class,
      method = "updateById"
  )
  int updateById(@Param(Param_ET) StudentScoreEntity entity);

  /**
   *  {@link StudentScoreSqlProvider#updateBy(Map)}
   */
  @Override
  @UpdateProvider(
      type = StudentScoreSqlProvider.class,
      method = "updateBy"
  )
  int updateBy(@Param(Param_EW) IUpdate... updates);

  @Override
  @SelectProvider(
      type = StudentScoreSqlProvider.class,
      method = "findById"
  )
  @Results(
      id = ResultMap,
      value = {
          @Result(column = "id", property = "id", javaType = Long.class, id = true, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "gmt_created", property = "gmtCreated", javaType = Date.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "gmt_modified", property = "gmtModified", javaType = Date.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "is_deleted", property = "isDeleted", javaType = Boolean.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "school_term", property = "schoolTerm", javaType = Integer.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "score", property = "score", javaType = Integer.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "student_id", property = "studentId", javaType = Long.class, jdbcType = JdbcType.UNDEFINED),
          @Result(column = "subject", property = "subject", javaType = String.class, jdbcType = JdbcType.UNDEFINED)
          }
  )
  StudentScoreEntity findById(Serializable id);

  @Override
  @SelectProvider(
      type = StudentScoreSqlProvider.class,
      method = "findOne"
  )
  @ResultMap(ResultMap)
  StudentScoreEntity findOne(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = StudentScoreSqlProvider.class,
      method = "listByIds"
  )
  @ResultMap(ResultMap)
  List<StudentScoreEntity> listByIds(@Param(Param_List) Collection ids);

  @Override
  @SelectProvider(
      type = StudentScoreSqlProvider.class,
      method = "listByMap"
  )
  @ResultMap(ResultMap)
  List<StudentScoreEntity> listByMap(@Param(Param_CM) Map<String, Object> columnMap);

  @Override
  @SelectProvider(
      type = StudentScoreSqlProvider.class,
      method = "listEntity"
  )
  @ResultMap(ResultMap)
  List<StudentScoreEntity> listEntity(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = StudentScoreSqlProvider.class,
      method = "listMaps"
  )
  @ResultType(Map.class)
  List<Map<String, Object>> listMaps(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = StudentScoreSqlProvider.class,
      method = "listObjs"
  )
  <O> List<O> listObjs(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = StudentScoreSqlProvider.class,
      method = "count"
  )
  Integer count(@Param(Param_EW) IQuery query);

  @Override
  @SelectProvider(
      type = StudentScoreSqlProvider.class,
      method = "countNoLimit"
  )
  Integer countNoLimit(@Param(Param_EW) IQuery query);

  default StudentScoreQuery query() {
    return new StudentScoreQuery();
  }

  default StudentScoreUpdate updater() {
    return new StudentScoreUpdate();
  }

  default StudentScoreQuery defaultQuery() {
    return StudentScoreDefaults.INSTANCE.defaultQuery();
  }

  default StudentScoreUpdate defaultUpdater() {
    return StudentScoreDefaults.INSTANCE.defaultUpdater();
  }

  @Override
  default FieldMapping primaryField() {
    return StudentScoreMapping.id;
  }

  @Override
  default Class<StudentScoreEntity> entityClass() {
    return StudentScoreEntity.class;
  }
}
