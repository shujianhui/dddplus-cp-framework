package io.demo.domain.wrapper;

import static cn.org.atool.fluent.mybatis.If.notBlank;

import cn.org.atool.fluent.mybatis.base.crud.BaseUpdate;
import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import io.demo.domain.entity.MemberFavoriteEntity;
import io.demo.domain.helper.MemberFavoriteDefaults;
import io.demo.domain.helper.MemberFavoriteMapping;
import io.demo.domain.helper.MemberFavoriteWrapperHelper.UpdateOrderBy;
import io.demo.domain.helper.MemberFavoriteWrapperHelper.UpdateSetter;
import io.demo.domain.helper.MemberFavoriteWrapperHelper.UpdateWhere;
import java.lang.Deprecated;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 *
 * MemberFavoriteUpdate: 更新构造
 *
 * @author powered by FluentMybatis
 */
public class MemberFavoriteUpdate extends BaseUpdate<MemberFavoriteEntity, MemberFavoriteUpdate, MemberFavoriteQuery> {
  /**
   * 默认设置器
   */
  private static final MemberFavoriteDefaults defaults = MemberFavoriteDefaults.INSTANCE;

  /**
   * same as {@link #update}
   */
  public final UpdateSetter set = new UpdateSetter(this);

  /**
   * replaced by {@link #set}
   */
  @Deprecated
  public final UpdateSetter update = set;

  public final UpdateWhere where = new UpdateWhere(this);

  public final UpdateOrderBy orderBy = new UpdateOrderBy(this);

  public MemberFavoriteUpdate() {
    this(defaults.table(), null);
  }

  public MemberFavoriteUpdate(Supplier<String> table, String alias) {
    super(table, alias, MemberFavoriteEntity.class, MemberFavoriteQuery.class);
  }

  @Override
  public UpdateWhere where() {
    return this.where;
  }

  @Override
  protected IMapping mapping() {
    return MemberFavoriteMapping.MAPPING;
  }

  protected List<String> allFields() {
    return MemberFavoriteMapping.ALL_COLUMNS;
  }

  @Override
  public DbType dbType() {
    return DbType.MYSQL;
  }

  public static MemberFavoriteUpdate updater() {
    return new MemberFavoriteUpdate();
  }

  public static MemberFavoriteUpdate updater(Supplier<String> table) {
    return new MemberFavoriteUpdate(table, null);
  }

  public static MemberFavoriteUpdate defaultUpdater() {
    return defaults.defaultUpdater();
  }

  @Override
  protected Map<String, FieldMapping> column2mapping() {
    return MemberFavoriteMapping.Column2Mapping;
  }
}
