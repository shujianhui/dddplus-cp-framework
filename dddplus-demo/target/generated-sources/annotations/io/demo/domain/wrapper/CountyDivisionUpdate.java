package io.demo.domain.wrapper;

import static cn.org.atool.fluent.mybatis.If.notBlank;

import cn.org.atool.fluent.mybatis.base.crud.BaseUpdate;
import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import io.demo.domain.entity.CountyDivisionEntity;
import io.demo.domain.helper.CountyDivisionDefaults;
import io.demo.domain.helper.CountyDivisionMapping;
import io.demo.domain.helper.CountyDivisionWrapperHelper.UpdateOrderBy;
import io.demo.domain.helper.CountyDivisionWrapperHelper.UpdateSetter;
import io.demo.domain.helper.CountyDivisionWrapperHelper.UpdateWhere;
import java.lang.Deprecated;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 *
 * CountyDivisionUpdate: 更新构造
 *
 * @author powered by FluentMybatis
 */
public class CountyDivisionUpdate extends BaseUpdate<CountyDivisionEntity, CountyDivisionUpdate, CountyDivisionQuery> {
  /**
   * 默认设置器
   */
  private static final CountyDivisionDefaults defaults = CountyDivisionDefaults.INSTANCE;

  /**
   * same as {@link #update}
   */
  public final UpdateSetter set = new UpdateSetter(this);

  /**
   * replaced by {@link #set}
   */
  @Deprecated
  public final UpdateSetter update = set;

  public final UpdateWhere where = new UpdateWhere(this);

  public final UpdateOrderBy orderBy = new UpdateOrderBy(this);

  public CountyDivisionUpdate() {
    this(defaults.table(), null);
  }

  public CountyDivisionUpdate(Supplier<String> table, String alias) {
    super(table, alias, CountyDivisionEntity.class, CountyDivisionQuery.class);
  }

  @Override
  public UpdateWhere where() {
    return this.where;
  }

  @Override
  protected IMapping mapping() {
    return CountyDivisionMapping.MAPPING;
  }

  protected List<String> allFields() {
    return CountyDivisionMapping.ALL_COLUMNS;
  }

  @Override
  public DbType dbType() {
    return DbType.MYSQL;
  }

  public static CountyDivisionUpdate updater() {
    return new CountyDivisionUpdate();
  }

  public static CountyDivisionUpdate updater(Supplier<String> table) {
    return new CountyDivisionUpdate(table, null);
  }

  public static CountyDivisionUpdate defaultUpdater() {
    return defaults.defaultUpdater();
  }

  @Override
  protected Map<String, FieldMapping> column2mapping() {
    return CountyDivisionMapping.Column2Mapping;
  }
}
