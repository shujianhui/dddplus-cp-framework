package io.demo.domain.wrapper;

import static cn.org.atool.fluent.mybatis.If.notBlank;

import cn.org.atool.fluent.mybatis.base.crud.BaseUpdate;
import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import io.demo.domain.entity.MemberEntity;
import io.demo.domain.helper.MemberDefaults;
import io.demo.domain.helper.MemberMapping;
import io.demo.domain.helper.MemberWrapperHelper.UpdateOrderBy;
import io.demo.domain.helper.MemberWrapperHelper.UpdateSetter;
import io.demo.domain.helper.MemberWrapperHelper.UpdateWhere;
import java.lang.Deprecated;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 *
 * MemberUpdate: 更新构造
 *
 * @author powered by FluentMybatis
 */
public class MemberUpdate extends BaseUpdate<MemberEntity, MemberUpdate, MemberQuery> {
  /**
   * 默认设置器
   */
  private static final MemberDefaults defaults = MemberDefaults.INSTANCE;

  /**
   * same as {@link #update}
   */
  public final UpdateSetter set = new UpdateSetter(this);

  /**
   * replaced by {@link #set}
   */
  @Deprecated
  public final UpdateSetter update = set;

  public final UpdateWhere where = new UpdateWhere(this);

  public final UpdateOrderBy orderBy = new UpdateOrderBy(this);

  public MemberUpdate() {
    this(defaults.table(), null);
  }

  public MemberUpdate(Supplier<String> table, String alias) {
    super(table, alias, MemberEntity.class, MemberQuery.class);
  }

  @Override
  public UpdateWhere where() {
    return this.where;
  }

  @Override
  protected IMapping mapping() {
    return MemberMapping.MAPPING;
  }

  protected List<String> allFields() {
    return MemberMapping.ALL_COLUMNS;
  }

  @Override
  public DbType dbType() {
    return DbType.MYSQL;
  }

  public static MemberUpdate updater() {
    return new MemberUpdate();
  }

  public static MemberUpdate updater(Supplier<String> table) {
    return new MemberUpdate(table, null);
  }

  public static MemberUpdate defaultUpdater() {
    return defaults.defaultUpdater();
  }

  @Override
  protected Map<String, FieldMapping> column2mapping() {
    return MemberMapping.Column2Mapping;
  }
}
