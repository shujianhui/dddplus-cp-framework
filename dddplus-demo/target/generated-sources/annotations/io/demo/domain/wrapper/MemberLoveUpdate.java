package io.demo.domain.wrapper;

import static cn.org.atool.fluent.mybatis.If.notBlank;

import cn.org.atool.fluent.mybatis.base.crud.BaseUpdate;
import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import io.demo.domain.entity.MemberLoveEntity;
import io.demo.domain.helper.MemberLoveDefaults;
import io.demo.domain.helper.MemberLoveMapping;
import io.demo.domain.helper.MemberLoveWrapperHelper.UpdateOrderBy;
import io.demo.domain.helper.MemberLoveWrapperHelper.UpdateSetter;
import io.demo.domain.helper.MemberLoveWrapperHelper.UpdateWhere;
import java.lang.Deprecated;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 *
 * MemberLoveUpdate: 更新构造
 *
 * @author powered by FluentMybatis
 */
public class MemberLoveUpdate extends BaseUpdate<MemberLoveEntity, MemberLoveUpdate, MemberLoveQuery> {
  /**
   * 默认设置器
   */
  private static final MemberLoveDefaults defaults = MemberLoveDefaults.INSTANCE;

  /**
   * same as {@link #update}
   */
  public final UpdateSetter set = new UpdateSetter(this);

  /**
   * replaced by {@link #set}
   */
  @Deprecated
  public final UpdateSetter update = set;

  public final UpdateWhere where = new UpdateWhere(this);

  public final UpdateOrderBy orderBy = new UpdateOrderBy(this);

  public MemberLoveUpdate() {
    this(defaults.table(), null);
  }

  public MemberLoveUpdate(Supplier<String> table, String alias) {
    super(table, alias, MemberLoveEntity.class, MemberLoveQuery.class);
  }

  @Override
  public UpdateWhere where() {
    return this.where;
  }

  @Override
  protected IMapping mapping() {
    return MemberLoveMapping.MAPPING;
  }

  protected List<String> allFields() {
    return MemberLoveMapping.ALL_COLUMNS;
  }

  @Override
  public DbType dbType() {
    return DbType.MYSQL;
  }

  public static MemberLoveUpdate updater() {
    return new MemberLoveUpdate();
  }

  public static MemberLoveUpdate updater(Supplier<String> table) {
    return new MemberLoveUpdate(table, null);
  }

  public static MemberLoveUpdate defaultUpdater() {
    return defaults.defaultUpdater();
  }

  @Override
  protected Map<String, FieldMapping> column2mapping() {
    return MemberLoveMapping.Column2Mapping;
  }
}
