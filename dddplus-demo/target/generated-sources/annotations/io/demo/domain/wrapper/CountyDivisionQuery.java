package io.demo.domain.wrapper;

import static cn.org.atool.fluent.mybatis.If.notBlank;
import static cn.org.atool.fluent.mybatis.mapper.StrConstant.EMPTY;

import cn.org.atool.fluent.mybatis.base.crud.BaseQuery;
import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import cn.org.atool.fluent.mybatis.segment.model.Parameters;
import io.demo.domain.entity.CountyDivisionEntity;
import io.demo.domain.helper.CountyDivisionDefaults;
import io.demo.domain.helper.CountyDivisionMapping;
import io.demo.domain.helper.CountyDivisionWrapperHelper.GroupBy;
import io.demo.domain.helper.CountyDivisionWrapperHelper.Having;
import io.demo.domain.helper.CountyDivisionWrapperHelper.QueryOrderBy;
import io.demo.domain.helper.CountyDivisionWrapperHelper.QueryWhere;
import io.demo.domain.helper.CountyDivisionWrapperHelper.Selector;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 *
 * CountyDivisionQuery: 查询构造
 *
 * @author powered by FluentMybatis
 */
public class CountyDivisionQuery extends BaseQuery<CountyDivisionEntity, CountyDivisionQuery> {
  /**
   * 默认设置器
   */
  private static final CountyDivisionDefaults defaults = CountyDivisionDefaults.INSTANCE;

  /**
   * 指定查询字段, 默认无需设置
   */
  public final Selector select = new Selector(this);

  /**
   * 分组：GROUP BY 字段, ...
   * 例: groupBy('id', 'name')
   */
  public final GroupBy groupBy = new GroupBy(this);

  /**
   * 分组条件设置 having...
   */
  public final Having having = new Having(this);

  /**
   * 排序设置 order by ...
   */
  public final QueryOrderBy orderBy = new QueryOrderBy(this);

  /**
   * 查询条件 where ...
   */
  public final QueryWhere where = new QueryWhere(this);

  public CountyDivisionQuery() {
    this(defaults.table(), null);
  }

  public CountyDivisionQuery(String alias) {
    this(defaults.table(), alias);
  }

  public CountyDivisionQuery(Supplier<String> table, String alias) {
    super(table, alias, CountyDivisionEntity.class, CountyDivisionQuery.class);
  }

  public CountyDivisionQuery(String alias, Parameters parameters) {
    this(alias);
    this.sharedParameter(parameters);
  }

  @Override
  protected Map<String, FieldMapping> column2mapping() {
    return CountyDivisionMapping.Column2Mapping;
  }

  @Override
  public QueryWhere where() {
    return this.where;
  }

  @Override
  protected IMapping mapping() {
    return CountyDivisionMapping.MAPPING;
  }

  @Override
  public List<String> allFields() {
    return CountyDivisionMapping.ALL_COLUMNS;
  }

  @Override
  public DbType dbType() {
    return DbType.MYSQL;
  }

  public static CountyDivisionQuery query() {
    return new CountyDivisionQuery();
  }

  public static CountyDivisionQuery query(String alias) {
    return new CountyDivisionQuery(alias);
  }

  public static CountyDivisionQuery query(Supplier<String> table) {
    return new CountyDivisionQuery(table, null);
  }

  public static CountyDivisionQuery query(Supplier<String> table, String alias) {
    return new CountyDivisionQuery(table, alias);
  }

  public static CountyDivisionQuery defaultQuery() {
    return defaults.defaultQuery();
  }

  /**
   * 自动分配表别名查询构造器(join查询的时候需要定义表别名)
   * 如果要自定义别名, 使用方法 {@link #aliasQuery(String)}
   */
  public static CountyDivisionQuery aliasQuery() {
    return defaults.aliasQuery();
  }

  /**
   * 显式指定表别名(join查询的时候需要定义表别名)
   */
  public static CountyDivisionQuery aliasQuery(String alias) {
    return defaults.aliasQuery(alias);
  }

  /**
   * 关联查询, 根据fromQuery自动设置别名和关联?参数
   * 如果要自定义别名, 使用方法 {@link #aliasWith(String, BaseQuery)}
   */
  public static CountyDivisionQuery aliasWith(BaseQuery fromQuery) {
    return defaults.aliasWith(fromQuery);
  }

  /**
   * 关联查询, 显式设置别名, 根据fromQuery自动关联?参数
   */
  public static CountyDivisionQuery aliasWith(String alias, BaseQuery fromQuery) {
    return defaults.aliasWith(alias, fromQuery);
  }
}
