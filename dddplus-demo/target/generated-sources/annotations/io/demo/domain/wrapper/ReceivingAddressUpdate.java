package io.demo.domain.wrapper;

import static cn.org.atool.fluent.mybatis.If.notBlank;

import cn.org.atool.fluent.mybatis.base.crud.BaseUpdate;
import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import io.demo.domain.entity.ReceivingAddressEntity;
import io.demo.domain.helper.ReceivingAddressDefaults;
import io.demo.domain.helper.ReceivingAddressMapping;
import io.demo.domain.helper.ReceivingAddressWrapperHelper.UpdateOrderBy;
import io.demo.domain.helper.ReceivingAddressWrapperHelper.UpdateSetter;
import io.demo.domain.helper.ReceivingAddressWrapperHelper.UpdateWhere;
import java.lang.Deprecated;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 *
 * ReceivingAddressUpdate: 更新构造
 *
 * @author powered by FluentMybatis
 */
public class ReceivingAddressUpdate extends BaseUpdate<ReceivingAddressEntity, ReceivingAddressUpdate, ReceivingAddressQuery> {
  /**
   * 默认设置器
   */
  private static final ReceivingAddressDefaults defaults = ReceivingAddressDefaults.INSTANCE;

  /**
   * same as {@link #update}
   */
  public final UpdateSetter set = new UpdateSetter(this);

  /**
   * replaced by {@link #set}
   */
  @Deprecated
  public final UpdateSetter update = set;

  public final UpdateWhere where = new UpdateWhere(this);

  public final UpdateOrderBy orderBy = new UpdateOrderBy(this);

  public ReceivingAddressUpdate() {
    this(defaults.table(), null);
  }

  public ReceivingAddressUpdate(Supplier<String> table, String alias) {
    super(table, alias, ReceivingAddressEntity.class, ReceivingAddressQuery.class);
  }

  @Override
  public UpdateWhere where() {
    return this.where;
  }

  @Override
  protected IMapping mapping() {
    return ReceivingAddressMapping.MAPPING;
  }

  protected List<String> allFields() {
    return ReceivingAddressMapping.ALL_COLUMNS;
  }

  @Override
  public DbType dbType() {
    return DbType.MYSQL;
  }

  public static ReceivingAddressUpdate updater() {
    return new ReceivingAddressUpdate();
  }

  public static ReceivingAddressUpdate updater(Supplier<String> table) {
    return new ReceivingAddressUpdate(table, null);
  }

  public static ReceivingAddressUpdate defaultUpdater() {
    return defaults.defaultUpdater();
  }

  @Override
  protected Map<String, FieldMapping> column2mapping() {
    return ReceivingAddressMapping.Column2Mapping;
  }
}
