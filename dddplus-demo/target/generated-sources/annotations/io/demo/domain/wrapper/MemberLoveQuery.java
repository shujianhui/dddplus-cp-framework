package io.demo.domain.wrapper;

import static cn.org.atool.fluent.mybatis.If.notBlank;
import static cn.org.atool.fluent.mybatis.mapper.StrConstant.EMPTY;

import cn.org.atool.fluent.mybatis.base.crud.BaseQuery;
import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import cn.org.atool.fluent.mybatis.segment.model.Parameters;
import io.demo.domain.entity.MemberLoveEntity;
import io.demo.domain.helper.MemberLoveDefaults;
import io.demo.domain.helper.MemberLoveMapping;
import io.demo.domain.helper.MemberLoveWrapperHelper.GroupBy;
import io.demo.domain.helper.MemberLoveWrapperHelper.Having;
import io.demo.domain.helper.MemberLoveWrapperHelper.QueryOrderBy;
import io.demo.domain.helper.MemberLoveWrapperHelper.QueryWhere;
import io.demo.domain.helper.MemberLoveWrapperHelper.Selector;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 *
 * MemberLoveQuery: 查询构造
 *
 * @author powered by FluentMybatis
 */
public class MemberLoveQuery extends BaseQuery<MemberLoveEntity, MemberLoveQuery> {
  /**
   * 默认设置器
   */
  private static final MemberLoveDefaults defaults = MemberLoveDefaults.INSTANCE;

  /**
   * 指定查询字段, 默认无需设置
   */
  public final Selector select = new Selector(this);

  /**
   * 分组：GROUP BY 字段, ...
   * 例: groupBy('id', 'name')
   */
  public final GroupBy groupBy = new GroupBy(this);

  /**
   * 分组条件设置 having...
   */
  public final Having having = new Having(this);

  /**
   * 排序设置 order by ...
   */
  public final QueryOrderBy orderBy = new QueryOrderBy(this);

  /**
   * 查询条件 where ...
   */
  public final QueryWhere where = new QueryWhere(this);

  public MemberLoveQuery() {
    this(defaults.table(), null);
  }

  public MemberLoveQuery(String alias) {
    this(defaults.table(), alias);
  }

  public MemberLoveQuery(Supplier<String> table, String alias) {
    super(table, alias, MemberLoveEntity.class, MemberLoveQuery.class);
  }

  public MemberLoveQuery(String alias, Parameters parameters) {
    this(alias);
    this.sharedParameter(parameters);
  }

  @Override
  protected Map<String, FieldMapping> column2mapping() {
    return MemberLoveMapping.Column2Mapping;
  }

  @Override
  public QueryWhere where() {
    return this.where;
  }

  @Override
  protected IMapping mapping() {
    return MemberLoveMapping.MAPPING;
  }

  @Override
  public List<String> allFields() {
    return MemberLoveMapping.ALL_COLUMNS;
  }

  @Override
  public DbType dbType() {
    return DbType.MYSQL;
  }

  public static MemberLoveQuery query() {
    return new MemberLoveQuery();
  }

  public static MemberLoveQuery query(String alias) {
    return new MemberLoveQuery(alias);
  }

  public static MemberLoveQuery query(Supplier<String> table) {
    return new MemberLoveQuery(table, null);
  }

  public static MemberLoveQuery query(Supplier<String> table, String alias) {
    return new MemberLoveQuery(table, alias);
  }

  public static MemberLoveQuery defaultQuery() {
    return defaults.defaultQuery();
  }

  /**
   * 自动分配表别名查询构造器(join查询的时候需要定义表别名)
   * 如果要自定义别名, 使用方法 {@link #aliasQuery(String)}
   */
  public static MemberLoveQuery aliasQuery() {
    return defaults.aliasQuery();
  }

  /**
   * 显式指定表别名(join查询的时候需要定义表别名)
   */
  public static MemberLoveQuery aliasQuery(String alias) {
    return defaults.aliasQuery(alias);
  }

  /**
   * 关联查询, 根据fromQuery自动设置别名和关联?参数
   * 如果要自定义别名, 使用方法 {@link #aliasWith(String, BaseQuery)}
   */
  public static MemberLoveQuery aliasWith(BaseQuery fromQuery) {
    return defaults.aliasWith(fromQuery);
  }

  /**
   * 关联查询, 显式设置别名, 根据fromQuery自动关联?参数
   */
  public static MemberLoveQuery aliasWith(String alias, BaseQuery fromQuery) {
    return defaults.aliasWith(alias, fromQuery);
  }
}
