package io.demo.domain.wrapper;

import static cn.org.atool.fluent.mybatis.If.notBlank;

import cn.org.atool.fluent.mybatis.base.crud.BaseUpdate;
import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import cn.org.atool.fluent.mybatis.base.model.FieldMapping;
import cn.org.atool.fluent.mybatis.metadata.DbType;
import io.demo.domain.entity.StudentScoreEntity;
import io.demo.domain.helper.StudentScoreDefaults;
import io.demo.domain.helper.StudentScoreMapping;
import io.demo.domain.helper.StudentScoreWrapperHelper.UpdateOrderBy;
import io.demo.domain.helper.StudentScoreWrapperHelper.UpdateSetter;
import io.demo.domain.helper.StudentScoreWrapperHelper.UpdateWhere;
import java.lang.Deprecated;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 *
 * StudentScoreUpdate: 更新构造
 *
 * @author powered by FluentMybatis
 */
public class StudentScoreUpdate extends BaseUpdate<StudentScoreEntity, StudentScoreUpdate, StudentScoreQuery> {
  /**
   * 默认设置器
   */
  private static final StudentScoreDefaults defaults = StudentScoreDefaults.INSTANCE;

  /**
   * same as {@link #update}
   */
  public final UpdateSetter set = new UpdateSetter(this);

  /**
   * replaced by {@link #set}
   */
  @Deprecated
  public final UpdateSetter update = set;

  public final UpdateWhere where = new UpdateWhere(this);

  public final UpdateOrderBy orderBy = new UpdateOrderBy(this);

  public StudentScoreUpdate() {
    this(defaults.table(), null);
  }

  public StudentScoreUpdate(Supplier<String> table, String alias) {
    super(table, alias, StudentScoreEntity.class, StudentScoreQuery.class);
  }

  @Override
  public UpdateWhere where() {
    return this.where;
  }

  @Override
  protected IMapping mapping() {
    return StudentScoreMapping.MAPPING;
  }

  protected List<String> allFields() {
    return StudentScoreMapping.ALL_COLUMNS;
  }

  @Override
  public DbType dbType() {
    return DbType.MYSQL;
  }

  public static StudentScoreUpdate updater() {
    return new StudentScoreUpdate();
  }

  public static StudentScoreUpdate updater(Supplier<String> table) {
    return new StudentScoreUpdate(table, null);
  }

  public static StudentScoreUpdate defaultUpdater() {
    return defaults.defaultUpdater();
  }

  @Override
  protected Map<String, FieldMapping> column2mapping() {
    return StudentScoreMapping.Column2Mapping;
  }
}
