package cn.org.atool.fluent.mybatis.refs;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.notFluentMybatisException;

import cn.org.atool.fluent.mybatis.base.entity.IMapping;
import io.demo.domain.entity.CountyDivisionEntity;
import io.demo.domain.entity.MemberEntity;
import io.demo.domain.entity.MemberFavoriteEntity;
import io.demo.domain.entity.MemberLoveEntity;
import io.demo.domain.entity.ReceivingAddressEntity;
import io.demo.domain.entity.StudentEntity;
import io.demo.domain.entity.StudentScoreEntity;
import io.demo.domain.entity.UserEntity;
import io.demo.domain.helper.CountyDivisionMapping;
import io.demo.domain.helper.MemberFavoriteMapping;
import io.demo.domain.helper.MemberLoveMapping;
import io.demo.domain.helper.MemberMapping;
import io.demo.domain.helper.ReceivingAddressMapping;
import io.demo.domain.helper.StudentMapping;
import io.demo.domain.helper.StudentScoreMapping;
import io.demo.domain.helper.UserMapping;
import java.lang.Class;
import java.lang.String;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * FieldRef: Entity所有Mapping引用
 *
 * @author powered by FluentMybatis
 */
public class FieldRef {
  public static final CountyDivisionMapping CountyDivision = CountyDivisionMapping.MAPPING;

  public static final MemberMapping Member = MemberMapping.MAPPING;

  public static final MemberFavoriteMapping MemberFavorite = MemberFavoriteMapping.MAPPING;

  public static final MemberLoveMapping MemberLove = MemberLoveMapping.MAPPING;

  public static final ReceivingAddressMapping ReceivingAddress = ReceivingAddressMapping.MAPPING;

  public static final StudentMapping Student = StudentMapping.MAPPING;

  public static final StudentScoreMapping StudentScore = StudentScoreMapping.MAPPING;

  public static final UserMapping User = UserMapping.MAPPING;

  private static final Map<Class, IMapping> mappings = new HashMap<>();

  static {
    mappings.put(CountyDivisionEntity.class, CountyDivisionMapping.MAPPING);
    mappings.put(MemberEntity.class, MemberMapping.MAPPING);
    mappings.put(MemberFavoriteEntity.class, MemberFavoriteMapping.MAPPING);
    mappings.put(MemberLoveEntity.class, MemberLoveMapping.MAPPING);
    mappings.put(ReceivingAddressEntity.class, ReceivingAddressMapping.MAPPING);
    mappings.put(StudentEntity.class, StudentMapping.MAPPING);
    mappings.put(StudentScoreEntity.class, StudentScoreMapping.MAPPING);
    mappings.put(UserEntity.class, UserMapping.MAPPING);
  }

  /**
   * 返回clazz属性field对应的数据库字段名称
   */
  public static final String findColumnByField(Class clazz, String field) {
    if (mappings.containsKey(clazz)) {
    	return mappings.get(clazz).findColumnByField(field);
    }
    throw notFluentMybatisException(clazz);
  }

  /**
   * 返回clazz属性对应数据库主键字段名称
   */
  public static final String findPrimaryColumn(Class clazz) {
    if (mappings.containsKey(clazz)) {
    	return mappings.get(clazz).findPrimaryColumn();
    }
    throw notFluentMybatisException(clazz);
  }
}
