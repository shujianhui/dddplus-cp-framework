package cn.org.atool.fluent.mybatis.refs;

import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.mapper.IRichMapper;
import cn.org.atool.fluent.mybatis.spring.MapperFactory;
import io.demo.domain.entity.CountyDivisionEntity;
import io.demo.domain.entity.MemberEntity;
import io.demo.domain.entity.MemberFavoriteEntity;
import io.demo.domain.entity.MemberLoveEntity;
import io.demo.domain.entity.ReceivingAddressEntity;
import io.demo.domain.entity.StudentEntity;
import io.demo.domain.entity.StudentScoreEntity;
import io.demo.domain.entity.UserEntity;
import io.demo.domain.mapper.CountyDivisionMapper;
import io.demo.domain.mapper.MemberFavoriteMapper;
import io.demo.domain.mapper.MemberLoveMapper;
import io.demo.domain.mapper.MemberMapper;
import io.demo.domain.mapper.ReceivingAddressMapper;
import io.demo.domain.mapper.StudentMapper;
import io.demo.domain.mapper.StudentScoreMapper;
import io.demo.domain.mapper.UserMapper;
import java.lang.Class;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * MapperRef: 应用所有Mapper Bean引用
 *
 * @author powered by FluentMybatis
 */
public class MapperRef {
  private static final Map<Class<? extends IEntity>, IRichMapper> allMappers = new HashMap<>();

  private static final Set<Class<? extends IEntity>> allEntityClass = new HashSet<>();

  private static MapperRef instance;

  public final CountyDivisionMapper countyDivisionMapper;

  public final MemberMapper memberMapper;

  public final MemberFavoriteMapper memberFavoriteMapper;

  public final MemberLoveMapper memberLoveMapper;

  public final ReceivingAddressMapper receivingAddressMapper;

  public final StudentMapper studentMapper;

  public final StudentScoreMapper studentScoreMapper;

  public final UserMapper userMapper;

  private MapperRef(MapperFactory factory) {
    this.countyDivisionMapper = factory.getBean(CountyDivisionMapper.class);
    this.memberMapper = factory.getBean(MemberMapper.class);
    this.memberFavoriteMapper = factory.getBean(MemberFavoriteMapper.class);
    this.memberLoveMapper = factory.getBean(MemberLoveMapper.class);
    this.receivingAddressMapper = factory.getBean(ReceivingAddressMapper.class);
    this.studentMapper = factory.getBean(StudentMapper.class);
    this.studentScoreMapper = factory.getBean(StudentScoreMapper.class);
    this.userMapper = factory.getBean(UserMapper.class);
    allMappers.put(CountyDivisionEntity.class, this.countyDivisionMapper);
    allMappers.put(MemberEntity.class, this.memberMapper);
    allMappers.put(MemberFavoriteEntity.class, this.memberFavoriteMapper);
    allMappers.put(MemberLoveEntity.class, this.memberLoveMapper);
    allMappers.put(ReceivingAddressEntity.class, this.receivingAddressMapper);
    allMappers.put(StudentEntity.class, this.studentMapper);
    allMappers.put(StudentScoreEntity.class, this.studentScoreMapper);
    allMappers.put(UserEntity.class, this.userMapper);
    allEntityClass.addAll(allMappers.keySet());
  }

  public static final synchronized MapperRef instance(MapperFactory factory) {
    if (instance == null) {
      instance = new MapperRef(factory);
    }
    return instance;
  }

  public static final IRichMapper mapper(Class<? extends IEntity> entityClass) {
    return allMappers.get(entityClass);
  }

  public static final Set<Class<? extends IEntity>> allEntityClass() {
    return allEntityClass;
  }
}
