package cn.org.atool.fluent.mybatis.refs;

import io.demo.domain.entity.MemberEntity;
import java.util.List;

/**
 *
 * IEntityRelation: 实体类间自定义的关联关系接口
 *
 * @author powered by Test4J
 */
public interface IEntityRelation {
  /**
   * {@link MemberEntity#findExFriends}
   */
  List<MemberEntity> findExFriendsOfMemberEntity(MemberEntity entity);

  /**
   * {@link MemberEntity#findCurrFriend}
   */
  MemberEntity findCurrFriendOfMemberEntity(MemberEntity entity);
}
