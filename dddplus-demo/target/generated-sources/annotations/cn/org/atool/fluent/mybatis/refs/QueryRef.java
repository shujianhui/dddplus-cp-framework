package cn.org.atool.fluent.mybatis.refs;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.notFluentMybatisException;

import cn.org.atool.fluent.mybatis.base.crud.IDefault;
import cn.org.atool.fluent.mybatis.base.crud.IQuery;
import cn.org.atool.fluent.mybatis.base.crud.IUpdate;
import io.demo.domain.entity.CountyDivisionEntity;
import io.demo.domain.entity.MemberEntity;
import io.demo.domain.entity.MemberFavoriteEntity;
import io.demo.domain.entity.MemberLoveEntity;
import io.demo.domain.entity.ReceivingAddressEntity;
import io.demo.domain.entity.StudentEntity;
import io.demo.domain.entity.StudentScoreEntity;
import io.demo.domain.entity.UserEntity;
import io.demo.domain.helper.CountyDivisionDefaults;
import io.demo.domain.helper.MemberDefaults;
import io.demo.domain.helper.MemberFavoriteDefaults;
import io.demo.domain.helper.MemberLoveDefaults;
import io.demo.domain.helper.ReceivingAddressDefaults;
import io.demo.domain.helper.StudentDefaults;
import io.demo.domain.helper.StudentScoreDefaults;
import io.demo.domain.helper.UserDefaults;
import java.lang.Class;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * QueryRef: 构造Entity对应的default query
 * 更新器工厂类单例引用
 *
 * @author powered by FluentMybatis
 */
public class QueryRef {
  public static final CountyDivisionDefaults countyDivision = CountyDivisionDefaults.INSTANCE;

  public static final MemberDefaults member = MemberDefaults.INSTANCE;

  public static final MemberFavoriteDefaults memberFavorite = MemberFavoriteDefaults.INSTANCE;

  public static final MemberLoveDefaults memberLove = MemberLoveDefaults.INSTANCE;

  public static final ReceivingAddressDefaults receivingAddress = ReceivingAddressDefaults.INSTANCE;

  public static final StudentDefaults student = StudentDefaults.INSTANCE;

  public static final StudentScoreDefaults studentScore = StudentScoreDefaults.INSTANCE;

  public static final UserDefaults user = UserDefaults.INSTANCE;

  private static final Map<Class, IDefault> ENTITY_DEFAULTS = new HashMap<Class, IDefault>() {
  	{
  		this.put(CountyDivisionEntity.class, countyDivision);
  		this.put(MemberEntity.class, member);
  		this.put(MemberFavoriteEntity.class, memberFavorite);
  		this.put(MemberLoveEntity.class, memberLove);
  		this.put(ReceivingAddressEntity.class, receivingAddress);
  		this.put(StudentEntity.class, student);
  		this.put(StudentScoreEntity.class, studentScore);
  		this.put(UserEntity.class, user);
  	}
  };

  public static final Set<Class> All_Entity_Class = Collections.unmodifiableSet(ENTITY_DEFAULTS.keySet());

  /**
   * 返回clazz实体对应的默认Query实例
   */
  public static final IQuery defaultQuery(Class clazz) {
    	return findDefault(clazz).defaultQuery();
  }

  /**
   * 返回clazz实体对应的空Query实例
   */
  public static final IQuery emptyQuery(Class clazz) {
    	return findDefault(clazz).query();
  }

  /**
   * 返回clazz实体对应的默认Updater实例
   */
  public static final IUpdate defaultUpdater(Class clazz) {
    	return findDefault(clazz).defaultUpdater();
  }

  /**
   * 返回clazz实体对应的空Updater实例
   */
  public static final IUpdate emptyUpdater(Class clazz) {
    	return findDefault(clazz).updater();
  }

  public static final IDefault findDefault(Class clazz) {
    if (ENTITY_DEFAULTS.containsKey(clazz)) {
    	return ENTITY_DEFAULTS.get(clazz);
    }
    throw notFluentMybatisException(clazz);
  }
}
