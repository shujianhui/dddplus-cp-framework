package cn.org.atool.fluent.mybatis.refs;

import cn.org.atool.fluent.mybatis.base.IRefs;
import io.demo.domain.entity.MemberEntity;
import io.demo.domain.entity.MemberFavoriteEntity;
import io.demo.domain.entity.ReceivingAddressEntity;
import io.demo.domain.entity.StudentEntity;
import io.demo.domain.wrapper.MemberFavoriteQuery;
import io.demo.domain.wrapper.StudentQuery;
import java.lang.RuntimeException;
import java.util.List;

/**
 *
 * Refs: 
 *  o - 查询器，更新器工厂类单例引用
 *  o - 应用所有Mapper Bean引用
 *  o - Entity关联对象延迟加载查询实现
 *
 * @author powered by FluentMybatis
 */
public final class Refs extends AllRef {
  /**
   * Refs 单例
   */
  public static final Refs instance() {
    return (Refs) IRefs.instance();
  }

  /**
   * {@link MemberEntity#findMyFavorite}
   */
  public List<MemberFavoriteEntity> findMyFavoriteOfMemberEntity(MemberEntity entity) {
    return mapper().memberFavoriteMapper.listEntity(new MemberFavoriteQuery()
    	.where.isDeleted().eq(entity.getIsDeleted())
    	.and.memberId().eq(entity.getId())
    	.end());
  }

  /**
   * {@link MemberEntity#findExFriends}
   */
  public List<MemberEntity> findExFriendsOfMemberEntity(MemberEntity entity) {
    if (relation instanceof IEntityRelation) {
    	return ((IEntityRelation)relation).findExFriendsOfMemberEntity(entity);
    } else {
    	throw new RuntimeException("It must implement IEntityRelation and add the implementation to spring management.");
    }
  }

  /**
   * {@link MemberEntity#findCurrFriend}
   */
  public MemberEntity findCurrFriendOfMemberEntity(MemberEntity entity) {
    if (relation instanceof IEntityRelation) {
    	return ((IEntityRelation)relation).findCurrFriendOfMemberEntity(entity);
    } else {
    	throw new RuntimeException("It must implement IEntityRelation and add the implementation to spring management.");
    }
  }

  /**
   * {@link ReceivingAddressEntity#findMyReceivingAddress}
   */
  public List<StudentEntity> findMyReceivingAddressOfReceivingAddressEntity(
      ReceivingAddressEntity entity) {
    return mapper().studentMapper.listEntity(new StudentQuery()
    	.where.isDeleted().eq(entity.getIsDeleted())
    	.and.addressId().eq(entity.getId())
    	.end());
  }
}
