package cn.org.atool.fluent.mybatis.refs;

import static cn.org.atool.fluent.mybatis.utility.MybatisUtil.notFluentMybatisException;

import cn.org.atool.fluent.mybatis.functions.FormFunction;
import io.demo.domain.entity.CountyDivisionEntity;
import io.demo.domain.entity.MemberEntity;
import io.demo.domain.entity.MemberFavoriteEntity;
import io.demo.domain.entity.MemberLoveEntity;
import io.demo.domain.entity.ReceivingAddressEntity;
import io.demo.domain.entity.StudentEntity;
import io.demo.domain.entity.StudentScoreEntity;
import io.demo.domain.entity.UserEntity;
import io.demo.domain.helper.CountyDivisionFormSetter;
import io.demo.domain.helper.MemberFavoriteFormSetter;
import io.demo.domain.helper.MemberFormSetter;
import io.demo.domain.helper.MemberLoveFormSetter;
import io.demo.domain.helper.ReceivingAddressFormSetter;
import io.demo.domain.helper.StudentFormSetter;
import io.demo.domain.helper.StudentScoreFormSetter;
import io.demo.domain.helper.UserFormSetter;

/**
 *
 * FormRef: 所有Entity Form Setter引用
 *
 * @author powered by FluentMybatis
 */
public interface FormRef {
  FormFunction<CountyDivisionEntity, CountyDivisionFormSetter> countyDivision = (obj, form) -> CountyDivisionFormSetter.by(obj, form);

  FormFunction<MemberEntity, MemberFormSetter> member = (obj, form) -> MemberFormSetter.by(obj, form);

  FormFunction<MemberFavoriteEntity, MemberFavoriteFormSetter> memberFavorite = (obj, form) -> MemberFavoriteFormSetter.by(obj, form);

  FormFunction<MemberLoveEntity, MemberLoveFormSetter> memberLove = (obj, form) -> MemberLoveFormSetter.by(obj, form);

  FormFunction<ReceivingAddressEntity, ReceivingAddressFormSetter> receivingAddress = (obj, form) -> ReceivingAddressFormSetter.by(obj, form);

  FormFunction<StudentEntity, StudentFormSetter> student = (obj, form) -> StudentFormSetter.by(obj, form);

  FormFunction<StudentScoreEntity, StudentScoreFormSetter> studentScore = (obj, form) -> StudentScoreFormSetter.by(obj, form);

  FormFunction<UserEntity, UserFormSetter> user = (obj, form) -> UserFormSetter.by(obj, form);
}
