CREATE database if NOT EXISTS `demo` default character set utf8mb4 collate utf8mb4_unicode_ci;
use `demo`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- MariaDB dump 10.19  Distrib 10.6.3-MariaDB, for osx10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: demo
-- ------------------------------------------------------
-- Server version	10.6.3-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `receiving_address`
--

DROP TABLE IF EXISTS `receiving_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receiving_address` (
  `id` bigint(21) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` bigint(21) NOT NULL COMMENT '用户id',
  `province` varchar(50) DEFAULT NULL COMMENT '省份',
  `city` varchar(50) DEFAULT NULL COMMENT '城市',
  `district` varchar(50) DEFAULT NULL COMMENT '区',
  `detail_address` varchar(100) DEFAULT NULL COMMENT '详细住址',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(2) DEFAULT 0 COMMENT '是否逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='用户收货地址';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receiving_address`
--

LOCK TABLES `receiving_address` WRITE;
/*!40000 ALTER TABLE `receiving_address` DISABLE KEYS */;
INSERT INTO `receiving_address` VALUES (1,1,'广东','广州','白云区','中山大道112','2021-09-02 14:12:24','2021-09-02 14:12:26',0);
/*!40000 ALTER TABLE `receiving_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_county_division`
--

DROP TABLE IF EXISTS `t_county_division`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_county_division` (
  `id` bigint(21) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `province` varchar(50) DEFAULT NULL COMMENT '省份',
  `city` varchar(50) DEFAULT NULL COMMENT '城市',
  `county` varchar(50) DEFAULT NULL COMMENT '区县',
  `gmt_created` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(2) DEFAULT 0 COMMENT '是否逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='区县';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_county_division`
--

LOCK TABLES `t_county_division` WRITE;
/*!40000 ALTER TABLE `t_county_division` DISABLE KEYS */;
INSERT INTO `t_county_division` VALUES (1,'广东','广州','中国','2021-09-01 23:20:07','2021-09-01 23:20:10',0);
/*!40000 ALTER TABLE `t_county_division` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_member`
--

DROP TABLE IF EXISTS `t_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_member` (
  `id` bigint(21) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_name` varchar(45) DEFAULT NULL COMMENT '名字',
  `is_girl` tinyint(1) DEFAULT 0 COMMENT '0:男孩; 1:女孩',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `school` varchar(20) DEFAULT NULL COMMENT '学校',
  `gmt_created` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) DEFAULT 0 COMMENT '是否逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='成员表:女孩或男孩信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_member`
--

LOCK TABLES `t_member` WRITE;
/*!40000 ALTER TABLE `t_member` DISABLE KEYS */;
INSERT INTO `t_member` VALUES (1,'张三',0,12,'广州第一小学','2021-09-01 23:21:26','2021-09-01 23:21:28',0),(2,'王花',1,13,'佛山第三小学','2021-09-01 23:23:59','2021-09-01 23:24:01',0);
/*!40000 ALTER TABLE `t_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_member_favorite`
--

DROP TABLE IF EXISTS `t_member_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_member_favorite` (
  `id` bigint(21) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `member_id` bigint(21) NOT NULL COMMENT 'member表外键',
  `favorite` varchar(45) DEFAULT NULL COMMENT '爱好: 电影, 爬山, 徒步...',
  `gmt_created` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(2) DEFAULT 0 COMMENT '是否逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='成员爱好';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_member_favorite`
--

LOCK TABLES `t_member_favorite` WRITE;
/*!40000 ALTER TABLE `t_member_favorite` DISABLE KEYS */;
INSERT INTO `t_member_favorite` VALUES (1,1234,'电影, 爬山, 徒步','2021-09-01 23:22:04','2021-09-01 23:22:06',0);
/*!40000 ALTER TABLE `t_member_favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_member_love`
--

DROP TABLE IF EXISTS `t_member_love`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_member_love` (
  `id` bigint(21) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `girl_id` bigint(21) NOT NULL COMMENT 'member表外键',
  `boy_id` bigint(21) NOT NULL COMMENT 'member表外键',
  `status` varchar(45) DEFAULT NULL COMMENT '状态',
  `gmt_created` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(2) DEFAULT 0 COMMENT '是否逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='成员恋爱关系';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_member_love`
--

LOCK TABLES `t_member_love` WRITE;
/*!40000 ALTER TABLE `t_member_love` DISABLE KEYS */;
INSERT INTO `t_member_love` VALUES (1,2,1,'1','2021-09-01 23:25:06','2021-09-01 23:25:09',0);
/*!40000 ALTER TABLE `t_member_love` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_student`
--

DROP TABLE IF EXISTS `t_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_student` (
  `id` bigint(21) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `grade` int(11) DEFAULT NULL COMMENT '年级',
  `user_name` varchar(45) DEFAULT NULL COMMENT '名字',
  `gender_man` tinyint(2) DEFAULT 0 COMMENT '性别, 0:女; 1:男',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `bonus_points` bigint(21) DEFAULT 0 COMMENT '积分',
  `status` varchar(32) DEFAULT NULL COMMENT '状态(字典)',
  `home_county_id` bigint(21) DEFAULT NULL COMMENT '家庭所在区县',
  `address` varchar(200) DEFAULT NULL COMMENT '家庭详细住址',
  `address_id` bigint(21) unsigned NOT NULL COMMENT 'receiving_address表外键',
  `gmt_created` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(2) DEFAULT 0 COMMENT '是否逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='学生信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_student`
--

LOCK TABLES `t_student` WRITE;
/*!40000 ALTER TABLE `t_student` DISABLE KEYS */;
INSERT INTO `t_student` VALUES (1,12,6,'张三',1,'2009-09-01 23:15:27','13800000000',0,'1',123,'广东广州',1,'2021-09-01 23:16:34','2021-09-01 23:16:36',0);
/*!40000 ALTER TABLE `t_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_student_score`
--

DROP TABLE IF EXISTS `t_student_score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_student_score` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `student_id` bigint(20) NOT NULL COMMENT '学号',
  `school_term` int(11) DEFAULT NULL COMMENT '学期',
  `subject` varchar(30) DEFAULT NULL COMMENT '学科',
  `score` int(11) DEFAULT NULL COMMENT '成绩',
  `gmt_created` datetime NOT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '记录最后修改时间',
  `is_deleted` tinyint(2) NOT NULL DEFAULT 0 COMMENT '逻辑删除标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COMMENT='学生成绩';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_student_score`
--

LOCK TABLES `t_student_score` WRITE;
/*!40000 ALTER TABLE `t_student_score` DISABLE KEYS */;
INSERT INTO `t_student_score` VALUES (1,1,2,'语文',75,'2021-09-01 23:17:59','2021-09-01 23:18:02',0),(2,1,2,'数学',98,'2021-09-01 23:17:59','2021-09-01 23:18:02',0),(3,1,2,'英语',89,'2021-09-01 23:17:59','2021-09-01 23:18:02',0);
/*!40000 ALTER TABLE `t_student_score` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_table`
--

DROP TABLE IF EXISTS `t_user_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user_table` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(30) DEFAULT NULL COMMENT '姓名',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `gmt_created` datetime DEFAULT NULL COMMENT '记录创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '记录最后修改时间',
  `is_deleted` tinyint(2) DEFAULT 0 COMMENT '逻辑删除标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_table`
--

LOCK TABLES `t_user_table` WRITE;
/*!40000 ALTER TABLE `t_user_table` DISABLE KEYS */;
INSERT INTO `t_user_table` VALUES (1,'张三',12,'a@163.com','2021-09-01 23:20:40','2021-09-01 23:20:42',0);
/*!40000 ALTER TABLE `t_user_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-02 14:13:03

SET FOREIGN_KEY_CHECKS = 1;
