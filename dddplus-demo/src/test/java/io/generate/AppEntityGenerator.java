package io.generate;

import cn.org.atool.generator.FileGenerator;
import cn.org.atool.generator.annotation.*;
import cn.org.atool.generator.database.model.ConfigKey;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.test4j.module.database.proxy.DataSourceMariaDb4jCreator;

public class AppEntityGenerator {
    static final String url = "jdbc:mysql://localhost:3306/demo?useSSL=false&useUnicode=true&characterEncoding=utf-8";

    /**
     * 使用 test/resources/init.sql文件自动初始化测试数据库
     */
    @BeforeAll
    static void runDbScript() {
        DataSourceMariaDb4jCreator.createTest4JDataSource("dataSource");
    }

    @Test
    public void generate() {
        FileGenerator.build(Abc.class);
    }

    @Tables(
        /** 数据库连接信息 **/
        url = url, username = "root", password = "root",
        /** Entity类parent package路径 **/
        basePack = "io.demo.domain",
        /** Entity代码源目录 **/
        srcDir = "src/main/java",
        /** Dao代码源目录 **/
        daoDir = "src/main/java",
        testDir = "src/test/java",
        tablePrefix = "t_",
        entitySuffix = ConfigKey.Entity_Default_Suffix,
        mapperPrefix = ConfigKey.NOT_DEFINED,
        selectKeyBefore = 0,
        /** 如果表定义记录创建，记录修改，逻辑删除字段 **/
        gmtCreated = "gmt_created", gmtModified = "gmt_modified", logicDeleted = "is_deleted",
        /** 需要生成文件的表 ( 表名称:对应的Entity名称 ) **/
        tables = @Table(
            value = {
                "t_user_table:UserEntity",
                "t_student",
                "t_county_division",
                "t_student_score",
                "t_member", "t_member_love", "t_member_favorite",
                "receiving_address"
            },
            //superMapper = ,
            columns = @Column(value = "gender_man", javaType = Boolean.class)
        ),
        relations = {
            @Relation(method = "findMyFavorite", source = "t_member", target = "t_member_favorite", type = RelationType.OneWay_0_N
                    , where = "id=member_id && is_deleted=is_deleted"),
            @Relation(method = "findExFriends", source = "t_member", target = "t_member", type = RelationType.OneWay_0_N),
            @Relation(method = "findCurrFriend", source = "t_member", target = "t_member", type = RelationType.OneWay_0_1),
            @Relation(method = "findMyReceivingAddress", source = "receiving_address", target = "t_student", type = RelationType.OneWay_0_N
                    , where = "id=address_id && is_deleted=is_deleted")
        }
    )

    static class Abc {
    }
}