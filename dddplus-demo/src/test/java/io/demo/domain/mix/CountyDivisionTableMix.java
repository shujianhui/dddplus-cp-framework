package io.demo.domain.mix;

import io.demo.domain.dm.CountyDivisionDataMap;
import java.lang.String;
import org.test4j.hamcrest.matcher.modes.EqMode;
import org.test4j.module.spec.IMix;
import org.test4j.module.spec.annotations.Step;

/**
 * 数据库[t_county_division]表数据准备和校验通用方法
 *
 * @author Powered By Test4J
 */
public class CountyDivisionTableMix implements IMix {
  @Step("清空表[t_county_division]数据")
  public CountyDivisionTableMix cleanCountyDivisionTable() {
    db.table("t_county_division").clean();
    return this;
  }

  @Step("准备表[t_county_division]数据{1}")
  public CountyDivisionTableMix readyCountyDivisionTable(CountyDivisionDataMap data) {
    db.table("t_county_division").insert(data);
    return this;
  }

  @Step("验证表[t_county_division]有全表数据{1}")
  public CountyDivisionTableMix checkCountyDivisionTable(CountyDivisionDataMap data,
      EqMode... modes) {
    db.table("t_county_division").query().eqDataMap(data, modes);
    return this;
  }

  @Step("验证表[t_county_division]有符合条件{1}的数据{2}")
  public CountyDivisionTableMix checkCountyDivisionTable(String where, CountyDivisionDataMap data,
      EqMode... modes) {
    db.table("t_county_division").queryWhere(where).eqDataMap(data, modes);
    return this;
  }

  @Step("验证表[t_county_division]有符合条件{1}的数据{2}")
  public CountyDivisionTableMix checkCountyDivisionTable(CountyDivisionDataMap where,
      CountyDivisionDataMap data, EqMode... modes) {
    db.table("t_county_division").queryWhere(where).eqDataMap(data, modes);
    return this;
  }

  @Step("验证表[t_county_division]有{1}条符合条件{2}的数据")
  public CountyDivisionTableMix countCountyDivisionTable(int count, CountyDivisionDataMap where) {
    db.table("t_county_division").queryWhere(where).sizeEq(count);
    return this;
  }

  @Step("验证表[t_county_division]有{1}条符合条件{2}的数据")
  public CountyDivisionTableMix countCountyDivisionTable(int count, String where) {
    db.table("t_county_division").queryWhere(where).sizeEq(count);
    return this;
  }

  @Step("验证表[t_county_division]有{1}条数据")
  public CountyDivisionTableMix countCountyDivisionTable(int count) {
    db.table("t_county_division").query().sizeEq(count);
    return this;
  }
}
