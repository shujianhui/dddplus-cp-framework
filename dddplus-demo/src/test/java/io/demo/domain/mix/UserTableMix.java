package io.demo.domain.mix;

import io.demo.domain.dm.UserDataMap;
import java.lang.String;
import org.test4j.hamcrest.matcher.modes.EqMode;
import org.test4j.module.spec.IMix;
import org.test4j.module.spec.annotations.Step;

/**
 * 数据库[t_user_table]表数据准备和校验通用方法
 *
 * @author Powered By Test4J
 */
public class UserTableMix implements IMix {
  @Step("清空表[t_user_table]数据")
  public UserTableMix cleanUserTable() {
    db.table("t_user_table").clean();
    return this;
  }

  @Step("准备表[t_user_table]数据{1}")
  public UserTableMix readyUserTable(UserDataMap data) {
    db.table("t_user_table").insert(data);
    return this;
  }

  @Step("验证表[t_user_table]有全表数据{1}")
  public UserTableMix checkUserTable(UserDataMap data, EqMode... modes) {
    db.table("t_user_table").query().eqDataMap(data, modes);
    return this;
  }

  @Step("验证表[t_user_table]有符合条件{1}的数据{2}")
  public UserTableMix checkUserTable(String where, UserDataMap data, EqMode... modes) {
    db.table("t_user_table").queryWhere(where).eqDataMap(data, modes);
    return this;
  }

  @Step("验证表[t_user_table]有符合条件{1}的数据{2}")
  public UserTableMix checkUserTable(UserDataMap where, UserDataMap data, EqMode... modes) {
    db.table("t_user_table").queryWhere(where).eqDataMap(data, modes);
    return this;
  }

  @Step("验证表[t_user_table]有{1}条符合条件{2}的数据")
  public UserTableMix countUserTable(int count, UserDataMap where) {
    db.table("t_user_table").queryWhere(where).sizeEq(count);
    return this;
  }

  @Step("验证表[t_user_table]有{1}条符合条件{2}的数据")
  public UserTableMix countUserTable(int count, String where) {
    db.table("t_user_table").queryWhere(where).sizeEq(count);
    return this;
  }

  @Step("验证表[t_user_table]有{1}条数据")
  public UserTableMix countUserTable(int count) {
    db.table("t_user_table").query().sizeEq(count);
    return this;
  }
}
