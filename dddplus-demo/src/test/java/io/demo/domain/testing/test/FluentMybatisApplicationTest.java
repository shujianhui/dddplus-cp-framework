package io.demo.domain.testing.test;

import io.demo.domain.DddplusDemoApplication;
import io.demo.domain.entity.UserEntity;
import io.demo.domain.mapper.UserMapper;
import io.demo.domain.wrapper.UserQuery;
import io.demo.domain.wrapper.UserUpdate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest(classes = DddplusDemoApplication.class)
public class FluentMybatisApplicationTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void contextLoads() {
        List<UserEntity> list = userMapper.listEntity(userMapper.query());
        for (UserEntity entity : list) {
            System.out.println(entity);
        }
    }

    @Test
    public void insert() {
        // 构造一个对象
        UserEntity entity = new UserEntity();
        entity.setName("Fluent Mybatis");
        entity.setAge(1);
        entity.setEmail("darui.wu@163.com");
        entity.setIsDeleted(false);
        // 插入操作
        int count = userMapper.insert(entity);
        System.out.println("count:" + count);
        System.out.println("entity:" + entity);
    }

    @Test
    public void insertBatch() {
        List<UserEntity> entities = new ArrayList<>();
        entities.add(new UserEntity().setName("Fluent Mybatis").setEmail("darui.wu@163.com"));
        entities.add(new UserEntity().setName("Fluent Mybatis Demo").setEmail("darui.wu@163.com"));
        entities.add(new UserEntity().setName("Test4J").setEmail("darui.wu@163.com"));
        int count = userMapper.insertBatch(entities);
        System.out.println("count:" + count);
        System.out.println("entity:" + entities);
    }

    @Test
    public void deleteById() {
        int count = userMapper.deleteById(3L);
        System.out.println("count:" + count);
    }

    @Test
    public void deleteByIds() {
        int count = userMapper.deleteByIds(Arrays.asList(1L, 2L, 3L));
        System.out.println("count:" + count);
    }

    @Test
    public void delete() {
        int count = userMapper.delete(new UserQuery()
            .where.id().in(new int[]{1, 2, 3}).end()
        );
        System.out.println("count:" + count);
    }

    @Test
    public void deleteByMap() {
        int count = userMapper.deleteByMap(new HashMap<String, Object>() {
            {
                this.put("name", "Fluent Mybatis");
                this.put("email", "darui.wu@163.com");
            }
        });
        System.out.println("count:" + count);
    }

    @Test
    public void findById() {
        UserEntity entity = userMapper.findById(8L);
        System.out.println(entity);
    }

    @Test
    public void listByIds() {
        List<UserEntity> entities = userMapper.listByIds(Arrays.asList(8L, 9L));
        System.out.println(entities);
    }

    @Test
    public void findOne() {
        UserEntity entity = userMapper.findOne(new UserQuery()
            .where.id().eq(4L).end()
        );
        System.out.println(entity);
    }

    @Test
    public void findOne2() {
        UserEntity entity = userMapper.findOne(new UserQuery()
            .where.name().eq("Fluent Mybatis").end()
        );
        System.out.println(entity);
    }

    @Test
    public void listByMap() {
        List<UserEntity> entities = userMapper.listByMap(new HashMap<String, Object>() {
            {
                this.put("name", "Fluent Mybatis");
                this.put("is_deleted", false);
            }
        });
        System.out.println(entities);
    }

    @Test
    public void listEntity() {
        List<UserEntity> entities = userMapper.listEntity(new UserQuery()
            .select.name().age().email().end()
            .where.id().lt(6L)
            .and.name().like("Fluent").end()
            .orderBy.id().desc().end()
        );
        System.out.println(entities);
    }


    @Test
    public void listMaps() {
        List<Map<String, Object>> maps = userMapper.listMaps(new UserQuery()
            .select.name().age().email().end()
            .where.id().lt(6L)
            .and.name().like("Fluent").end()
            .orderBy.id().desc().end()
        );
        System.out.println(maps);
    }


    @Test
    public void listObjs() {
        List<String> ids = userMapper.listObjs(new UserQuery()
            .select.name().age().email().end()
            .where.id().lt(6L)
            .and.name().like("Fluent").end()
            .orderBy.id().desc().end()
        );
        System.out.println(ids);
    }

    @Test
    public void count() {
        int count = userMapper.count(new UserQuery()
            .where.id().lt(1000L)
            .and.name().like("Fluent").end()
            .limit(0, 10)
        );
        System.out.println(count);
    }

    @Test
    public void countNoLimit() {
        int count = userMapper.countNoLimit(new UserQuery()
            .where.id().lt(1000L)
            .and.name().like("Fluent").end()
            .limit(0, 10)
        );
        System.out.println(count);
    }

    @Test
    public void updateById() {
        int count = userMapper.updateById(new UserEntity()
            .setId(2L)
            .setName("Powerful Fluent Mybatis")
        );
        System.out.println(count);
    }


    @Test
    public void updateBy() {
        int count = userMapper.updateBy(new UserUpdate()
            .set.name().is("Powerful Fluent mybatis")
            .set.email().is("darui.wu@163.com")
            .set.age().is(1).end()
            .where.id().eq(2).end()
        );
        System.out.println(count);
    }
}