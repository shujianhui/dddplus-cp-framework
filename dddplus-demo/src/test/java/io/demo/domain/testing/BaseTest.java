package io.demo.domain.testing;

import io.demo.domain.mapper.StudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.test4j.junit5.Test4J;

@ContextConfiguration(classes = {TestAutoConfig.class})
public abstract class BaseTest extends Test4J {

    @Autowired
    protected StudentMapper mapper;
}