package io.demo.domain.testing.where;

import cn.org.atool.fluent.mybatis.base.crud.IUpdate;
import io.demo.domain.ATM;
import io.demo.domain.testing.BaseTest;
import io.demo.domain.entity.StudentEntity;
import io.demo.domain.wrapper.StudentUpdate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.hamcrest.matcher.string.StringMode;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class UpdateTest extends BaseTest {

    @Test
    public void update() {
        StudentUpdate update = new StudentUpdate()
            .set.phone().is("13800000000").end()
            .where.userName().eq("darui.wu")
            .and.genderMan().eq(true).end();
        mapper.updateBy(update);

        db.sqlList().wantFirstSql().end(
            "UPDATE `t_student` " +
                "SET `gmt_modified` = now(), " +
                "`phone` = ? " +
                "WHERE `user_name` = ? AND `gender_man` = ?",
            StringMode.SameAsSpace);
    }

    @DisplayName("批量更新同一张表")
    @Test
    public void testUpdateBatch_same() {
        IUpdate[] updates = this.newListUpdater().toArray(new IUpdate[0]);
        mapper.updateBy(updates);
        /** 验证SQL语句 **/
        db.sqlList().wantFirstSql().eq(
                        "UPDATE `t_student` SET `gmt_modified` = now(), `user_name` like ? WHERE `id` = ?; " +
                        "UPDATE `t_student` SET `gmt_modified` = now(), `user_name` like ? WHERE `id` = ?"
                , StringMode.SameAsSpace);
        /** 验证SQL参数 **/
        db.table(ATM.table.student).query().eqDataMap(ATM.dataMap.student.table(2)
                .id.values(1L, 2L)
                .userName.values("张二", "王五")
        );
    }

    /**
     * 构造多个更新操作
     */
    private List<IUpdate> newListUpdater() {
        StudentUpdate update1 = new StudentUpdate()
                .set.userName().is("张二").end()
                .where.id().eq(1L).end();
        StudentUpdate update2 = new StudentUpdate()
                .set.userName().is("王五").end()
                .where.id().eq(2L).end();
        return Arrays.asList(update1, update2);
    }

    @Test
    public void test_fluentMybatisBatch() throws Exception {
        final String CaseWhen = "case id " +
                "when 1 then ? " +
                "when 2 then ? " +
                "else ? end";
        StudentUpdate update = new StudentUpdate()
                .set.address().applyFunc(CaseWhen, "address 1", "address 2", "address 3")
                .set.age().applyFunc(CaseWhen, 23, 24, 25)
                .end()
                .where.id().in(new int[]{1, 2, 3}).end();
        mapper.updateBy(update);
        /** 验证执行的SQL语句 **/
        db.sqlList().wantFirstSql()
                .eq("UPDATE `t_student` SET `gmt_modified` = now(), `address` = case id when 1 then ? when 2 then ? else ? end, `age` = case id when 1 then ? when 2 then ? else ? end WHERE `id` IN (?, ?, ?)",
                        StringMode.SameAsSpace);
    }

    @Test
    public void test_fluentMybatisBatch2() throws Exception {
        List<StudentEntity> students = Arrays.asList(
                new StudentEntity().setId(1L).setAddress("address 1").setAge(23),
                new StudentEntity().setId(2L).setAddress("address 2").setAge(24),
                new StudentEntity().setId(3L).setAddress("address 3").setAge(25));
        final String CaseWhen = "case id " +
                "when 1 then ? " +
                "when 2 then ? " +
                "else ? end";
        StudentUpdate update = new StudentUpdate()
                .update.address().applyFunc(CaseWhen, getFields(students, StudentEntity::getAddress))
                .set.age().applyFunc(CaseWhen, getFields(students, StudentEntity::getAge))
                .end()
                .where.id().in(getFields(students, StudentEntity::getId)).end();
        mapper.updateBy(update);
        // 验证SQL语句
        db.sqlList().wantFirstSql()
                .eq("UPDATE `t_student` SET `gmt_modified` = now(), `address` = case id when 1 then ? when 2 then ? else ? end, `age` = case id when 1 then ? when 2 then ? else ? end WHERE `id` IN (?, ?, ?)",
                        StringMode.SameAsSpace);
        // 验证参数
        db.sqlList().wantFirstPara()
                .eqReflect(new Object[]{"address 1", "address 2", "address 3", 23, 24, 25, 1L, 2L, 3L});
    }

    private Object[] getFields(List<StudentEntity> students, Function<StudentEntity, Object> getField) {
        return students.stream().map(getField).toArray(Object[]::new);
    }
}