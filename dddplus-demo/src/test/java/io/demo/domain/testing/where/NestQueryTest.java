package io.demo.domain.testing.where;

import io.demo.domain.testing.BaseTest;
import io.demo.domain.wrapper.ReceivingAddressQuery;
import io.demo.domain.wrapper.StudentQuery;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.hamcrest.matcher.string.StringMode;

import java.util.List;

/**
 * fluent mybatis嵌套查询示例
 */
public class NestQueryTest extends BaseTest {

    @DisplayName("嵌套查询和主查询的表是同一个")
    @Test
    void test_in_same_table_query() {
        StudentQuery query = new StudentQuery()
            .where.id().in(q -> q.selectId()
                .where.id().eq(1L).end())
            .and.userName().like("张")
            .and.age().gt(12).end();

        List list = mapper.listEntity(query);
        db.sqlList().wantFirstSql()
            .end("FROM `t_student` WHERE `id` IN " +
                "   (SELECT `id` FROM `t_student` WHERE `id` = ?) " +
                "AND `user_name` LIKE ? " +
                "AND `age` > ?", StringMode.SameAsSpace);
    }

    @DisplayName("嵌套查询和主查询的表是不同")
    @Test
    void test_in_difference_table_query() {
        StudentQuery query = new StudentQuery()
            .selectId()
            .where.addressId().in(new ReceivingAddressQuery().selectId()
                .where.id().in(new int[]{1, 2}).end())
            .end();
        mapper.listEntity(query);
        db.sqlList().wantFirstSql()
            .eq("SELECT `id` " +
                "FROM `t_student` " +
                "WHERE `address_id` IN " +
                "   (SELECT `id` FROM `receiving_address` WHERE `id` IN (?, ?))", StringMode.SameAsSpace);
    }

    @DisplayName("EXISTS查询")
    @Test
    void test_exists_query() {
        StudentQuery query = new StudentQuery()
            .where.exists(new ReceivingAddressQuery()
                .where.detailAddress().like("广东广州")
                .and.id().apply(" = t_student.address_id").end())
            .end();
        mapper.listEntity(query);
        db.sqlList().wantFirstSql()
            .eq("SELECT `id`, `gmt_created`, `gmt_modified`, `is_deleted`, `address`, `address_id`, `age`, `birthday`, `bonus_points`, `gender_man`, `grade`, `home_county_id`, `phone`, `status`, `user_name` " +
                    "FROM `t_student` " +
                    "WHERE EXISTS (SELECT *" +
                    "   FROM `receiving_address`" +
                    "   WHERE `detail_address` LIKE ?" +
                    "   AND `id` = t_student.address_id)",
                StringMode.SameAsSpace);
    }
}