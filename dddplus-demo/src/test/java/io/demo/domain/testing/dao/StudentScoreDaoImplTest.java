package io.demo.domain.testing.dao;

import io.demo.domain.DddplusDemoApplication;
import io.demo.domain.dao.intf.StudentScoreDao;
import io.demo.domain.dao.model.ScoreStatistics;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(classes = DddplusDemoApplication.class)
public class StudentScoreDaoImplTest {
    @Autowired
    private StudentScoreDao dao;

    @Test
    public void statistics() {
        List<ScoreStatistics> list = dao.statistics(2000, 2019, new String[]{"语文", "数学", "英语"});
        System.out.println(list);
    }
}