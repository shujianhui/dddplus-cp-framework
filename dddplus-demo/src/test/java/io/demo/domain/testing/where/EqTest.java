package io.demo.domain.testing.where;

import cn.org.atool.fluent.mybatis.If;
import io.demo.domain.testing.BaseTest;
import io.demo.domain.wrapper.StudentQuery;
import org.junit.jupiter.api.Test;
import org.test4j.hamcrest.matcher.string.StringMode;

public class EqTest extends BaseTest {

    @Test
    public void column_eq_value() {
        StudentQuery query = new StudentQuery()
            .where.userName().eq("张三")
            .and.genderMan().eq(true).end();
        mapper.findOne(query);

        db.sqlList().wantFirstSql().end(
            "FROM `t_student` " +
                "WHERE `user_name` = ? " +
                "AND `gender_man` = ?",
            StringMode.SameAsSpace);
    }

    @Test
    public void condition_eq() {
        StudentQuery query = new StudentQuery()
            .where.userName().eq("张三")
            .and.genderMan().eq(true).end();
        mapper.findOne(query);
    }

    @Test
    public void eq_null() {
        want.exception(
            () -> new StudentQuery()
                .where.age().eq(null),
            RuntimeException.class
        );
    }

    @Test
    public void eq_condition_true() {
        StudentQuery query = new StudentQuery()
            .where.age().eq(12)
            .end();
        mapper.count(query);
        db.sqlList().wantFirstSql().eq("SELECT COUNT(*) FROM `t_student` WHERE `age` = ?", StringMode.SameAsSpace);
        db.sqlList().wantFirstPara().eqReflect(new Object[]{12});
    }

    @Test
    public void eq_condition_false() {
        StudentQuery query = new StudentQuery()
            .where.age().eq(12, o -> false)
            .end();
        mapper.count(query);
        db.sqlList().wantFirstSql().eq("SELECT COUNT(*) FROM `t_student`");
        db.sqlList().wantFirstPara().sizeEq(0);
    }

    @Test
    public void eq_IfNotNull() {
        StudentQuery query = new StudentQuery()
            .where.userName().eq("name", If::notNull)
            .end();
        mapper.count(query);
        db.sqlList().wantFirstSql().eq("SELECT COUNT(*) FROM `t_student` WHERE `user_name` = ?", StringMode.SameAsSpace);
        db.sqlList().wantFirstPara().eqReflect(new Object[]{"name"});
    }

    @Test
    public void eq_IfNull() {
        StudentQuery query = new StudentQuery()
            .where.userName().eq(null, If::notNull)
            .end();
        mapper.count(query);
        db.sqlList().wantFirstSql().eq("SELECT COUNT(*) FROM `t_student`");
        db.sqlList().wantFirstPara().sizeEq(0);
    }
}