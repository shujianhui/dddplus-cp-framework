package io.demo.domain;

import io.demo.domain.dm.CountyDivisionDataMap;
import io.demo.domain.dm.MemberDataMap;
import io.demo.domain.dm.MemberFavoriteDataMap;
import io.demo.domain.dm.MemberLoveDataMap;
import io.demo.domain.dm.ReceivingAddressDataMap;
import io.demo.domain.dm.StudentDataMap;
import io.demo.domain.dm.StudentScoreDataMap;
import io.demo.domain.dm.UserDataMap;
import io.demo.domain.mix.CountyDivisionTableMix;
import io.demo.domain.mix.MemberFavoriteTableMix;
import io.demo.domain.mix.MemberLoveTableMix;
import io.demo.domain.mix.MemberTableMix;
import io.demo.domain.mix.ReceivingAddressTableMix;
import io.demo.domain.mix.StudentScoreTableMix;
import io.demo.domain.mix.StudentTableMix;
import io.demo.domain.mix.UserTableMix;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import org.test4j.module.database.IDataSourceScript;
import org.test4j.module.spec.internal.MixProxy;

/**
 * ATM: Application Table Manager
 *
 * @author Powered By Test4J
 */
public interface ATM {
  DataMap dataMap = new DataMap();

  Table table = new Table();

  Mixes mixes = new Mixes();

  /**
   * 应用表名
   */
  class Table {
    public final String user = "t_user_table";

    public final String student = "t_student";

    public final String memberFavorite = "t_member_favorite";

    public final String receivingAddress = "receiving_address";

    public final String countyDivision = "t_county_division";

    public final String memberLove = "t_member_love";

    public final String studentScore = "t_student_score";

    public final String member = "t_member";
  }

  /**
   * table or entity data构造器
   */
  class DataMap {
    public final UserDataMap.Factory user = new UserDataMap.Factory();

    public final StudentDataMap.Factory student = new StudentDataMap.Factory();

    public final MemberFavoriteDataMap.Factory memberFavorite = new MemberFavoriteDataMap.Factory();

    public final ReceivingAddressDataMap.Factory receivingAddress = new ReceivingAddressDataMap.Factory();

    public final CountyDivisionDataMap.Factory countyDivision = new CountyDivisionDataMap.Factory();

    public final MemberLoveDataMap.Factory memberLove = new MemberLoveDataMap.Factory();

    public final StudentScoreDataMap.Factory studentScore = new StudentScoreDataMap.Factory();

    public final MemberDataMap.Factory member = new MemberDataMap.Factory();
  }

  /**
   * 应用表数据操作
   */
  class Mixes {
    public final UserTableMix userTableMix = MixProxy.proxy(UserTableMix.class);

    public final StudentTableMix studentTableMix = MixProxy.proxy(StudentTableMix.class);

    public final MemberFavoriteTableMix memberFavoriteTableMix = MixProxy.proxy(MemberFavoriteTableMix.class);

    public final ReceivingAddressTableMix receivingAddressTableMix = MixProxy.proxy(ReceivingAddressTableMix.class);

    public final CountyDivisionTableMix countyDivisionTableMix = MixProxy.proxy(CountyDivisionTableMix.class);

    public final MemberLoveTableMix memberLoveTableMix = MixProxy.proxy(MemberLoveTableMix.class);

    public final StudentScoreTableMix studentScoreTableMix = MixProxy.proxy(StudentScoreTableMix.class);

    public final MemberTableMix memberTableMix = MixProxy.proxy(MemberTableMix.class);

    public void cleanAllTable() {
      this.userTableMix.cleanUserTable();
      this.studentTableMix.cleanStudentTable();
      this.memberFavoriteTableMix.cleanMemberFavoriteTable();
      this.receivingAddressTableMix.cleanReceivingAddressTable();
      this.countyDivisionTableMix.cleanCountyDivisionTable();
      this.memberLoveTableMix.cleanMemberLoveTable();
      this.studentScoreTableMix.cleanStudentScoreTable();
      this.memberTableMix.cleanMemberTable();
    }
  }

  /**
   * 应用数据库创建脚本构造
   */
  class Script implements IDataSourceScript {
    @Override
    public List<Class> getTableKlass() {
      return list(
      	UserDataMap.class,
      	StudentDataMap.class,
      	MemberFavoriteDataMap.class,
      	ReceivingAddressDataMap.class,
      	CountyDivisionDataMap.class,
      	MemberLoveDataMap.class,
      	StudentScoreDataMap.class,
      	MemberDataMap.class
      );
    }

    @Override
    public IndexList getIndexList() {
      return new IndexList();
    }
  }
}
