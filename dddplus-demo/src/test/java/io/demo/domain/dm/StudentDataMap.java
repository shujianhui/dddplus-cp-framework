package io.demo.domain.dm;

import java.lang.Boolean;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Supplier;
import org.test4j.hamcrest.matcher.modes.EqMode;
import org.test4j.module.database.IDatabase;
import org.test4j.module.database.annotations.ColumnDef;
import org.test4j.module.database.annotations.ScriptTable;
import org.test4j.tools.datagen.DataMap;
import org.test4j.tools.datagen.IDataMap;
import org.test4j.tools.datagen.KeyValue;

/**
 * StudentDataMap: 表(实体)数据对比(插入)构造器
 *
 * @author Powered By Test4J
 */
@ScriptTable("t_student")
@SuppressWarnings({"unused", "rawtypes"})
public class StudentDataMap extends DataMap<StudentDataMap> {
  private boolean isTable;

  private final Supplier<Boolean> supplier = () -> this.isTable;

  @ColumnDef(
      value = "id",
      type = "BIGINT UNSIGNED",
      primary = true,
      autoIncrease = true,
      notNull = true
  )
  public final transient KeyValue<StudentDataMap> id = new KeyValue<>(this, "id", "id", supplier);

  @ColumnDef(
      value = "gmt_created",
      type = "DATETIME"
  )
  public final transient KeyValue<StudentDataMap> gmtCreated = new KeyValue<>(this, "gmt_created", "gmtCreated", supplier);

  @ColumnDef(
      value = "gmt_modified",
      type = "DATETIME"
  )
  public final transient KeyValue<StudentDataMap> gmtModified = new KeyValue<>(this, "gmt_modified", "gmtModified", supplier);

  @ColumnDef(
      value = "is_deleted",
      type = "TINYINT",
      defaultValue = "0"
  )
  public final transient KeyValue<StudentDataMap> isDeleted = new KeyValue<>(this, "is_deleted", "isDeleted", supplier);

  @ColumnDef(
      value = "address",
      type = "VARCHAR(200)"
  )
  public final transient KeyValue<StudentDataMap> address = new KeyValue<>(this, "address", "address", supplier);

  @ColumnDef(
      value = "address_id",
      type = "BIGINT UNSIGNED",
      notNull = true
  )
  public final transient KeyValue<StudentDataMap> addressId = new KeyValue<>(this, "address_id", "addressId", supplier);

  @ColumnDef(
      value = "age",
      type = "INT"
  )
  public final transient KeyValue<StudentDataMap> age = new KeyValue<>(this, "age", "age", supplier);

  @ColumnDef(
      value = "birthday",
      type = "DATETIME"
  )
  public final transient KeyValue<StudentDataMap> birthday = new KeyValue<>(this, "birthday", "birthday", supplier);

  @ColumnDef(
      value = "bonus_points",
      type = "BIGINT",
      defaultValue = "0"
  )
  public final transient KeyValue<StudentDataMap> bonusPoints = new KeyValue<>(this, "bonus_points", "bonusPoints", supplier);

  @ColumnDef(
      value = "gender_man",
      type = "TINYINT",
      defaultValue = "0"
  )
  public final transient KeyValue<StudentDataMap> genderMan = new KeyValue<>(this, "gender_man", "genderMan", supplier);

  @ColumnDef(
      value = "grade",
      type = "INT"
  )
  public final transient KeyValue<StudentDataMap> grade = new KeyValue<>(this, "grade", "grade", supplier);

  @ColumnDef(
      value = "home_county_id",
      type = "BIGINT"
  )
  public final transient KeyValue<StudentDataMap> homeCountyId = new KeyValue<>(this, "home_county_id", "homeCountyId", supplier);

  @ColumnDef(
      value = "phone",
      type = "VARCHAR(20)"
  )
  public final transient KeyValue<StudentDataMap> phone = new KeyValue<>(this, "phone", "phone", supplier);

  @ColumnDef(
      value = "status",
      type = "VARCHAR(32)"
  )
  public final transient KeyValue<StudentDataMap> status = new KeyValue<>(this, "status", "status", supplier);

  @ColumnDef(
      value = "user_name",
      type = "VARCHAR(45)"
  )
  public final transient KeyValue<StudentDataMap> userName = new KeyValue<>(this, "user_name", "userName", supplier);

  StudentDataMap(boolean isTable) {
    super();
    this.isTable = isTable;
  }

  StudentDataMap(boolean isTable, int size) {
    super(size);
    this.isTable = isTable;
  }

  /**
   * 创建StudentDataMap
   * 初始化主键和gmtCreate, gmtModified, isDeleted等特殊值
   */
  public StudentDataMap init() {
    this.id.autoIncrease();
    this.gmtCreated.values(new Date());
    this.gmtModified.values(new Date());
    this.isDeleted.values(false);
    return this;
  }

  public StudentDataMap with(Consumer<StudentDataMap> init) {
    init.accept(this);
    return this;
  }

  public static StudentDataMap table() {
    return new StudentDataMap(true, 1);
  }

  public static StudentDataMap table(int size) {
    return new StudentDataMap(true, size);
  }

  public static StudentDataMap entity() {
    return new StudentDataMap(false, 1);
  }

  public static StudentDataMap entity(int size) {
    return new StudentDataMap(false, size);
  }

  /**
   * DataMap数据和表[t_student]数据比较
   */
  public StudentDataMap eqTable(EqMode... modes) {
    IDatabase.db.table("t_student").query().eqDataMap(this, modes);
    return this;
  }

  /**
   * DataMap数据和表[t_student]数据比较
   */
  public StudentDataMap eqQuery(String query, EqMode... modes) {
    IDatabase.db.table("t_student").queryWhere(query).eqDataMap(this, modes);
    return this;
  }

  /**
   * DataMap数据和表[t_student]数据比较
   */
  public StudentDataMap eqQuery(IDataMap query, EqMode... modes) {
    IDatabase.db.table("t_student").queryWhere(query).eqDataMap(this, modes);
    return this;
  }

  /**
   * 清空[t_student]表数据
   */
  public StudentDataMap clean() {
    IDatabase.db.cleanTable("t_student");
    return this;
  }

  /**
   * 插入[t_student]表数据
   */
  public StudentDataMap insert() {
    IDatabase.db.table("t_student").insert(this);
    return this;
  }

  /**
   * 先清空, 再插入[t_student]表数据
   */
  public StudentDataMap cleanAndInsert() {
    return this.clean().insert();
  }

  public static class Factory {
    public StudentDataMap table() {
      return StudentDataMap.table();
    }

    public StudentDataMap table(int size) {
      return  StudentDataMap.table(size);
    }

    public StudentDataMap initTable() {
      return StudentDataMap.table().init();
    }

    public StudentDataMap initTable(int size) {
      return  StudentDataMap.table(size).init();
    }

    public StudentDataMap entity() {
      return StudentDataMap.entity();
    }

    public StudentDataMap entity(int size) {
      return  StudentDataMap.entity(size);
    }
  }
}
