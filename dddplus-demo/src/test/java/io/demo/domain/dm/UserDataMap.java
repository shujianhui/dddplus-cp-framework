package io.demo.domain.dm;

import java.lang.Boolean;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Supplier;
import org.test4j.hamcrest.matcher.modes.EqMode;
import org.test4j.module.database.IDatabase;
import org.test4j.module.database.annotations.ColumnDef;
import org.test4j.module.database.annotations.ScriptTable;
import org.test4j.tools.datagen.DataMap;
import org.test4j.tools.datagen.IDataMap;
import org.test4j.tools.datagen.KeyValue;

/**
 * UserDataMap: 表(实体)数据对比(插入)构造器
 *
 * @author Powered By Test4J
 */
@ScriptTable("t_user_table")
@SuppressWarnings({"unused", "rawtypes"})
public class UserDataMap extends DataMap<UserDataMap> {
  private boolean isTable;

  private final Supplier<Boolean> supplier = () -> this.isTable;

  @ColumnDef(
      value = "id",
      type = "BIGINT",
      primary = true,
      autoIncrease = true,
      notNull = true
  )
  public final transient KeyValue<UserDataMap> id = new KeyValue<>(this, "id", "id", supplier);

  @ColumnDef(
      value = "gmt_created",
      type = "DATETIME"
  )
  public final transient KeyValue<UserDataMap> gmtCreated = new KeyValue<>(this, "gmt_created", "gmtCreated", supplier);

  @ColumnDef(
      value = "gmt_modified",
      type = "DATETIME"
  )
  public final transient KeyValue<UserDataMap> gmtModified = new KeyValue<>(this, "gmt_modified", "gmtModified", supplier);

  @ColumnDef(
      value = "is_deleted",
      type = "TINYINT",
      defaultValue = "0"
  )
  public final transient KeyValue<UserDataMap> isDeleted = new KeyValue<>(this, "is_deleted", "isDeleted", supplier);

  @ColumnDef(
      value = "age",
      type = "INT"
  )
  public final transient KeyValue<UserDataMap> age = new KeyValue<>(this, "age", "age", supplier);

  @ColumnDef(
      value = "email",
      type = "VARCHAR(50)"
  )
  public final transient KeyValue<UserDataMap> email = new KeyValue<>(this, "email", "email", supplier);

  @ColumnDef(
      value = "name",
      type = "VARCHAR(30)"
  )
  public final transient KeyValue<UserDataMap> name = new KeyValue<>(this, "name", "name", supplier);

  UserDataMap(boolean isTable) {
    super();
    this.isTable = isTable;
  }

  UserDataMap(boolean isTable, int size) {
    super(size);
    this.isTable = isTable;
  }

  /**
   * 创建UserDataMap
   * 初始化主键和gmtCreate, gmtModified, isDeleted等特殊值
   */
  public UserDataMap init() {
    this.id.autoIncrease();
    this.gmtCreated.values(new Date());
    this.gmtModified.values(new Date());
    this.isDeleted.values(false);
    return this;
  }

  public UserDataMap with(Consumer<UserDataMap> init) {
    init.accept(this);
    return this;
  }

  public static UserDataMap table() {
    return new UserDataMap(true, 1);
  }

  public static UserDataMap table(int size) {
    return new UserDataMap(true, size);
  }

  public static UserDataMap entity() {
    return new UserDataMap(false, 1);
  }

  public static UserDataMap entity(int size) {
    return new UserDataMap(false, size);
  }

  /**
   * DataMap数据和表[t_user_table]数据比较
   */
  public UserDataMap eqTable(EqMode... modes) {
    IDatabase.db.table("t_user_table").query().eqDataMap(this, modes);
    return this;
  }

  /**
   * DataMap数据和表[t_user_table]数据比较
   */
  public UserDataMap eqQuery(String query, EqMode... modes) {
    IDatabase.db.table("t_user_table").queryWhere(query).eqDataMap(this, modes);
    return this;
  }

  /**
   * DataMap数据和表[t_user_table]数据比较
   */
  public UserDataMap eqQuery(IDataMap query, EqMode... modes) {
    IDatabase.db.table("t_user_table").queryWhere(query).eqDataMap(this, modes);
    return this;
  }

  /**
   * 清空[t_user_table]表数据
   */
  public UserDataMap clean() {
    IDatabase.db.cleanTable("t_user_table");
    return this;
  }

  /**
   * 插入[t_user_table]表数据
   */
  public UserDataMap insert() {
    IDatabase.db.table("t_user_table").insert(this);
    return this;
  }

  /**
   * 先清空, 再插入[t_user_table]表数据
   */
  public UserDataMap cleanAndInsert() {
    return this.clean().insert();
  }

  public static class Factory {
    public UserDataMap table() {
      return UserDataMap.table();
    }

    public UserDataMap table(int size) {
      return  UserDataMap.table(size);
    }

    public UserDataMap initTable() {
      return UserDataMap.table().init();
    }

    public UserDataMap initTable(int size) {
      return  UserDataMap.table(size).init();
    }

    public UserDataMap entity() {
      return UserDataMap.entity();
    }

    public UserDataMap entity(int size) {
      return  UserDataMap.entity(size);
    }
  }
}
