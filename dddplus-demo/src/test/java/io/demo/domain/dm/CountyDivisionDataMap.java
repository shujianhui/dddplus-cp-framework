package io.demo.domain.dm;

import java.lang.Boolean;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Supplier;
import org.test4j.hamcrest.matcher.modes.EqMode;
import org.test4j.module.database.IDatabase;
import org.test4j.module.database.annotations.ColumnDef;
import org.test4j.module.database.annotations.ScriptTable;
import org.test4j.tools.datagen.DataMap;
import org.test4j.tools.datagen.IDataMap;
import org.test4j.tools.datagen.KeyValue;

/**
 * CountyDivisionDataMap: 表(实体)数据对比(插入)构造器
 *
 * @author Powered By Test4J
 */
@ScriptTable("t_county_division")
@SuppressWarnings({"unused", "rawtypes"})
public class CountyDivisionDataMap extends DataMap<CountyDivisionDataMap> {
  private boolean isTable;

  private final Supplier<Boolean> supplier = () -> this.isTable;

  @ColumnDef(
      value = "id",
      type = "BIGINT UNSIGNED",
      primary = true,
      autoIncrease = true,
      notNull = true
  )
  public final transient KeyValue<CountyDivisionDataMap> id = new KeyValue<>(this, "id", "id", supplier);

  @ColumnDef(
      value = "gmt_created",
      type = "DATETIME"
  )
  public final transient KeyValue<CountyDivisionDataMap> gmtCreated = new KeyValue<>(this, "gmt_created", "gmtCreated", supplier);

  @ColumnDef(
      value = "gmt_modified",
      type = "DATETIME"
  )
  public final transient KeyValue<CountyDivisionDataMap> gmtModified = new KeyValue<>(this, "gmt_modified", "gmtModified", supplier);

  @ColumnDef(
      value = "is_deleted",
      type = "TINYINT",
      defaultValue = "0"
  )
  public final transient KeyValue<CountyDivisionDataMap> isDeleted = new KeyValue<>(this, "is_deleted", "isDeleted", supplier);

  @ColumnDef(
      value = "city",
      type = "VARCHAR(50)"
  )
  public final transient KeyValue<CountyDivisionDataMap> city = new KeyValue<>(this, "city", "city", supplier);

  @ColumnDef(
      value = "county",
      type = "VARCHAR(50)"
  )
  public final transient KeyValue<CountyDivisionDataMap> county = new KeyValue<>(this, "county", "county", supplier);

  @ColumnDef(
      value = "province",
      type = "VARCHAR(50)"
  )
  public final transient KeyValue<CountyDivisionDataMap> province = new KeyValue<>(this, "province", "province", supplier);

  CountyDivisionDataMap(boolean isTable) {
    super();
    this.isTable = isTable;
  }

  CountyDivisionDataMap(boolean isTable, int size) {
    super(size);
    this.isTable = isTable;
  }

  /**
   * 创建CountyDivisionDataMap
   * 初始化主键和gmtCreate, gmtModified, isDeleted等特殊值
   */
  public CountyDivisionDataMap init() {
    this.id.autoIncrease();
    this.gmtCreated.values(new Date());
    this.gmtModified.values(new Date());
    this.isDeleted.values(false);
    return this;
  }

  public CountyDivisionDataMap with(Consumer<CountyDivisionDataMap> init) {
    init.accept(this);
    return this;
  }

  public static CountyDivisionDataMap table() {
    return new CountyDivisionDataMap(true, 1);
  }

  public static CountyDivisionDataMap table(int size) {
    return new CountyDivisionDataMap(true, size);
  }

  public static CountyDivisionDataMap entity() {
    return new CountyDivisionDataMap(false, 1);
  }

  public static CountyDivisionDataMap entity(int size) {
    return new CountyDivisionDataMap(false, size);
  }

  /**
   * DataMap数据和表[t_county_division]数据比较
   */
  public CountyDivisionDataMap eqTable(EqMode... modes) {
    IDatabase.db.table("t_county_division").query().eqDataMap(this, modes);
    return this;
  }

  /**
   * DataMap数据和表[t_county_division]数据比较
   */
  public CountyDivisionDataMap eqQuery(String query, EqMode... modes) {
    IDatabase.db.table("t_county_division").queryWhere(query).eqDataMap(this, modes);
    return this;
  }

  /**
   * DataMap数据和表[t_county_division]数据比较
   */
  public CountyDivisionDataMap eqQuery(IDataMap query, EqMode... modes) {
    IDatabase.db.table("t_county_division").queryWhere(query).eqDataMap(this, modes);
    return this;
  }

  /**
   * 清空[t_county_division]表数据
   */
  public CountyDivisionDataMap clean() {
    IDatabase.db.cleanTable("t_county_division");
    return this;
  }

  /**
   * 插入[t_county_division]表数据
   */
  public CountyDivisionDataMap insert() {
    IDatabase.db.table("t_county_division").insert(this);
    return this;
  }

  /**
   * 先清空, 再插入[t_county_division]表数据
   */
  public CountyDivisionDataMap cleanAndInsert() {
    return this.clean().insert();
  }

  public static class Factory {
    public CountyDivisionDataMap table() {
      return CountyDivisionDataMap.table();
    }

    public CountyDivisionDataMap table(int size) {
      return  CountyDivisionDataMap.table(size);
    }

    public CountyDivisionDataMap initTable() {
      return CountyDivisionDataMap.table().init();
    }

    public CountyDivisionDataMap initTable(int size) {
      return  CountyDivisionDataMap.table(size).init();
    }

    public CountyDivisionDataMap entity() {
      return CountyDivisionDataMap.entity();
    }

    public CountyDivisionDataMap entity(int size) {
      return  CountyDivisionDataMap.entity(size);
    }
  }
}
