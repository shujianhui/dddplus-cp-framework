package io.demo.domain.dao.intf;

import cn.org.atool.fluent.mybatis.base.IBaseDao;
import io.demo.domain.entity.MemberEntity;

/**
 * MemberDao: 数据操作接口
 *
 * 这只是一个减少手工创建的模板文件
 * 可以任意添加方法和实现, 更改作者和重定义类名
 * <p/>@author Powered By Fluent Mybatis
 */
public interface MemberDao extends IBaseDao<MemberEntity> {
}
