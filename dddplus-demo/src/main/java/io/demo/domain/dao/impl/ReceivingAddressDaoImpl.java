package io.demo.domain.dao.impl;

import io.demo.domain.dao.base.ReceivingAddressBaseDao;
import io.demo.domain.dao.intf.ReceivingAddressDao;
import org.springframework.stereotype.Repository;

/**
 * ReceivingAddressDaoImpl: 数据操作接口实现
 *
 * 这只是一个减少手工创建的模板文件
 * 可以任意添加方法和实现, 更改作者和重定义类名
 * <p/>@author Powered By Fluent Mybatis
 */
@Repository
public class ReceivingAddressDaoImpl extends ReceivingAddressBaseDao implements ReceivingAddressDao {
}
