package io.demo.domain.dao.impl;

import io.demo.domain.dao.base.CountyDivisionBaseDao;
import io.demo.domain.dao.intf.CountyDivisionDao;
import org.springframework.stereotype.Repository;

/**
 * CountyDivisionDaoImpl: 数据操作接口实现
 *
 * 这只是一个减少手工创建的模板文件
 * 可以任意添加方法和实现, 更改作者和重定义类名
 * <p/>@author Powered By Fluent Mybatis
 */
@Repository
public class CountyDivisionDaoImpl extends CountyDivisionBaseDao implements CountyDivisionDao {
}
