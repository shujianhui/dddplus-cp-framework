package io.demo.domain.dao.intf;

import cn.org.atool.fluent.mybatis.base.IBaseDao;
import io.demo.domain.dao.model.ScoreStatistics;
import io.demo.domain.entity.StudentScoreEntity;

import java.util.List;

/**
 * StudentScoreDao: 数据操作接口
 *
 * 这只是一个减少手工创建的模板文件
 * 可以任意添加方法和实现, 更改作者和重定义类名
 * <p/>@author Powered By Fluent Mybatis
 */
public interface StudentScoreDao extends IBaseDao<StudentScoreEntity> {

    /**
     * 统计从fromYear到endYear年间学科subjects的统计数据
     *
     * @param fromSchoolTerm 统计年份区间开始
     * @param endSchoolTerm  统计年份区间结尾
     * @param subjects       统计的学科列表
     * @return 统计数据
     */
    List<ScoreStatistics> statistics(int fromSchoolTerm, int endSchoolTerm, String[] subjects);
}
