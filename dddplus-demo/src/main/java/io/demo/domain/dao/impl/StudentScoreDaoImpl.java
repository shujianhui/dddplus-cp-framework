package io.demo.domain.dao.impl;

import io.demo.domain.dao.base.StudentScoreBaseDao;
import io.demo.domain.dao.intf.StudentScoreDao;
import io.demo.domain.dao.model.ScoreStatistics;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * StudentScoreDaoImpl: 数据操作接口实现
 *
 * 这只是一个减少手工创建的模板文件
 * 可以任意添加方法和实现, 更改作者和重定义类名
 * <p/>@author Powered By Fluent Mybatis
 */
@Repository
public class StudentScoreDaoImpl extends StudentScoreBaseDao implements StudentScoreDao {

    @Override
    public List<ScoreStatistics> statistics(int fromSchoolTerm, int endSchoolTerm, String[] subjects) {
        return super.listPoJos(ScoreStatistics.class, super.query()
                .select.schoolTerm().subject()
                .count("count")
                .min.score("min_score")
                .max.score("max_score")
                .avg.score("avg_score")
                .end()
                .where.isDeleted().isFalse()
                .and.schoolTerm().between(fromSchoolTerm, endSchoolTerm)
                .and.subject().in(subjects)
                .end()
                .groupBy.schoolTerm().subject().end()
                .orderBy.schoolTerm().asc().subject().asc().end()
        );
    }
}
