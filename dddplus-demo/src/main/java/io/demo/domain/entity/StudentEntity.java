package io.demo.domain.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.LogicDelete;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.RichEntity;
import cn.org.atool.fluent.mybatis.functions.TableSupplier;
import java.io.Serializable;
import java.lang.Boolean;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Date;
import java.util.function.Consumer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * StudentEntity: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@SuppressWarnings({"unchecked"})
@Data
@Accessors(
    chain = true
)
@EqualsAndHashCode(
    callSuper = false
)
@FluentMybatis(
    table = "t_student",
    schema = "demo"
)
public class StudentEntity extends RichEntity {
  private static final long serialVersionUID = 1L;

  /**
   * 主键id
   */
  @TableId("id")
  private Long id;

  /**
   * 创建时间
   */
  @TableField(
      value = "gmt_created",
      insert = "now()"
  )
  private Date gmtCreated;

  /**
   * 更新时间
   */
  @TableField(
      value = "gmt_modified",
      insert = "now()",
      update = "now()"
  )
  private Date gmtModified;

  /**
   * 是否逻辑删除
   */
  @TableField(
      value = "is_deleted",
      insert = "0"
  )
  @LogicDelete
  private Boolean isDeleted;

  /**
   * 家庭详细住址
   */
  @TableField("address")
  private String address;

  /**
   * receiving_address表外键
   */
  @TableField("address_id")
  private Long addressId;

  /**
   * 年龄
   */
  @TableField("age")
  private Integer age;

  /**
   * 生日
   */
  @TableField("birthday")
  private Date birthday;

  /**
   * 积分
   */
  @TableField("bonus_points")
  private Long bonusPoints;

  /**
   * 性别, 0:女; 1:男
   */
  @TableField("gender_man")
  private Boolean genderMan;

  /**
   * 年级
   */
  @TableField("grade")
  private Integer grade;

  /**
   * 家庭所在区县
   */
  @TableField("home_county_id")
  private Long homeCountyId;

  /**
   * 电话
   */
  @TableField("phone")
  private String phone;

  /**
   * 状态(字典)
   */
  @TableField("status")
  private String status;

  /**
   * 名字
   */
  @TableField("user_name")
  private String userName;

  @Override
  public Serializable findPk() {
    return this.id;
  }

  @Override
  public Consumer<Long> pkSetter() {
    return this::setId;
  }

  @Override
  public final Class<? extends IEntity> entityClass() {
    return StudentEntity.class;
  }

  @Override
  public final StudentEntity changeTableBelongTo(TableSupplier supplier) {
    return super.changeTableBelongTo(supplier);
  }

  @Override
  public final StudentEntity changeTableBelongTo(String table) {
    return super.changeTableBelongTo(table);
  }
}
