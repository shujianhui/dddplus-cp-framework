package io.demo.domain;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("io.demo.domain.mapper*")
public class DddplusDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DddplusDemoApplication.class, args);
    }

}
