中台框架，DDD领域驱动设计适合复杂系统设计开发，ddd架构层次清晰简明，fluent-mybatis主要是节省开发工作量，asyncTool解决高并发，仓储系统WMS(warehouse managment system),订单管理系统OMS(order managment system，包括了支付中台) 。

**用到的技术**
SpringBoot 2.3.5.RELEASE
fluent-mybatis 1.7.3
HikariCP 4.0.3
mysql 8.0.21
clickhouse 0.3.1-patch
lombok 1.18.16
mapstruct-jdk8 1.4.2.Final
hutool 5.7.10