# 领域基础服务业务框架
文档链接：https://nv95cg33i6.feishu.cn/docs/doccnDEv1PRZ6LM9JNYhbGAhaqe#9oahiq
## 使用说明
### 1.pom依赖
```xml
    <dependency>
        <groupId>com.xy.ddd</groupId>
        <artifactId>ddd-framework</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
```
### 2.代码配置

方法一：
在启动类加入注解@ImportAutoConfiguration(value = {FrameworkAutoConfig.class})

方法二：
Bean注入

```java
@Bean
public FrameworkAutoConfig frameworkAutoConfig() {
    return new FrameworkAutoConfig();
}
```

### 3.代码说明
数据库映射实体类父类：BasePo<br/>
领域业务模型父类：DomainModel<br/>
通用Mapper父类：BaseMapper<br/>
仓储服务默认实现：DefaultRpository<br/>
领域服务抽象类：AbstractDomainService

### 4.代码开发

