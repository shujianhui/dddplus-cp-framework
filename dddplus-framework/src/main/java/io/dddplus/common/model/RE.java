package io.dddplus.common.model;

import cn.hutool.http.HttpStatus;

public enum RE {
    /**
     * 操作成功！
     */
    OK(HttpStatus.HTTP_OK, "OK"),
    FAILED(HttpStatus.HTTP_INTERNAL_ERROR, "系统繁忙"),
    FAILED_RELEASE(HttpStatus.HTTP_INTERNAL_ERROR, "请勿尝试释放其他线程资源锁"),
    FAILED_TOO_MANY_REQUESTS(HttpStatus.HTTP_INTERNAL_ERROR, "当前请求人数过多请稍后再试"),
    FAILED_REPEAT_REQUEST(HttpStatus.HTTP_INTERNAL_ERROR, "重复请求，请稍后再试"),
    FAILED_EMPTY(HttpStatus.HTTP_INTERNAL_ERROR, "指定数据不存在"),
    FAILED_REPEAT_PARAM_EMPTY(HttpStatus.HTTP_INTERNAL_ERROR, "请求参数 %s 为空"),
    FAILED_REPEAT_PARAM_INVALID(HttpStatus.HTTP_INTERNAL_ERROR, "无效参数 %s,可选值 %s"),
    FAILED_REPEAT_FORBIDDEN(HttpStatus.HTTP_FORBIDDEN, "授权失败，禁止访问"),
    FAILED_REQUEST_NOT_SUPPORTED(HttpStatus.HTTP_INTERNAL_ERROR, "请求方式不被允许"),
    FAILED_REQUEST_LT_EQ_ZERO(HttpStatus.HTTP_INTERNAL_ERROR, "请求参数 %s 小于0"),
    ;

    /**
     * 状态码
     */
    private final int code;

    /**
     * 消息
     */
    private final String msg;

    RE(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
