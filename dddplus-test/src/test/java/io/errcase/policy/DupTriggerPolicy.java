package io.errcase.policy;

import io.dddplus.annotation.Policy;
import io.dddplus.ext.IExtPolicy;
import io.dddplus.runtime.registry.mock.ext.ITrigger;
import io.dddplus.runtime.registry.mock.model.FooModel;

@Policy(extClazz = ITrigger.class)
public class DupTriggerPolicy implements IExtPolicy<FooModel> {
    @Override
    public String extensionCode(FooModel model) {
        return "foo";
    }
}
