package io.errcase.service;

import lombok.extern.slf4j.Slf4j;
import io.dddplus.annotation.DomainService;
import io.dddplus.model.IDomainService;
import io.dddplus.runtime.DDD;
import io.dddplus.runtime.registry.mock.ability.FooDomainAbility;
import io.dddplus.runtime.registry.mock.model.FooModel;

@DomainService(domain = "non-exist")
@Slf4j
public class DomainServiceWithInvalidDomain implements IDomainService {

    public void submitOrder(FooModel model) {
        FooDomainAbility ability = DDD.findAbility(FooDomainAbility.class);
        log.info(ability.submit(model));
    }

}
