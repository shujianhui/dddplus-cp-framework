package io.errcase.partner;


import io.dddplus.annotation.Partner;
import io.dddplus.ext.IIdentityResolver;
import io.dddplus.runtime.registry.mock.model.FooModel;

import javax.validation.constraints.NotNull;

@Partner(code = DupFooPartner.CODE, name = "BP::foo")
public class DupFooPartner implements IIdentityResolver<FooModel> {
    public static final String CODE = "ddd.cn.ka";

    @Override
    public boolean match(@NotNull FooModel model) {
        return DupFooPartner.CODE.equals(model.getPartnerCode());
    }
}
