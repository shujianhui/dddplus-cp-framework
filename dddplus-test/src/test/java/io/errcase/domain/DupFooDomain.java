package io.errcase.domain;


import io.dddplus.annotation.Domain;

@Domain(code = DupFooDomain.CODE, name = "Foo域")
public class DupFooDomain {
    public static final String CODE = "FooDomain";
}
