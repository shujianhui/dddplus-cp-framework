package io.dddplus.runtime.registry.mock.interceptor;

import io.dddplus.runtime.BaseDomainAbility;
import io.dddplus.step.IDomainStep;

import java.lang.annotation.*;

/**
 * 领域能力或领域步骤方法上的自动监控、拦截机制演示.
 * <p>
 * <p>应用在{@link BaseDomainAbility}或{@link IDomainStep}的公共方法上.</p>
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DomainProfiler {
}
