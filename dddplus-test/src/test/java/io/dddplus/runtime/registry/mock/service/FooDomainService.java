package io.dddplus.runtime.registry.mock.service;

import io.dddplus.annotation.DomainService;
import io.dddplus.runtime.DDD;
import io.dddplus.model.IDomainService;
import lombok.extern.slf4j.Slf4j;
import io.dddplus.runtime.registry.mock.ability.FooDomainAbility;
import io.dddplus.runtime.registry.mock.domain.FooDomain;
import io.dddplus.runtime.registry.mock.model.FooModel;

@DomainService(domain = FooDomain.CODE)
@Slf4j
public class FooDomainService implements IDomainService {

    public void submitOrder(FooModel model) {
        FooDomainAbility ability = DDD.findAbility(FooDomainAbility.class);
        log.info(ability.submit(model));
    }

}
