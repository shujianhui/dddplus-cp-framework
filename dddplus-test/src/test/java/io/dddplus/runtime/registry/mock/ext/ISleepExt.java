package io.dddplus.runtime.registry.mock.ext;

import io.dddplus.ext.IDomainExtension;

public interface ISleepExt extends IDomainExtension {

    void sleep(int seconds);
}
