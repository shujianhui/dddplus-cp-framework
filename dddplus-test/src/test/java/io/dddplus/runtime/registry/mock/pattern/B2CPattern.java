package io.dddplus.runtime.registry.mock.pattern;

import io.dddplus.annotation.Pattern;
import io.dddplus.ext.IIdentityResolver;
import io.dddplus.model.IDomainModel;
import io.dddplus.runtime.registry.mock.model.FooModel;

@Pattern(code = B2CPattern.CODE, name = "B2C模式")
public class B2CPattern implements IIdentityResolver<IDomainModel> {
    public static final String CODE = "bar";

    @Override
    public boolean match(IDomainModel model) {
        if (!(model instanceof FooModel)) {
            return false;
        }

        return ((FooModel) model).isB2c();
    }
}
