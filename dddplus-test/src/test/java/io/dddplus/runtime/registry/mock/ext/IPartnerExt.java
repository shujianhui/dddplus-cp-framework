package io.dddplus.runtime.registry.mock.ext;

import io.dddplus.ext.IDomainExtension;
import io.dddplus.runtime.registry.mock.model.FooModel;

public interface IPartnerExt extends IDomainExtension {

    Integer execute(FooModel model);

}
