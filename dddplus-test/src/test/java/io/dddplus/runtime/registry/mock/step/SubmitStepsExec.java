package io.dddplus.runtime.registry.mock.step;

import io.dddplus.runtime.StepsExecTemplate;
import io.dddplus.runtime.registry.mock.model.FooModel;
import org.springframework.stereotype.Component;

@Component
public class SubmitStepsExec extends StepsExecTemplate<SubmitStep, FooModel> {
}
