package io.dddplus.runtime.registry.mock.pattern.extension;

import io.dddplus.annotation.Extension;
import io.dddplus.runtime.registry.mock.step.Steps;
import io.dddplus.runtime.registry.mock.ext.IReviseStepsExt;
import io.dddplus.runtime.registry.mock.model.FooModel;
import io.dddplus.runtime.registry.mock.pattern.RedecideStepsPattern;

import java.util.ArrayList;
import java.util.List;

@Extension(code = RedecideStepsPattern.CODE)
public class ReviseStepsExt implements IReviseStepsExt {

    @Override
    public List<String> reviseSteps(FooModel model) {
        List<String> result = new ArrayList<>();
        result.add(Steps.Submit.BazStep);
        result.add(Steps.Submit.HamStep);
        return result;
    }
}
