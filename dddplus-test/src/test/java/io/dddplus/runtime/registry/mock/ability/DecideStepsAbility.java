package io.dddplus.runtime.registry.mock.ability;

import io.dddplus.annotation.DomainAbility;
import io.dddplus.runtime.BaseDecideStepsAbility;
import io.dddplus.runtime.registry.mock.domain.FooDomain;
import io.dddplus.runtime.registry.mock.model.FooModel;

@DomainAbility(domain = FooDomain.CODE, value = "mockDecideStepsAbility", tags = AbilityTag.decideSteps)
public class DecideStepsAbility extends BaseDecideStepsAbility<FooModel> {
}
