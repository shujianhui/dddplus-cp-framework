package io.dddplus.runtime.registry.mock.step;

import io.dddplus.runtime.registry.mock.exception.FooException;
import io.dddplus.runtime.registry.mock.model.FooModel;
import io.dddplus.step.IDomainStep;

public abstract class CancelStep implements IDomainStep<FooModel, FooException> {

    @Override
    public String activityCode() {
        return Steps.Cancel.Activity;
    }
}
