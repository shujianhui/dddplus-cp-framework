package io.dddplus.runtime.registry.mock.ext;

import io.dddplus.ext.IDomainExtension;
import io.dddplus.runtime.registry.mock.model.FooModel;

public interface ITrigger extends IDomainExtension {

    void beforeInsert(FooModel model);

    void afterInsert(FooModel model);
}
