package io.dddplus.runtime.registry.mock.ext;

import io.dddplus.ext.IDomainExtension;
import io.dddplus.runtime.registry.mock.model.FooModel;

import java.util.List;

public interface IReviseStepsExt extends IDomainExtension {

    List<String> reviseSteps(FooModel model);

}
