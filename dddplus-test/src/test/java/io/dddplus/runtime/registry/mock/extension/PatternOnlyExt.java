package io.dddplus.runtime.registry.mock.extension;

import io.dddplus.annotation.Extension;
import io.dddplus.runtime.registry.mock.ext.IPatternOnlyExt;
import io.dddplus.runtime.registry.mock.pattern.B2BPattern;

@Extension(code = B2BPattern.CODE)
public class PatternOnlyExt implements IPatternOnlyExt {

}
