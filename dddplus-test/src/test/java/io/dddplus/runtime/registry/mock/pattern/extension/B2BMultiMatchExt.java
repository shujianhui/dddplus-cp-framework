package io.dddplus.runtime.registry.mock.pattern.extension;

import io.dddplus.annotation.Extension;
import io.dddplus.runtime.registry.mock.ext.IMultiMatchExt;
import io.dddplus.runtime.registry.mock.pattern.B2BPattern;

@Extension(code = B2BPattern.CODE)
public class B2BMultiMatchExt implements IMultiMatchExt {
}
