package io.dddplus.runtime.registry.mock.domain;


import io.dddplus.annotation.Domain;

@Domain(code = FooDomain.CODE, name = "Foo域")
public class FooDomain {
    public static final String CODE = "FooDomain";
}
