package io.dddplus.runtime.registry.mock.pattern.extension;

import io.dddplus.annotation.Extension;
import io.dddplus.runtime.registry.mock.ext.IMultiMatchExt;
import io.dddplus.runtime.registry.mock.pattern.FooPattern;

@Extension(code = FooPattern.CODE)
public class FooMultiMatchExt implements IMultiMatchExt {
}
