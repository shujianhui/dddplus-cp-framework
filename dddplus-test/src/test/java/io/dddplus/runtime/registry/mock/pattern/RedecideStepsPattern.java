package io.dddplus.runtime.registry.mock.pattern;

import io.dddplus.annotation.Pattern;
import io.dddplus.ext.IIdentityResolver;
import io.dddplus.model.IDomainModel;
import io.dddplus.runtime.registry.mock.model.FooModel;

import javax.validation.constraints.NotNull;

@Pattern(code = RedecideStepsPattern.CODE, name = "重新编排后续步骤")
public class RedecideStepsPattern implements IIdentityResolver<IDomainModel> {
    public static final String CODE = "revise";

    @Override
    public boolean match(@NotNull IDomainModel model) {
        if (!(model instanceof FooModel)) {
            return false;
        }

        return ((FooModel) model).isRedecide();
    }
}
