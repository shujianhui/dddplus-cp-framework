package io.dddplus.runtime.registry.mock.partner;


import io.dddplus.annotation.Partner;
import io.dddplus.ext.IIdentityResolver;
import io.dddplus.runtime.registry.mock.model.FooModel;

import javax.validation.constraints.NotNull;

@Partner(code = FooPartner.CODE, name = "BP::foo")
public class FooPartner implements IIdentityResolver<FooModel> {
    public static final String CODE = "ddd.cn.ka";

    @Override
    public boolean match(@NotNull FooModel model) {
        return CODE.equals(model.getPartnerCode());
    }
}
