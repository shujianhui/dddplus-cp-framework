package io.dddplus.runtime.registry.mock.pattern;

import io.dddplus.annotation.Pattern;
import io.dddplus.model.IDomainModel;
import io.dddplus.runtime.registry.mock.model.FooModel;
import io.dddplus.ext.IIdentityResolver;

@Pattern(code = FooPattern.CODE, name = "foo模式", tags = Patterns.B2C)
public class FooPattern implements IIdentityResolver<IDomainModel> {
    public static final String CODE = "foo";

    @Override
    public boolean match(IDomainModel model) {
        if (!(model instanceof FooModel)) {
            return false;
        }

        return ((FooModel) model).getPartnerCode().equals("foo");
    }
}
