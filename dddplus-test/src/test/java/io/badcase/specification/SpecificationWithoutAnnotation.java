package io.badcase.specification;

import io.dddplus.runtime.registry.mock.model.FooModel;
import io.dddplus.specification.ISpecification;
import io.dddplus.specification.Notification;

import javax.validation.constraints.NotNull;

public class SpecificationWithoutAnnotation implements ISpecification<FooModel> {
    @Override
    public boolean satisfiedBy(@NotNull FooModel candidate, Notification notification) {
        return false;
    }
}
