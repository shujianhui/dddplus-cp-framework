package io.badcase.pattern;

import io.dddplus.annotation.Pattern;
import io.dddplus.ext.IIdentityResolver;
import io.dddplus.model.IDomainModel;

import javax.validation.constraints.NotNull;

@Pattern(code = InvalidPriorityPattern.CODE, name = "B2B模式", priority = -1)
public class InvalidPriorityPattern implements IIdentityResolver<IDomainModel> {
    public static final String CODE = "invalid";

    @Override
    public boolean match(@NotNull IDomainModel model) {
        return false;
    }
}
