package io.badcase.step.emptyactivity;

import io.dddplus.annotation.Step;
import io.dddplus.runtime.registry.mock.exception.FooException;
import io.dddplus.runtime.registry.mock.model.FooModel;
import io.dddplus.runtime.registry.mock.step.SubmitStep;

@Step
public class EmptyActivityStep extends SubmitStep {
    @Override
    public void execute(FooModel model) throws FooException {

    }

    @Override
    public String activityCode() {
        return "";
    }

    @Override
    public String stepCode() {
        return null;
    }
}
