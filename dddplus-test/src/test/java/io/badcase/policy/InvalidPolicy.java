package io.badcase.policy;

import io.dddplus.annotation.Policy;
import io.dddplus.runtime.registry.mock.ext.IFooExt;

@Policy(extClazz = IFooExt.class)
public class InvalidPolicy {
    // policy must implement IExtPolicy
}
