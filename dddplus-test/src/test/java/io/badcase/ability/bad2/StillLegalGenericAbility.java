package io.badcase.ability.bad2;

import io.dddplus.annotation.DomainAbility;
import io.dddplus.ext.IDomainExtension;
import io.dddplus.model.IDomainModel;
import io.dddplus.runtime.BaseDomainAbility;
import io.dddplus.runtime.registry.mock.domain.FooDomain;
import lombok.extern.slf4j.Slf4j;

// 虽然没有明确指定具体的泛型，但它仍旧合法，Model is IDomainModel, Ext is IDomainExtension
@DomainAbility(domain = FooDomain.CODE)
@Slf4j
public class StillLegalGenericAbility extends BaseDomainAbility {

    @Override
    public IDomainExtension defaultExtension(IDomainModel model) {
        return null;
    }
}
